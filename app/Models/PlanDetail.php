<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanDetail extends Model
{
  public function plans()
  {
      return $this->belongsTo(Plan::class, 'ID_PLAN');
  }
}
