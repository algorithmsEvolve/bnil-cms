<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category()
    {
        return $this->belongsTo(ProductCategorie::class, 'CATEGORY_ID');
    }
    public function plans()
    {
        return $this->hasMany(Plan::class, 'ID_PRODUCT');
    }
    public function getStartFromPrice(){
        $price = array();
        foreach ($this->plans as $plan) {
            foreach ($plan->plan_details as $plan_detail) {
                if(!empty($plan_detail->MONTHLY) || !is_null($plan_detail->MONTHLY)){
                    $price[] = $plan_detail->MONTHLY;
                }
                if(!empty($plan_detail->THREE_MONTH) || !is_null($plan_detail->THREE_MONTH)){
                    $price[] = $plan_detail->THREE_MONTH;
                }
                if(!empty($plan_detail->SIX_MONTH) || !is_null($plan_detail->SIX_MONTH)){
                    $price[] = $plan_detail->SIX_MONTH;
                }
                if(!empty($plan_detail->ANNUALY) || !is_null($plan_detail->ANNUALY)){
                    $price[] = $plan_detail->ANNUALY;
                }
            }
        }
        sort($price);
        return number_format(reset($price), 0, ',', '.');
    }
    public function getIntervalPremi(){
        $result = array(
            'bulanan' => true,
            'triwulan' => true,
            'semester' => true,
            'tahunan' => true,
            'max_waktu' => 0,
        );

        foreach ($this->plans as $plan) {
            foreach ($plan->plan_details as $plan_detail) {
                if(empty($plan_detail->MONTHLY) || is_null($plan_detail->MONTHLY)){
                    $result['bulanan'] = false;
                }
                if(empty($plan_detail->THREE_MONTH) || is_null($plan_detail->THREE_MONTH)){
                    $result['triwulan'] = false;
                }
                if(empty($plan_detail->SIX_MONTH) || is_null($plan_detail->SIX_MONTH)){
                    $result['semester'] = false;
                }
                if(empty($plan_detail->ANNUALY) || is_null($plan_detail->ANNUALY)){
                    $result['tahunan'] = false;
                }
            }
        }
        if($result['bulanan']){
            $result['max_waktu']++;
        }
        if($result['triwulan']){
            $result['max_waktu']++;
        }
        if($result['semester']){
            $result['max_waktu']++;
        }
        if($result['tahunan']){
            $result['max_waktu']++;
        }
        
        return $result;
    }

}
