<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategorie extends Model
{
    
    public function products()
    {
        return $this->hasMany(Product::class, 'CATEGORY_ID');
    }
}
