<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    public function product()
    {
        return $this->belongsTo(Product::class, 'ID_PRODUCT');
    }
    public function plan_details()
    {
        return $this->hasMany(PlanDetail::class, 'ID_PLAN');
    }
    public function plan_manfaats()
    {
        return $this->hasMany(Manfaat::class, 'ID_PLAN');
    }
    public function getFirstPlan(){
        $result = array(
            'table' => '',
            'waktu' => '',
            'nominal' => '',
        );
        foreach ($this->plan_details as $detail) {
            if(!empty($detail->MONTHLY) || !is_null($detail->MONTHLY)){
                $result = array(
                    'table' => 'MONTHLY',
                    'waktu' => 'Perbulan',
                    'nominal' => number_format($detail->MONTHLY, 0, ',', '.'),
                );
            }elseif(!empty($detail->THREE_MONTH) || !is_null($detail->THREE_MONTH)){
                $result = array(
                    'table' => 'THREE_MONTH',
                    'waktu' => '3 Bulan',
                    'nominal' => number_format($detail->THREE_MONTH, 0, ',', '.'),
                );
            }elseif(!empty($detail->SIX_MONTH) || !is_null($detail->SIX_MONTH)){
                $result = array(
                    'table' => 'SIX_MONTH',
                    'waktu' => '6 Bulan',
                    'nominal' => number_format($detail->SIX_MONTH, 0, ',', '.'),
                );
            }elseif(!empty($detail->ANNUALY) || !is_null($detail->ANNUALY)){
                $result = array(
                    'table' => 'ANNUALY',
                    'waktu' => 'Pertahun',
                    'nominal' => number_format($detail->ANNUALY, 0, ',', '.'),
                );
            }
        }
        return $result;
    }
    public function getFirstPlanDetail(){
        $result = array(
            'table' => '',
            'waktu' => '',
            'nominal' => '',
        );
        $detail = $this->plan_details->first();
        if(!empty($detail->MONTHLY) || !is_null($detail->MONTHLY)){
            $result = array(
                'table' => 'MONTHLY',
                'waktu' => 'Perbulan',
                'nominal' => number_format($detail->MONTHLY, 0, ',', '.'),
            );
        }elseif(!empty($detail->THREE_MONTH) || !is_null($detail->THREE_MONTH)){
            $result = array(
                'table' => 'THREE_MONTH',
                'waktu' => '3 Bulan',
                'nominal' => number_format($detail->THREE_MONTH, 2, '.', ','),
            );
        }elseif(!empty($detail->SIX_MONTH) || !is_null($detail->SIX_MONTH)){
            $result = array(
                'table' => 'SIX_MONTH',
                'waktu' => '6 Bulan',
                'nominal' => number_format($detail->SIX_MONTH, 2, '.', ','),
            );
        }elseif(!empty($detail->ANNUALY) || !is_null($detail->ANNUALY)){
            $result = array(
                'table' => 'ANNUALY',
                'waktu' => 'Pertahun',
                'nominal' => number_format($detail->ANNUALY, 2, '.', ','),
            );
        }
        return $result;
    }
}
