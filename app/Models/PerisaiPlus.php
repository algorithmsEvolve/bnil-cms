<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class PerisaiPlus extends Model
{
    public function tipe_kartu_rel()
    {
        return $this->belongsTo(TipeKartuKredit::class, 'TIPE_KARTU');
    }
    public function getNomorKartuAttribute(){
        return Crypt::decryptString($this->attributes['nomor_kartu']);
    }
}
