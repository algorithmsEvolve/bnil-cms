<?php

namespace App\Library\Helpers;

use Illuminate\Support\Facades\Log;
use App\Library\Helpers\ApiHelper;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;

class BnilifeApi
{
  // const CHANNEL_USERNAME = env('CHANNEL_USERNAME');
  // const CHANNEL_PASSWORD = env('CHANNEL_PASSWORD');
  // const CHANNEL_TYPE = env('CHANNEL_TYPE');

  //get token 
  public function getTokenBniL()
  {
    try {
      $api_helper = new ApiHelper();
      $resp = json_decode($api_helper->respApi(env('URL_GETTOKEN'), []));
      if (!isset($resp->access_token)) {
        return false;
      } else {
        return $resp->access_token;
      }
    } catch (\Throwable $e) {
      report($e);
      return false;
    }
  }

  //INSERT POLICY
  public function insertPolicy($request, $params, $mulas, $jtempo, $token)
  {
    try {
      if (App::environment('local')) {
        //untuk testing local
        $message = array(
          "PREFIX"    => "100",
          "KDCHANEL"  => "701",
          "KDPRODUK"  => "BMDP",
          "NOKTP"     => $request->input('NIK'),
          "NOPOLIS"   => 'LOCAL'.mt_rand(100000000, 999999999),
          "MULAS"     => "2021-06-10 21:49:22.002",
          "JTEMPO"    => "2021-06-10 21:49:22.002",
        );
        $result = array("MESSAGE" => $message, 'CODE' => '20');
        return json_encode($result);
      } else {
        $api_helper = new ApiHelper();
        if(empty($token) || $token == false){ // jika token kosong atau false, get hit api token lagi
          $api_bni = new BnilifeApi();
          $token = $api_bni->getTokenBniL();
          if ($token === false) {
            $token = $this->getTokenBniL();
            if ($token === false)
              return response()->json(array('message' => 'ERROR'), 500);
          }
        }

        $header = array(
          "Authorization: Bearer " . $token
        );

        $age = Carbon::parse($request->input('TANGGAL_LAHIR'))->diff(Carbon::now());
        
        $data = array(
          "KODEPREFIX"=> $params->KODEPREFIX,
          "KDCHANNEL" => $params->KDCHANNEL,
          "KDPRODUK"  => $params->KDPRODUK,
          "UP"        => $params->UP,
          "PREMI"     => $params->PREMI,
          "MASA_TH"   => $params->MASA_TH,
          "MASA_BL"   => $params->MASA_BL,
          "MASA_HR"   => $params->MASA_HR,

          "USIA_TH"   => $age->format('%y'),
          "USIA_BL"   => $age->format('%m'),
          "MULAS"     => $mulas->format('Y-m-d H:i:s'),
          "JTEMPO"    => $jtempo->format('Y-m-d H:i:s'),
          "NOKTP"     => $request->input('NIK'),
          "NAMA"      => $request->input('NAMA_LENGKAP'),
          "TGLLHR"    => date("Y-m-d", strtotime($request->input('TANGGAL_LAHIR'))),
          "PHONE"     => $request->input('PHONE'),
          "EMAIL"     => $request->input('EMAIL'),
          "EMAIL_AW"  => $request->input('AW_EMAIL'),
        );
        return $api_helper->respApi(env('URL_INSERTPOLICY_BNI'), $header, $data);
      }
    } catch (\Throwable $e) {
      report($e);
      return response()->json(array('message' => 'ERROR'), 500);
    }
  }

  //List Product
  //params: USERNAME, PASSWORD, CHANNEL
  public function list_product()
  {
    try {
      $api_helper = new ApiHelper();
      $token = $this->getTokenBniL();
      if ($token === false) {
        $token = $this->getTokenBniL();
        if ($token === false)
          return response()->json(array('message' => 'ERROR'), 500);
      }
      $header = array(
        "Authorization: Bearer " . $token
      );
      $params = array(
        "USERNAME" => env('USERNAME_PRD'),
        "PASSWORD" => env('PASSWORD_PRD'),
        "CHANNEL"  => env('CHANNEL_PRD'),
      );
      return json_decode($api_helper->respApi(env('URL_LISTPRODUCT_BNI'), $header, $params));
    } catch (\Throwable $e) {
      report($e);
    }
  }

  //PaymentPremium
  //params: NOPOLIS, TGLBYR, JTTEMPO, KDCHANNEL
  public function paymentPremium($params)
  {
    try {
        $api_helper = new ApiHelper();
        $token = $this->getTokenBniL();
        if ($token === false) {
          $token = $this->getTokenBniL();
          if ($token === false)
            return response()->json(array('message' => 'ERROR'), 500);
        }
        $header = array(
          "Authorization: Bearer " . $token
        );
        return json_decode($api_helper->respApi(env('URL_PAYMENTPREMIUM_BNI'), $header, $params));
    } catch (\Throwable $e) {
      report($e);
      return false;
    }
  }
  //Check KTP
  //params: NOKTP,KDPRODUK
  public function checkKtp($no_ktp, $kd_produk, $token)
  {
    try {
      $api_helper = new ApiHelper();
      if(empty($token) || $token == false){ // jika token kosong atau false, get hit api token lagi
        $api_bni = new BnilifeApi();
        $token = $api_bni->getTokenBniL();
        if ($token === false) {
          $token = $this->getTokenBniL();
          if ($token === false)
            return response()->json(array('message' => 'ERROR'), 500);
        }
      }
      $header = array(
        "Authorization: Bearer " . $token
      );
      $params= array(
        "NOKTP"     => $no_ktp,
        "KDPRODUK"  => $kd_produk,
      );
      return json_decode($api_helper->respApi(env('URL_CHECKKTP_BNI'), $header, $params));
    } catch (\Throwable $e) {
      report($e);
      return false;
    }
  }
}
