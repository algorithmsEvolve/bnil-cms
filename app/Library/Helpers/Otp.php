<?php

namespace App\Library\Helpers;

use App\Jobs\SendOtp;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\App;
use App\Library\Helpers\BnilifeApi;

class Otp
{
  public function sendOtp($mobile_phone, $otp, $message)
  {
    try {
      if (!App::environment('local')) {
        $api_bni = new BnilifeApi();
        $token = $api_bni->getTokenBniL();
        $header = array(
          "Authorization: Bearer " . $token
        );
        
        $url = env('URL_SEND_OTP_BNI');
        $num = preg_replace('/^(?:\+?27|0)?/','+62', $mobile_phone); 
        $post = array(
          'Mobile_phone_number' => $num,
          'OTP' => $otp,
          'Message' => $message,
        );
        SendOtp::dispatch($url, $header, $post);  
      }
      //encrypt otp
      return Hash::make($otp);
    } catch (\Exception $e) {
      Log::error($e);
      return false;
    }
  }

  //if return "00" OTP is correct
  public function decryptOtp($new_otp, $encrypt_otp)
  {
    try {
      if(Hash::check($new_otp, $encrypt_otp)){
        return "00";
      }else{
        return false;
      }      
    } catch (\Exception $e) {
      Log::error($e);
      return false;
    }
  }
}
