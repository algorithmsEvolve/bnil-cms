<?php

namespace App\Library\Doku;
use App\Models\Payment;
use Carbon\Carbon;

class DokuLibrary
{
  protected $CLIENT_ID;
  protected $SECRET_KEY;
  protected $URL_TARGET_CHECKOUT;
  protected $TARGET_PATH_CHECKOUT;
  protected $WEBHOOK_URL_PATH;
  protected $TARGET_PATH_STATUS;
  protected $URL_TARGET_STATUS;

  public function __construct()
  {
    $this->CLIENT_ID = env('CLIENT_ID_DOKU');
    $this->SECRET_KEY = env('SECRET_KEY_ID');
    $this->URL_TARGET_CHECKOUT = env('URL_TARGET_CHECKOUT');
    $this->TARGET_PATH_CHECKOUT = env('TARGET_PATH_CHECKOUT');
    $this->WEBHOOK_URL_PATH = env('WEBHOOK_URL_PATH');
    $this->TARGET_PATH_STATUS = env('TARGET_PATH_STATUS');
    $this->URL_TARGET_STATUS = env('URL_TARGET_STATUS');
  }

  public function generateCheckout($params)
  {
    $dateTime = gmdate("Y-m-d H:i:s");
    $dateTime = date(DATE_ISO8601, strtotime($dateTime));
    $dateTimeFinal = substr($dateTime, 0, 19) . "Z";

    $data = array(
      "order" => array(
        "amount" => $params['amount'],
        "invoice_number" => "BL". $this->randomString(14),
        "currency" => $params['currency'],
        "session_id" => $this->randomString(20, true),
        "callback_url" => $params['callback_url'],
        "line_items" => $params['line_items'],
      ),
      "payment" => array(
        "payment_due_date" => 60
      ),
      "customer" => array(
        "id" => $params['cust_id'],
        "name" => $params['cust_name'],
        "email" => $params['cust_email'],
        "phone" => $params['cust_phone'],
        "address" => $params['cust_address'],
        "country" => $params['cust_country']
      ),
    );
    $targetPath = $this->TARGET_PATH_CHECKOUT;
    $url = $this->URL_TARGET_CHECKOUT;

    $regId = $this->randomString(24, true);
    $header['Client-Id'] = $this->CLIENT_ID;
    $header['Request-Id'] = $regId;
    $header['Request-Timestamp'] = $dateTimeFinal;
    $header['Request-Target'] = $targetPath;
    $signature = "";
    $signature = $this->generateSignature($header, json_encode($data));
    
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Signature:' . $signature,
      'Request-Id:' . $regId,
      'Client-Id:' . $this->CLIENT_ID,
      'Request-Timestamp:' . $dateTimeFinal,
      'Request-Target:' . $url,
    ));

    $responseJson = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    if (is_string($responseJson) && $httpcode == 200) {
      $payment = new Payment();
      $carbon = Carbon::now();
      $payment->NO_INVOICE = $data['order']['invoice_number'];
      $payment->ID_NASABAH = $params['cust_id'];
      $payment->CUST_NAME = $params['cust_name'];
      $payment->CUST_PHONE = $params['cust_phone'];
      $payment->CUST_EMAIL = $params['cust_email'];
      $payment->CUST_ADDRESS = $params['cust_address'];
      $payment->CUST_COUNTRY = $params['cust_country'];
      $payment->TOTAL_AMOUNT = $params['amount'];
      $payment->PAYMENT_STATUS = 0;
      $payment->PAYMENT_DATE = $carbon->isoFormat('YYYY-MM-DD HH:mm:ss.SSS');
      $payment->PAYMENT_CHANNEL = NULL;
      $payment->save();
      if($payment){
        return $responseJson;
      }else{
        return false;
      }
    } else {
      return false;
    }
  }
  
  public function checkStatus($inv_number){
    $dateTime = gmdate("Y-m-d H:i:s");
    $dateTime = date(DATE_ISO8601, strtotime($dateTime));
    $dateTimeFinal = substr($dateTime, 0, 19) . "Z";
    $regId = $this->randomString(24, true);
    $targetPath = $this->TARGET_PATH_STATUS.$inv_number;
    $url = $this->URL_TARGET_STATUS.$inv_number;
    
    $header['Client-Id'] = $this->CLIENT_ID;
    $header['Request-Id'] = $regId;
    $header['Request-Timestamp'] = $dateTimeFinal;
    $header['Request-Target'] = $targetPath;
    $signature = "";
    $signature = $this->generateSignature($header, false, "GET");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
      'Signature:' . $signature,
      'Request-Id:' . $regId,
      'Client-Id:' . $this->CLIENT_ID,
      'Request-Timestamp:' . $dateTimeFinal
    ));

    $responseJson = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);
    
    if (is_string($responseJson) && $httpcode == 200) {
      return $responseJson;
    } else {
      return false;
    }
  }

  public function generateSignature($headers, $body = false, $http_method = "POST")
  {
    $clientId = $this->CLIENT_ID;
    $targetPath = $headers['Request-Target'];
    $secretKey = $this->SECRET_KEY;
    
    if($http_method=="POST"){
      $requestBody = $body;
      // Generate Digest
      $digestValue = base64_encode(hash('sha256', $requestBody, true));
  
      // Prepare Signature Component
      $componentSignature = "Client-Id:" . $clientId . "\n" .
        "Request-Id:" . $headers['Request-Id'] . "\n" .
        "Request-Timestamp:" . $headers['Request-Timestamp'] . "\n" .
        "Request-Target:" . $targetPath . "\n" .
        "Digest:" . $digestValue;
    }else{
      $componentSignature = "Client-Id:" . $clientId . "\n" .
        "Request-Id:" . $headers['Request-Id'] . "\n" .
        "Request-Timestamp:" . $headers['Request-Timestamp'] . "\n" .
        "Request-Target:" . $targetPath;
    }
    // Calculate HMAC-SHA256 base64 from all the components above
    $signature = base64_encode(hash_hmac('sha256', $componentSignature, $secretKey, true));
    // Sample of Usage
    $headerSignature =  "HMACSHA256=" . $signature;

    return $headerSignature;
  }

  public function randomString($length, $lowercase = false)
  {
    if($lowercase){
      $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    }else{
      $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    return substr(
      str_shuffle($str_result),
      0,
      $length
    );
  }
}
