<?php

namespace App\Library\Midtranslib;
use App\Models\Payment;
use Carbon\Carbon;

class CreatePayment
{
  public function generateCheckout($params){
    try{
      // Set your Merchant Server Key
      \Midtrans\Config::$serverKey = env('SECRETKEY_MIDTRANS');
      // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
      \Midtrans\Config::$isProduction = $this->isProduction();
      // Set sanitization on (default)
      \Midtrans\Config::$isSanitized = true;
      // Set 3DS transaction for credit card to true
      \Midtrans\Config::$is3ds = true;
      
      return \Midtrans\Snap::getSnapToken($params);
    }catch (\Throwable $e) {
      report($e);
    }
  }
  public function isProduction(){
    if(env('APP_ENV')=='production'){
      return true;
    }else{
      return false;
    }
  }

}