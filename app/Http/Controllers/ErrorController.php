<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{
    public function notfound(Request $request)
    {
        return view('pages.errors.404');
    }

    public function pageerror(Request $request)
    {
        return view('pages.errors.500');
    }
}
