<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TermsConditions;
use Illuminate\Http\Request;

class TermsController extends Controller
{
    public function __construct()
    {
        $this->model = new TermsConditions();
    }
    public function index()
    {
        $data = $this->model::first();
        return view('admin.terms.index', compact('data'));
    }

    public function store(Request $request, $id)
    {
        $request->validate([
            'detail' => 'required',
        ]);

        try{
            $this->model::findOrFail($id)->update([
                'detail' => $request->detail,
            ]);
            $request->session()->flash('alert-success', 'Success Updated!');
            return redirect()->route('admin.terms');
        } catch (\Exception $e){
            $request->session()->flash('alert-error', 'Failed Updated!');
            return redirect()->back()->withInput();
        }
    }
}
