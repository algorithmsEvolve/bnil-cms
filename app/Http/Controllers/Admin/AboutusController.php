<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AboutUs;

class AboutusController extends Controller
{
    public function __construct()
    {
        $this->model = new AboutUs();
    }
    public function index()
    {
        $data = $this->model::first();
        // dd($data);
        return view('admin.about-us.index', compact('data'));
    }

    public function store(Request $request, $id)
    {
        // dd($id);
        // dd($request->images);
        $request->validate([
            'title' => 'required|max:255',
            'qoute' => 'required',
            'desc' => 'required',
            'images.*' => 'image|mimes:jpg|max:2048'
        ]);

        try{
            $basePath   = "aboutus";
            if($request->images) {
                foreach ($request->images as $key => $image)
                {
                    $filename = 'image'.$key. '.' . $image->getClientOriginalExtension();
                    $image->move(public_path($basePath), $filename);
                }
                // exit();
            }
            $this->model::findOrFail($id)->update([
                'title' => $request->title,
                'desc' => $request->desc,
                'qoute' => $request->qoute,
            ]);
            $request->session()->flash('alert-success', 'Success Updated!');
            return redirect()->route('admin.about');
        } catch (\Exception $e){
            $request->session()->flash('alert-error', 'Failed Updated!');
            return redirect()->back()->withInput();
        }
    }
}
