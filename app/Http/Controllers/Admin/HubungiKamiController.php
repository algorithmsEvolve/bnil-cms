<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use Illuminate\Http\Request;
use DataTables;

class HubungiKamiController extends Controller
{
    public function index()
    {
        return view('admin.hubungi-kami.index');
    }
    public function getHubungiKami(Request $request)
    {
        $hubungi_kami = new ContactUs;
        if ($request->startDate != '' && $request->endDate != '') {
            $hubungi_kami = $hubungi_kami->whereBetween('created_at', [$request->startDate, $request->endDate]);
        }
        $hubungi_kami = $hubungi_kami->orderBy('created_at', 'desc')->get();
        return DataTables::of($hubungi_kami)
            ->addIndexColumn()
            ->editColumn('created_at', function ($hubungi_kami) {
                return date("d-m-Y H:i", strtotime($hubungi_kami->created_at));
            })
            ->make(true);
    }
}
