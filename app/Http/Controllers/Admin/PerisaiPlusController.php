<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PerisaiPlus;
use DataTables;

class PerisaiPlusController extends Controller
{
    public function index()
    {
        return view('admin.perisai-plus.index');
    }

    public function getPerisaiPlus(Request $request)
    {
        $perisai_plus = new PerisaiPlus;
        if ($request->startDate != '' && $request->endDate != '') {
            $perisai_plus = $perisai_plus->whereBetween('created_at', [$request->startDate, $request->endDate]);
        }

        $perisai_plus = $perisai_plus->orderBy('created_at', 'desc')->get();

        return DataTables::of($perisai_plus)
            ->addIndexColumn()
            ->editColumn('STATUS', function ($perisai_plus) {
                $text = '';

                switch ($perisai_plus->STATUS) {
                    case 0:
                        $text = 'Terkirim';
                        break;

                    case 1:
                        $text = 'Pending';
                        break;

                    default:
                        $text = '';
                        break;
                }

                return $text;
            })
            ->editColumn('TIPE_KARTU', function ($perisai_plus) {
                return $perisai_plus->tipe_kartu_rel->TIPE_NAME;
            })
            ->editColumn('ACTIVATION_DATE', function ($perisai_plus) {
                return date("d-m-Y H:i", strtotime($perisai_plus->ACTIVATION_DATE));
            })
            ->make(true);
    }
}
