<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PolishController extends Controller
{
    public function index(Request $request)
    {
        return view('pages.polish');
    }
}
