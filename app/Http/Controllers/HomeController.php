<?php

namespace App\Http\Controllers;

use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;


use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        // return redirect()->route('front.perisai_plus');
        return view('pages.home');
    }

    public function about(Request $request)
    {
        return view('pages.about');
    }

    public function service(Request $request)
    {
        return view('pages.service');
    }

    public function product(Request $request)
    {
        return view('pages.product');
    }

    public function packet(Request $request)
    {
        return view('pages.packet');
    }

    public function register(Request $request)
    {
        $provinces = Province::all();

        return view('pages.register', ['provinces' => $provinces]);
    }

    public function contact(Request $request)
    {
        return view('pages.contact');
    }

    public function profile(Request $request)
    {
        return view('pages.profile');
    }

    public function registerSuccess()
    {
        return view('pages.register-success');
    }
}
