<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Plan;
use Illuminate\Http\Request;
use App\Models\Province;
use Illuminate\Support\Facades\Crypt;
use App\Library\Helpers\BnilifeApi;
use App\Models\PlanDetail;
use Carbon\Carbon;

class RegisterController extends Controller
{
    protected $masa_waktu = array('monthly', 'three_month', 'six_month', 'annualy');
    /*Params
    p for id_plan encrypted
    w for waktu interval
    */
    public function index(Request $request){
        try{
            $id_plan_detail = $request->input('p');
            $waktu_plan = $request->input('w');
            $provinces = Province::all();
            $decrypt_id_plan_detail = Crypt::decrypt($id_plan_detail);
            $detail = PlanDetail::find($decrypt_id_plan_detail);
            if(!in_array($waktu_plan, $this->masa_waktu)) // jika waktu_paln tidak temasuk di masa_waktu
                return redirect()->back();

            if(empty($detail)) // jika plan detail tidak ada 
                abort(404);
            
            $plan = $detail->plans;
            if(empty($plan)) // jika plan tidak ada 
                abort(404);

            $product = $plan->product;
            if(empty($product)) // jika produk tidak ada 
                abort(404);
                

            $table_premi = strtoupper($waktu_plan);
            if(is_null($detail->$table_premi))
                abort(404);

            $params = array();
            $masa_waktu = $this->getMasaWaktu($waktu_plan);
            $params['id_plan_detail'] = $decrypt_id_plan_detail;
            $params['waktu_plan'] = $waktu_plan;
            $params['JUMLAH_BULAN'] = $masa_waktu['JUMLAH_BULAN'];

            $params['KDPRODUK'] = $product->PRODUCT_CODE_API;
            $params['KODEPREFIX'] = '700';
            $params['KDCHANNEL'] = '709';
            $params['UP'] = $plan->UP;
            $params['PREMI'] = $detail->$table_premi;
            $params['MASA_TH'] = $masa_waktu['MASA_TH'];
            $params['MASA_BL'] = $masa_waktu['MASA_BL'];
            $params['MASA_HR'] = $masa_waktu['MASA_HR'];

            $price = number_format($detail->$table_premi, 0, ',', '.');
            $json_params =  Crypt::encrypt(json_encode($params));
            $hideRiwayat = $product->HIDE_RIWAYAT_KESEHATAN == 1 ? 'true' : 'false'; // jika hideriwayat true
            return view('pages.register', compact('provinces', 'plan','hideRiwayat','price','json_params'));
        }catch(\Illuminate\Contracts\Encryption\DecryptException $e){
            abort(404);
        } catch (\Throwable $e) {
            report($e);
            return redirect()->back();
        }
    }
    private function getMasaWaktu($waktu_plan){
        $result = array(
            "MASA_TH" => 0,
            "MASA_BL" => 0,
            "MASA_HR" => 0,
            "JUMLAH_BULAN" => 1,
        );
        if($waktu_plan=="monthly"){
            $result = array(
                "MASA_TH" => 0,
                "MASA_BL" => 1,
                "MASA_HR" => 0,
                "JUMLAH_BULAN" => 1,
            );
        }elseif($waktu_plan=="three_month"){
            $result = array(
                "MASA_TH" => 0,
                "MASA_BL" => 3,
                "MASA_HR" => 0,
                "JUMLAH_BULAN" => 3,
            );
        }elseif($waktu_plan=="six_month"){
            $result = array(
                "MASA_TH" => 0,
                "MASA_BL" => 6,
                "MASA_HR" => 0,
                "JUMLAH_BULAN" => 6,
            );
        }elseif($waktu_plan=="annualy"){
            $result = array(
                "MASA_TH" => 1,
                "MASA_BL" => 0,
                "MASA_HR" => 0,
                "JUMLAH_BULAN" => 12,
            );
        }
        return $result;
    }
    
}
