<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\TipeKartuKredit;
use App\SettingParameter;
use Illuminate\Http\Request;

class PerisaiController extends Controller
{
    public function index(Request $request)
    {
        $tipe_kartu = TipeKartuKredit::get();
        $riplay_perisaiplus = SettingParameter::select('SETTING_VALUE')->where('SETTING_ID', 'pdf_perisaiplus')->first();
        return view('pages.perisai', compact('tipe_kartu', 'riplay_perisaiplus'));
    }
}
