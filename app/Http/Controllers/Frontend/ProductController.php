<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategorie;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class ProductController extends Controller
{
    public function single_category($slug){
        $category = ProductCategorie::where('PUBLISHED', 1)->whereRaw('lower(NAME) = ?', strtolower($slug))->first();
        // dd($category->products[0]->getStartFromPrice());
        if(!empty($category)){
            return view('pages.product_cat', compact('category'));
        }else{
            abort(404);
        }
    }
    public function single_product($slug){
        $product = Product::where('PUBLISHED', 1)->whereRaw('lower(SLUG) = ?', strtolower($slug))->first();
        if(!empty($product)){
            $min_date = "";
            $max_date = "";
            if(!empty($product->USIA_MIN) && !empty($product->USIA_MAX)){
                $date = Carbon::now();
                $min_date = $date->subYears($product->USIA_MIN)->format('Y-m-d');
                $max_date = $date->subYears($product->USIA_MAX)->format('Y-m-d');
            }
            $plans = $product->plans;
            $plan_first = array(
                'nominal' => $plans->first()->getFirstPlan()['nominal'],
                'manfaat' => $plans->first()->plan_manfaats->first()->MANFAAT,
                'desc' => json_decode($plans->first()->plan_manfaats->first()->DESC),
            );
            // dd($plan_first);
            $product_id = Crypt::encrypt($product->id);
            return view('pages.product', compact('product','plans','plan_first','product_id','max_date','min_date'));
        }else{
            abort(404);
        }
    }
}
