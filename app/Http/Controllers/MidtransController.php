<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Library\Midtranslib\CreatePayment;
use Illuminate\Support\Facades\Log;

class MidtransController extends Controller
{
    public function index(){
        return view('midtrans-test');
    }
    public function getUrlPayment()
    {
        $midtrans = new CreatePayment;
        $params = array(
            'transaction_details' => array(
                'order_id' => rand(),
                'gross_amount' => 100000,
            ),
            'customer_details' => array(
                'first_name' => 'budi',
                'last_name' => 'pratama',
                'email' => 'budi.pra@example.com',
                'phone' => '08111222333',
            ),
        );

        $response = $midtrans->generateCheckout($params);
        if ($response) {
            return response()->json($response);
        }
    }
    public function getNotify(){
        $doku_library = new DokuLibrary;
        $headers = getallheaders();
        $raw_notification = json_decode(file_get_contents('php://input'), true);
        $headers['Request-Target'] = $doku_library->WEBHOOK_URL_PATH;
        $signature = $doku_library->generateSignature($headers, file_get_contents('php://input'));
        $response = json_encode($raw_notification);
        if ($signature == $headers['Signature']) {
            Log::error('Response Sukses');
            return response()->json([], 200);
        } else {
            Log::error('Response Gagal');
            return response()->json([], 401);
        }
        Log::error($headers['Signature']);
        Log::error($signature);

        header('Content-type:application/json;charset=utf-8');        
    }
    public function getStatus(){
        $doku_library = new DokuLibrary;
        return $doku_library->checkStatus("BLIDHY8O90FPZAGE");
    }
}
