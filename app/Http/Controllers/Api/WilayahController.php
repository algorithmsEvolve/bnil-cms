<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;
use Illuminate\Http\Request;

class WilayahController extends Controller
{
    public function getProvinsi()
    {
        try {
            $locations = Province::all();
            return response()->json(['message' => $locations], 200);
        } catch (\Throwable $e) {
            report($e);
            return response()->json(array('message' => 'ERROR'), 500);
        }
    }
    public function getKabupaten(Request $request)
    {
        try {
            $locations = Regency::where('province_id', $request->input('id_prov'))->get();
            return response()->json(['message' => $locations], 200);
        } catch (\Throwable $e) {
            report($e);
            return response()->json(array('message' => 'ERROR'), 500);
        }
    }
    public function getKecamatan(Request $request)
    {
        try {
            $locations = District::where('regency_id', $request->input('id_kab'))->get();
            return response()->json(['message' => $locations], 200);
        } catch (\Throwable $e) {
            report($e);
            return response()->json(array('message' => 'ERROR'), 500);
        }
    }
    public function getDesa(Request $request)
    {
        try {
            $locations = Village::where('district_id', $request->input('id_kec'))->get();
            return response()->json(['message' => $locations], 200);
        } catch (\Throwable $e) {
            report($e);
            return response()->json(array('message' => 'ERROR'), 500);
        }
    }
}
