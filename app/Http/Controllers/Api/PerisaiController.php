<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\PerisaiPlus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Library\Helpers\Otp;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Crypt;

class PerisaiController extends Controller
{
    public function index()
    {
        // dd(App::environment('local'));
    }

    public function confirmPage(Request $request)
    {
        $validator = $this->validateForm($request, false);
        if ($validator == "00") {
            return response()->json(['message' => 'OK'], 200);
        } else {
            return response()->json([$validator], 403);
        }
    }

    public function createOtp(Request $request)
    {
        try {
            $validator = $this->validateForm($request);
            if ($validator == "00") {
                $phone = $request->input('phone');
                $create_rand_otp = rand(100000, 999999);
                $message = "[BNILIFE] KODE OTP " . $create_rand_otp . '. Untuk Aktivasi Asuransi Perisai Plus. Berlaku 5 Menit';
                if (App::environment('local')) { // if local
                    Log::info('OTP code is ' . $create_rand_otp);
                }
                $otp = new Otp();
                $enrypted_otp =  $otp->sendOtp($phone, $create_rand_otp, $message);
                if ($enrypted_otp === false) {
                    return response()->json(['message' => 'Error'], 500);
                }
                if ($request->session()->has('encrypt_opt')) {
                    $request->session()->forget('encrypt_opt');
                }
                $request->session()->put('encrypt_opt', $enrypted_otp);
                return response()->json(['message' => 'OK'], 200);
            } else {
                return response()->json([$validator], 403);
            }
        } catch (\Throwable $e) {
            report($e);
            return response()->json(['message' => 'Error'], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $validator_form = $this->validateForm($request);
            if ($validator_form != "00") {
                return response()->json([$validator_form], 403);
            }
            $validator_otp = $this->validateOtp($request);
            if ($validator_otp == "00") {
                $number_card = $this->decryptCC($request->input('number_card'));
                $carbon = Carbon::now();
                $perisai_model = new PerisaiPlus;
                $perisai_model->NAMA_NASABAH = $request->input('name');
                $perisai_model->NOMOR_HP = $request->input('phone');
                $perisai_model->NOMOR_KARTU = Crypt::encryptString($number_card);
                $perisai_model->TIPE_KARTU = $request->input('card_type');
                $perisai_model->STATUS = 1;
                $perisai_model->MANUAL_FILLED = 1;
                $perisai_model->ACTIVATION_DATE = $carbon->isoFormat('YYYY-MM-DD HH:mm:ss.SSS');
                $perisai_model->save();
                return response()->json(['message' => 'OK'], 200);
            } else {
                return response()->json(['message' => 'Wrong OTP Code'], 403);
            }
        } catch (\Throwable $e) {
            report($e);
            return response()->json(['message' => 'Error'], 500);
        }
    }
    //validate input otp
    private function validateOtp($request)
    {
        $encrypt_otp = $request->session()->get('encrypt_opt');
        $input_otp = $request->input('otp');
        $otp = new Otp();
        return $otp->decryptOtp($input_otp, $encrypt_otp);
    }

    //validate form function
    private function validateForm($request, $recaptcha = true)
    {
        if ($recaptcha) {
            $response = (new \ReCaptcha\ReCaptcha(env('RECAPTCHA_SECRET_KEY')))
                ->setExpectedAction('perisai_plus')
                ->verify($request->input('_recaptcha'), $request->ip());

            if (!$response->isSuccess()) 
                return "FORBIDDEN rECAPTCHA";
            
        }
        $custom_validator = array(
            'name.required' => 'Silahkan melengkapi data',
            'phone.required' => 'Silahkan melengkapi data',
            'card_type.required' => 'Silahkan melengkapi data',
            'number_card.required' => 'Silahkan melengkapi data',
            'name.max' => 'Maksimal adalah 100 karakter',
            'phone.max' => 'Maksimal adalah 14 karakter',
            'phone.min' => 'Minimal adalah 9 karakter',
            'number_card.max' => 'Jumlah adalah 4 karakter',
            'number_card.min' => 'Jumlah adalah 4 karakter',
            'phone.unique' => 'Nomor HP telah terdaftar',
            'number_card.unique' => 'Nomor kartu telah terdaftar',
            'phone.numeric' => 'Nomor HP harus berisikan angka'
        );
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:100',
            'phone' => 'required|string|min:9|max:14',
            'card_type' => 'required',
            'number_card' => 'required',
        ], $custom_validator);
        if ($validator->fails()) 
            return $validator->errors();

        $number_card = $this->decryptCC($request->input('number_card'));
        if(strlen($number_card) != 4)
            return ["number_card" => ["Jumlah adalah 4 karakter"]];

        return "00";
    }
    public function decryptCC($encoded) {
        $encoded = base64_decode($encoded);
        $decoded = "";
        for( $i = 0; $i < strlen($encoded); $i++ ) {
          $b = ord($encoded[$i]);
          $a = $b ^ 123;  // <-- must be same number used to encode the character
          $decoded .= chr($a);
        }
        return $decoded;
      }
}
