<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Library\Helpers\ApiHelper;
use App\Library\Midtranslib\CreatePayment;
use App\Models\Payment;
use App\Jobs\SendEmail;
use App\Library\Helpers\BnilifeApi;
use Illuminate\Support\Facades\Log;

class MidtransController extends Controller
{
    protected $settlement_time;
    public function getStatus($order_id, $notif_status, $direct = false){
        try{
            if(!$direct){
                $url = "https://api.sandbox.midtrans.com/v2/".$order_id."/status";
                $auth_string = base64_encode(env('SECRETKEY_MIDTRANS').":");
                $header_midtrans[] = 'Authorization: Basic '.$auth_string;
                $header_midtrans[] = 'Accept:application/json';
                $api = new ApiHelper;
                $resp = json_decode($api->respApi($url, $header_midtrans));
                $notif_status = $this->transactionStatus($resp);
            }

            // if($status != $notif_status){
            //     $status = 8;
            //     $detail = [
            //     'view' => 'mail.registration-perisai-plus',
            //     'subject' => '[Microsite BNILIFE] PAYMENT NEED ATTENTION',
            //     'title' => 'Title',
            //     'body' => 'Test Body',
            //     ];
            //     SendEmail::dispatch('kurniarocki30@gmail.com', $detail);
            // }

            $payment = Payment::where('NO_ORDER' , $order_id)->first();
            if(!empty($payment)){
                if($notif_status == 1 || $notif_status == 6){
                    $api_bnil = new BnilifeApi();
                    $params = array(
                        "NOPOLIS" => $order_id,
                        "TGLBYR" => $this->settlement_time,
                        "JTTEMPO" => $payment->JTTEMPO,
                        "KDCHANNEL" => $payment->KDCHANEL,
                      );
                    $resp = $api_bnil->paymentPremium($params);
                    $payment->PAYMENT_PREMIUM = $resp->CODE;
                }
                $payment->PAYMENT_STATUS = $notif_status;
                $payment->save();
                return true;
            }else{
                Log::error("Notif Midtrans: NO POLIS NOT FOUND : ".$order_id);
                return false;    
            }
        } catch (\Throwable $e) {
            report($e);
            return false;
        }

    }

    public function notifHandler(){
        try{
            $raw = json_decode(file_get_contents('php://input'), true);
            if(!is_array($raw))
                return response()->json([], 500);

            if(empty($raw['transaction_id']))
                return response()->json([], 500);

            $helper_midtrans = new CreatePayment();
            \Midtrans\Config::$isProduction = $helper_midtrans->isProduction();
            \Midtrans\Config::$serverKey = env('SECRETKEY_MIDTRANS');
            $notif = (new \Midtrans\Notification());
            $transaction = $notif->transaction_status;
            $type = $notif->payment_type;
            $order_id = $notif->order_id;
            $fraud = $notif->fraud_status;

            $result = 0;
            if ($transaction == 'capture') {
                // For credit card transaction, we need to check whether transaction is challenge by FDS or not
                if ($type == 'credit_card') {
                    if ($fraud == 'challenge') {
                        // TODO set payment status in merchant's database to 'Challenge by FDS'
                        // TODO merchant should decide whether this transaction is authorized or not in MAP
                        $result = 7;
                    } else {
                        // TODO set payment status in merchant's database to 'Success'
                        $this->settlement_time = $notif->transaction_time;
                        $result = 6;
                    }
                }
            } else if ($transaction == 'settlement') {
                // TODO set payment status in merchant's database to 'Settlement'
                $this->settlement_time = $notif->settlement_time;
                $result = 1;
            } else if ($transaction == 'pending') {
                // TODO set payment status in merchant's database to 'Pending'
                $result = 2;
            } else if ($transaction == 'deny') {
                // TODO set payment status in merchant's database to 'Denied'
                $result = 3;
            } else if ($transaction == 'expire') {
                // TODO set payment status in merchant's database to 'expire'
                $result = 4;
            } else if ($transaction == 'cancel') {
                // TODO set payment status in merchant's database to 'Denied'
                $result = 5;
            }
            if($this->getStatus($order_id, $result, true))
                return response()->json([], 200);
            else
                return response()->json([], 400);
            
            Log::channel('payment')->info($result);
            return response()->json([], 200);
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            return response()->json([], 500);
        }
    }

    private function transactionStatus($data){
        $transaction = $data->transaction_status;
        $type = $data->payment_type;
        $fraud = $data->fraud_status;
        $result = 0;
        if ($transaction == 'capture') {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {
                if ($fraud == 'challenge') {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    $result = 7;
                } else {
                    // TODO set payment status in merchant's database to 'Success'
                    $result = 6;
                }
            }
        } else if ($transaction == 'settlement') {
            // TODO set payment status in merchant's database to 'Settlement'
            $result = 1;
        } else if ($transaction == 'pending') {
            // TODO set payment status in merchant's database to 'Pending'
            $result = 2;
        } else if ($transaction == 'deny') {
            // TODO set payment status in merchant's database to 'Denied'
            $result = 3;
        } else if ($transaction == 'expire') {
            // TODO set payment status in merchant's database to 'expire'
            $result = 4;
        } else if ($transaction == 'cancel') {
            // TODO set payment status in merchant's database to 'Denied'
            $result = 5;
        }
        return $result;
    }
}
