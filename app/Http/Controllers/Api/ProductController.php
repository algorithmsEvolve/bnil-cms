<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Nasabah;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Library\Helpers\ApiHelper;
use App\Library\Helpers\BnilifeApi;
use Carbon\Carbon;
use App\Library\Midtranslib\CreatePayment;
use App\Models\Manfaat;
use App\Models\Payment;
use App\Models\PlanDetail;
use App\Models\Product;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\App;

class ProductController extends Controller
{
    protected $token_bearer;

    public function confirmPage(Request $request)
    {
        try {
            $params = json_decode(Crypt::decrypt($request->input('params'))); // decrypt params
            $validator = $this->validateForm($request, $params->KDPRODUK);
            if ($validator == "00") 
                return response()->json(['message' => 'OK', 'code' => "20"], 200);
            else 
                return response()->json(['message' => $validator, 'code' => "403"], 200);
            
        }catch(\Illuminate\Contracts\Encryption\DecryptException $e){
            report($e);
            return response()->json(array('message' => 'INVALID PARAMS'), 500);
        }
    }

    public function storeNasabah(Request $request)
    {
        try {
            $params = json_decode(Crypt::decrypt($request->input('params'))); // decrypt params
            $validator_form = $this->validateForm($request, $params->KDPRODUK);
            if ($validator_form != "00") {
                return response()->json(['message' => $validator_form, 'code' => "403"], 200);
            }
            //hitung mulas dan jatuh tempo
            $mulas = Carbon::now();
            $jtempo = Carbon::now()->addMonths($params->JUMLAH_BULAN);

            $api_bnil = new BnilifeApi();
            $insertPolicy = json_decode($api_bnil->insertPolicy($request, $params, $mulas, $jtempo, $this->token_bearer),true);
            if ($insertPolicy['CODE'] == "21" || $insertPolicy['CODE'] == "24") {
                return response()->json([$insertPolicy], 200);
            } elseif ($insertPolicy['CODE'] == "22") {
                return response()->json([$insertPolicy], 200);
            } elseif ($insertPolicy['CODE'] == "23") {
                return response()->json([$insertPolicy], 200);
            }

            $total_amount = $params->PREMI;
            $nasabah = new Nasabah;
            $nasabah->NIK = $request->input('NIK');
            $nasabah->NAMA_LENGKAP = $request->input('NAMA_LENGKAP');
            $nasabah->JENIS_KELAMIN = $request->input('JENIS_KELAMIN');
            $nasabah->TANGGAL_LAHIR = date("Y-m-d", strtotime($request->input('TANGGAL_LAHIR')));
            $nasabah->EMAIL = $request->input('EMAIL');
            $nasabah->PROVINSI = $request->input('PROVINSI');
            $nasabah->KOTA = $request->input('KOTA');
            $nasabah->KECAMATAN = $request->input('KECAMATAN');
            $nasabah->KELURAHAN = $request->input('KELURAHAN');
            $nasabah->ALAMAT = $request->input('ALAMAT');
            $nasabah->KODE_POS = $request->input('KODE_POS');
            $nasabah->PHONE = $request->input('PHONE');
            // $nasabah->STATUS_PERNIKAHAN = $request->input('STATUS_PERNIKAHAN');
            $nasabah->TEMPAT_LAHIR = $request->input('TEMPAT_LAHIR');
            $nasabah->PENGELUARAN = $request->input('PENGELUARAN');
            $nasabah->PEKERJAAN = $request->input('PEKERJAAN');

            $nasabah->TERTANGGUNG_STATUS_TERTANGGUNG = $request->input('TERTANGGUNG_STATUS_TERTANGGUNG');
            $nasabah->TERTANGGUNG_NIK = $request->input('TERTANGGUNG_NIK');
            $nasabah->TERTANGGUNG_NAMA_LENGKAP = $request->input('TERTANGGUNG_NAMA_LENGKAP');
            $nasabah->TERTANGGUNG_JENIS_KELAMIN = $request->input('TERTANGGUNG_JENIS_KELAMIN');
            $nasabah->TERTANGGUNG_EMAIL = $request->input('TERTANGGUNG_EMAIL');
            $nasabah->TERTANGGUNG_NOMOR_HP = $request->input('TERTANGGUNG_NOMOR_HP');
            $nasabah->TERTANGGUNG_TEMPAT_LAHIR = $request->input('TERTANGGUNG_TEMPAT_LAHIR');
            $nasabah->TERTANGGUNG_TANGGAL_LAHIR = $request->input('TERTANGGUNG_TANGGAL_LAHIR')!==null ? date("Y-m-d", strtotime($request->input('TERTANGGUNG_TANGGAL_LAHIR'))) : null;

            $nasabah->AW_NIK = $request->input('AW_NIK');
            $nasabah->AW_STATUS_PENERIMA = $request->input('AW_STATUS_PENERIMA');
            $nasabah->AW_NAMA_LENGKAP = $request->input('AW_NAMA_LENGKAP');
            $nasabah->AW_JENIS_KELAMIN = $request->input('AW_JENIS_KELAMIN');
            $nasabah->AW_EMAIL = $request->input('AW_EMAIL');
            $nasabah->AW_NOMOR_HP = $request->input('AW_NOMOR_HP');
            $nasabah->AW_TEMPAT_LAHIR = $request->input('AW_TEMPAT_LAHIR');
            $nasabah->AW_TANGGAL_LAHIR = $request->input('AW_TANGGAL_LAHIR')!==null ? date("Y-m-d", strtotime($request->input('AW_TANGGAL_LAHIR'))) : null;

            //upload foto
            $nasabah->TERTANGGUNG_FOTO_KTP = $this->uploadImage($request->input('TERTANGGUNG_FOTO_KTP'), strtolower(Str::random(17)), 'Tertanggung');
            $nasabah->AW_FOTO_KTP = $this->uploadImage($request->input('AW_FOTO_KTP'), strtolower(Str::random(17)), 'AW');

            //ubah jadi insert or update untuk no order atau nik
            $payment = new Payment;
            $payment->NO_ORDER = $insertPolicy['MESSAGE']['NOPOLIS'];
            $payment->NIK = $request->input('NIK');
            $payment->CUST_NAME = $request->input('NAMA_LENGKAP');
            $payment->CUST_PHONE = $request->input('PHONE');
            $payment->CUST_EMAIL = $request->input('EMAIL');
            $payment->CUST_ADDRESS = $request->input('ALAMAT');
            $payment->TOTAL_AMOUNT = $total_amount;
            $payment->PREFIX = $params->KODEPREFIX;
            $payment->KDCHANEL = $insertPolicy['MESSAGE']['KDCHANEL'];
            $payment->KDPRODUK = $insertPolicy['MESSAGE']['KDPRODUK'];

            $payment->MULAS = $mulas->format('Y-m-d H:i:s');
            $payment->JTTEMPO = $jtempo->format('Y-m-d H:i:s');

            DB::transaction(function () use ($nasabah, $payment) {
                $nasabah->save();
                $payment->save();
            });

            $midtrans = new CreatePayment;
            $params = array(
                'transaction_details' => array(
                    'order_id' => $insertPolicy['MESSAGE']['NOPOLIS'],
                    'gross_amount' => $total_amount,
                ),
                'customer_details' => array(
                    'first_name' => $request->input('NAMA_LENGKAP'),
                    'last_name' => '',
                    'email' => $request->input('EMAIL'),
                    'phone' => $request->input('PHONE'),
                ),
                "enabled_payments" => array("cimb_clicks",
                    "bca_klikbca", "bca_klikpay", "bri_epay", "echannel", "permata_va",
                    "bca_va", "bni_va", "bri_va", "other_va", "gopay", "indomaret",
                    "danamon_online", "akulaku", "shopeepay"
                ),
            );

            $response = $midtrans->generateCheckout($params);
            return response()->json(['message' => $response, 'code' => '20']);
            // return response()->json(array('code' => '20', 'nasabah_id' => $nasabah->id, 'no_polis' => $insertPolicy->MESSAGE->NOPOLIS), 200);
        }catch(\Illuminate\Contracts\Encryption\DecryptException $e){
            report($e);
            return response()->json(array('message' => 'INVALID PARAMS'), 500);
        } catch (\Throwable $e) {
            report($e);
            return response()->json(array('message' => 'ERROR'), 500);
        }
    }

    //validate form function
    private function validateForm($request, $kdproduk=null)
    {
        $custom_validator = array(
            'required' => 'Silahkan melengkapi data',
            'number' => 'Hanya format angka',
            'string' => 'Hanya format huruf',
            'email' => 'Harus format email',
            'NIK.size' => 'NO KTP harus 16 karakter',
            'TERTANGGUNG_NIK.size' => 'NO KTP harus 16 karakter',
            'AW_NIK.size' => 'NO KTP harus 16 karakter',
        );
        $validator = Validator::make($request->all(), [
            'NIK' => 'required|size:16',
            'NAMA_LENGKAP' => 'required|string|max:100',
            'JENIS_KELAMIN' => 'required|max:1',
            'TANGGAL_LAHIR' => 'required|max:15',
            'EMAIL' => 'required|email|max:250',
            'PROVINSI' => 'required|max:2',
            'KOTA' => 'required|max:4',
            'KECAMATAN' => 'required|max:7',
            'KELURAHAN' => 'required|max:10',
            'ALAMAT' => 'required|max:100',
            'KODE_POS' => 'nullable|max:6',
            'PHONE' => 'required|max:13',
            'TEMPAT_LAHIR' => 'required|max:100',
            'PENGELUARAN' => 'nullable|max:1',
            'PEKERJAAN' => 'nullable|string|max:100',

            // 'TERTANGGUNG_STATUS_TERTANGGUNG' => 'required|max:1',
            // 'TERTANGGUNG_NIK' => 'required|size:16',
            // 'TERTANGGUNG_NAMA_LENGKAP' => 'required|string|max:100',
            // 'TERTANGGUNG_JENIS_KELAMIN' => 'required|max:1',
            // 'TERTANGGUNG_EMAIL' => 'nullable|email|max:250',
            // 'TERTANGGUNG_NOMOR_HP' => 'nullable|max:13',
            // 'TERTANGGUNG_TEMPAT_LAHIR' => 'required|max:100',
            // 'TERTANGGUNG_TANGGAL_LAHIR' => 'required|max:15',
            // 'TERTANGGUNG_FOTO_KTP' => 'required',

            'AW_NIK' => 'required|size:16',
            'AW_STATUS_PENERIMA' => 'required|max:1',
            'AW_NAMA_LENGKAP' => 'required|string|max:100',
            'AW_JENIS_KELAMIN' => 'required|max:1',
            'AW_EMAIL' => 'nullable|email|max:100',
            'AW_NOMOR_HP' => 'nullable|max:13',
            'AW_TEMPAT_LAHIR' => 'required|max:100',
            'AW_TANGGAL_LAHIR' => 'required|max:15',
            'AW_FOTO_KTP' => 'required',
        ], $custom_validator);
        if ($validator->fails()) 
            return $validator->errors();

        if (!App::environment('local') && !is_null($kdproduk)) { // if not local
            try{
                $api_bni = new BnilifeApi();
                $token = $api_bni->getTokenBniL();
                if ($token === false) {
                    $token = $this->getTokenBniL();
                    if ($token === false)
                        return 'ERROR TOKEN';
                }
                $this->token_bearer = $token;
                $checkKTP = $api_bni->checkKtp($request->input('NIK'), $kdproduk, $token);
                if($checkKTP->CODE != "60")
                    return ["NIK" => ["Nomor KTP tidak valid"]];
                    
            } catch (\Throwable $e) {
                report($e);
                return response()->json(array('message' => 'ERROR'), 500);
            }
        }
        return "00";
    }

    public function getPlan(Request $request)
    {
        $product_id = $request->input('product_id');
        $usia = $request->input('usia');
        $waktu = $request->input('waktu');
        $plan = $request->input('plan');
        $product = Product::find(Crypt::decrypt($product_id));
        if (!empty($product)) {
            $plan_id = $product->plans->pluck('id');
            $plan_detail = "";
            $table_name = "";
            $plan_manfaat = "";
            $plan_detail = PlanDetail::select('id', 'ID_PLAN', 'MONTHLY', 'THREE_MONTH', 'SIX_MONTH', 'ANNUALY', 'USIA_MIN', 'USIA_MIN')->whereIn('ID_PLAN', $plan_id);
            if ($usia) {
                $usia = Carbon::parse($usia)->diff(Carbon::now())->format('%y');
                $plan_detail = $plan_detail->where('USIA_MIN', '<=', $usia)->where('USIA_MAX', '>=', $usia);
            }
            if ($waktu) {
                $number_value = $this->intervalPlantoNumber($product->plans);
                $table_name = $number_value[$waktu];
            }
            if ($plan_id) {
                $plan_manfaat = Manfaat::select('ID_PLAN', 'MANFAAT', 'DESC')->whereIn('ID_PLAN', $plan_id)->get();
            }
            $plan_result = array();
            foreach ($plan_detail->get() as $val) {
                $plan_result[] = array(
                    'id' => Crypt::encrypt($val->id),
                    'ID_PLAN' => $val->ID_PLAN,
                    'MONTHLY' => $val->MONTHLY,
                    'THREE_MONTH' => $val->THREE_MONTH,
                    'SIX_MONTH' => $val->SIX_MONTH,
                    'ANNUALY' => $val->ANNUALY,
                    'USIA_MIN' => $val->USIA_MIN,
                    'USIA_MIN' => $val->USIA_MIN,
                );
            }
            $result = array(
                "plan" => $plan_result,
                "table_name" => $table_name,
                "plan_manfaat" => $plan_manfaat,
            );
            // dd($result);
            return response()->json(array('message' => $result, 'CODE' => '200'), 200);
        } else {
            return response()->json(array('message' => 'ERROR', 'CODE' => '404'), 200);
        }
    }

    public function intervalPlantoNumber($plans)
    {
        $temp_result = array(
            'MONTHLY' => true,
            'THREE_MONTH' => true,
            'SIX_MONTH' => true,
            'ANNUALY' => true,
        );
        foreach ($plans as $plan) {
            foreach ($plan->plan_details as $plan_detail) {
                if (empty($plan_detail->MONTHLY) || is_null($plan_detail->MONTHLY)) {
                    $temp_result['MONTHLY'] = false;
                }
                if (empty($plan_detail->THREE_MONTH) || is_null($plan_detail->THREE_MONTH)) {
                    $temp_result['THREE_MONTH'] = false;
                }
                if (empty($plan_detail->SIX_MONTH) || is_null($plan_detail->SIX_MONTH)) {
                    $temp_result['SIX_MONTH'] = false;
                }
                if (empty($plan_detail->ANNUALY) || is_null($plan_detail->ANNUALY)) {
                    $temp_result['ANNUALY'] = false;
                }
            }
        }
        $count = 1;
        if ($temp_result['MONTHLY']) {
            $result[$count++] = 'MONTHLY';
        }
        if ($temp_result['THREE_MONTH']) {
            $result[$count++] = 'THREE_MONTH';
        }
        if ($temp_result['SIX_MONTH']) {
            $result[$count++] = 'SIX_MONTH';
        }
        if ($temp_result['ANNUALY']) {
            $result[$count++] = 'ANNUALY';
        }

        return $result;
    }

    private function uploadImage($image, $filename, $folder_name)
    {
        if(!empty($image)){
            $extension = explode('/', mime_content_type($image))[1];
            $image = str_replace('data:image/'.$extension.';base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $dirDate = date("Y") . '/' . date("m") . '/' . date("d");
            $dirLocation = 'assets/ktp/' . $folder_name . '/' . $dirDate . '/';
            $filePath = $dirLocation . $filename . '.'.$extension;
            Storage::disk('public')->put($filePath, base64_decode($image), 'public'); 
            return $filePath;
        }else{
            return null;
        }
    }
}
