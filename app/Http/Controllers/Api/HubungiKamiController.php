<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HubungiKamiController extends Controller
{
    public function store(Request $request)
    {
        try {
            $response = (new \ReCaptcha\ReCaptcha(env('RECAPTCHA_SECRET_KEY')))
                ->setExpectedAction('hubungi_kami')
                ->verify($request->input('_recaptcha'), $request->ip());
            if (!$response->isSuccess()) {
                return response()->json(array('message' => 'FORBIDDEN rECAPTCHA'), 500);
            }
            $custom_validator = array(
                'required' => 'Silahkan melengkapi data',
                'email' => 'Format email tidak benar',
                'max' => 'Melebihi maksimal kata',
            );
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:100',
                'email' => 'required|email|max:100',
                'alamat' => 'required|max:250',
                'phone' => 'required|max:15',
                'pesan' => 'required|max:300',
            ], $custom_validator);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors(), 'code' => "403"], 200);
            }

            $hubungi_kami = new ContactUs();
            $hubungi_kami->NAME = $request->input('name');
            $hubungi_kami->EMAIL = $request->input('email');
            $hubungi_kami->ADDRESS = $request->input('alamat');
            $hubungi_kami->PHONE = $request->input('phone');
            $hubungi_kami->MESSAGE = $request->input('pesan');
            $hubungi_kami->save();
            return response()->json(array("MESSAGE" => 'OK', 'CODE' => '20'), 200);
        } catch (\Throwable $e) {
            report($e);
            return response()->json(array('message' => 'ERROR'), 500);
        }
    }
}
