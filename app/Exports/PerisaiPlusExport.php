<?php

namespace App\Exports;

use App\Models\PerisaiPlus;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class PerisaiPlusExport implements FromView, WithColumnFormatting
{
    public $ids;
    public $perisai_plus;

    function __construct()
    {
        $this->perisai_plus = PerisaiPlus::select('id','ACTIVATION_DATE','NAMA_NASABAH','NOMOR_HP','nomor_kartu')
                                ->where('status', 1)->get();
    }
    
    public function view(): View
    {
        $perisai_plus = $this->perisai_plus;
        $this->ids = [];
        $data = [];
        foreach ($perisai_plus as $value) {
            $data[] = array(
                'ACTIVATION_DATE' => date("dmY", strtotime($value->ACTIVATION_DATE)),
                'NAMA_NASABAH' => $value->NAMA_NASABAH,
                'NOMOR_HP' => preg_replace('/^(?:\+?27|0)?/','+62', $value->NOMOR_HP),
                'NOMOR_KARTU' => $value->nomor_kartu
            );
            $this->ids[] = $value->id;
        }
        $this->count_data = $data;
        return view('excel.perisai-plus-act', compact('data'));
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_TEXT,
            'D' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function updateExported(){
        DB::table('perisai_pluses')->whereIn('id', $this->ids)->update(['STATUS' => 0]);
    }
}
