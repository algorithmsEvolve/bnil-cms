<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingParameter extends Model
{
    protected $primaryKey = "SETTING_ID";
    public $incrementing = false;
}
