<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Exports\PerisaiPlusExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class SendPerisaiPlusGfx extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:perisaiplusgfx';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a daily excel file Perisai Plus to Gfx';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $date_now = Carbon::now()->isoFormat('DDMMYY');
            $perisai_plus_export = new PerisaiPlusExport;
            if(count($perisai_plus_export->perisai_plus) > 0){
                if (Storage::disk('sftp')->exists('outbox/Data Aktivasi PP Digital_'.$date_now.'.xlsx')) { // cek apakah file sudah ada sebelumnya
                    Log::channel('cron')->info('Canceled scheduler PerisaiPlusExport (File exist)');
                }else{
                    $excel = Excel::store($perisai_plus_export, 'outbox/Data Aktivasi PP Digital_'.$date_now.'.xlsx', 'sftp'); // write file excel
                    if($excel){
                        if (Storage::disk('sftp')->exists('outbox/Data Aktivasi PP Digital_'.$date_now.'.xlsx')) { // cek lg apakah sudah terwrite filenya
                            $perisai_plus_export->updateExported();
                        }   
                    }
                    Log::channel('cron')->info('Running scheduler PerisaiPlusExport ['.$excel.']');
                    Log::channel('cron')->info(json_encode(($perisai_plus_export->perisai_plus)));
                    return $excel;
                }
            }else{
                Log::channel('cron')->info('Canceled scheduler PerisaiPlusExport (Data export empty)');
            }
        }catch (\Throwable $e) {
            report ($e);
        }
    }
}
