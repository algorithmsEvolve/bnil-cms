<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Library\Helpers\ApiHelper;

class SendOtp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $url;
    protected $header;
    protected $post;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $header, $post)
    {
        $this->url = $url;
        $this->header = $header;
        $this->post = $post;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $api_helper = new ApiHelper();
        $api_helper->respApi($this->url, $this->header, $this->post);    
    }
}
