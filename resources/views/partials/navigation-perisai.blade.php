<script>
    window.addEventListener('scroll', (e) => {
        if (scrollY > 150) {
            $("#navigation").addClass('navigation--background-white')
            $(".color").show()
            $(".black-white").hide()
        } else {
            $("#navigation").removeClass('navigation--background-white')
            $(".color").hide()
            $(".black-white").show()
        }
    })
</script>

<div id="navigation" class="navigation--transparent">
    <div class="navigation--desktop">
        <div class="container">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="row logo">
                            <img class="black-white" src="{{ url('images/additional/logo.png') }}" alt="Logo Lifeplan">
                            <img style="display: none;" class="color" src="{{ url('images/additional/bni-life-color-logo.svg') }}" alt="Logo Lifeplan">
                        </div>
                    </div>
                    <div class="col-lg-8 align-items-center justify-content-center">
                        <div class="row">
                            @if (Route::current()->getName()!="front.perisai_plus")
                            <nav>
                                <ul>
                                    <li><a class="font-size-14 font-weight-600" href="/about">Tentang Kami</a></li>
                                    <li><a class="font-size-14 font-weight-600" href="/product">Produk</a></li>
                                    <li><a class="font-size-14 font-weight-600" href="/service">Layanan</a></li>
                                    <li><a class="font-size-14 font-weight-600" href="/contact">Hubungi</a></li>
                                    <li>
                                        <button class="font-size-14 font-weight-100 button--login">Masuk</button>
                                    </li>
                                </ul>
                            </nav>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-2 align-items-center justify-content-flex-end">
                        <div class="row">
                            <a href="http://www.bni-life.co.id">
                                <img class="black-white" src="{{ url('images/icons/logo-bni-header.svg') }}" alt="Logo BNI">
                                <img style="display: none;" class="color" src="{{ url('images/additional/bni-color-logo.svg') }}" alt="Logo Lifeplan">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="navigation--mobile">
        <div class="container">
            <div class="justify-content-space-between align-items-center">
                <!-- <div class="icons">
                    <img crc="{{ url('images/icons/icon-menu-white.svg') }}" alt="Menu Icon">

                </div> -->
                <div class="logo">
                    <img class="black-white" src="{{ url('images/additional/logo.png') }}" alt="Logo BNI">
                    <img class="color" style="display: none;" class="color" src="{{ url('images/additional/bni-life-color-logo.svg') }}" alt="Logo Lifeplan">
                </div>
                <div></div>
            </div>
        </div>
    </div>
</div>