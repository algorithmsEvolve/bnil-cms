<script>
    $(document).ready(() => {
        $(".menu").click(() => {
            $(".navigation--slide").animate({
                marginLeft: '0%'
            }, 500)
        })

        $(".close").click(() => {
            $(".navigation--slide").animate({
                marginLeft: '-100%'
            }, 500)
        })

        $(".action--bottom").click(() => {
            $(".navigation--slide").animate({
                marginLeft: '-100%'
            }, 500)
        })

        $("#content-register").hide()

        $(".tab-register").on('click', () => {
            $("#content-register").show()
            $("#content-login").hide()
            $(".tab-register").addClass('active-tab')
            $(".tab-login").removeClass('active-tab')
        })

        $(".tab-login").on('click', () => {
            $("#content-login").show()
            $("#content-register").hide()
            $(".tab-login").addClass('active-tab')
            $(".tab-register").removeClass('active-tab')
        })
    })
</script>

<div id="navigation" class="navigation--white">
    <div class="navigation--desktop">
        <div class="container">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-2 align-items-center">
                        <a class="row logo" href="/">
                            <img class="color" src="{{ url('images/additional/bni-life-color-logo.svg') }}" alt="Logo Lifeplan">
                        </a>
                    </div>
                    <div class="col-lg-8 align-items-center justify-content-center">
                        <div class="row">
                            <nav>
                                <ul>
                                    <li><a class="font-size-14 font-weight-600" href="/about">Tentang Kami</a></li>
                                    <li class="dropdown">
                                        <a class="font-size-14 font-weight-600 mr-2" href="/product">Produk</a>
                                        <img src="{{ url('/images/icons/icon-dropdown.svg') }}" alt="Icon Dropdown">
                                        <div class="menu-dropdown">
                                            <ul>
                                                <li><a href="{{route('produk.single_category', 'perjalanan')}}">Rencanan Perjalanan</a></li>
                                                {{-- <li><a href="#">Paket Kesehatan</a></li> --}}
                                            </ul>
                                        </div>
                                    </li>
                                    <li><a class="font-size-14 font-weight-600" href="/service">Layanan</a></li>
                                    <li><a class="font-size-14 font-weight-600" href="/contact">Hubungi</a></li>
                                    <li>
                                        <button class="font-size-14 font-weight-100 button--login" data-bs-toggle="modal" data-bs-target="#login">Masuk</button>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-lg-2 align-items-center justify-content-flex-end">
                        <div class="row">
                            <img class="color" src="{{ url('images/additional/bni-color-logo.svg') }}" alt="Logo Lifeplan">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal login -->
    <div class="modal fade" id="login" tabindex="-1" aria-labelledby="loginLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="tabs">
                    <div class="tab tab-login active-tab">
                        <p class="m-0">Masuk</p>
                    </div>
                    <div class="tab tab-register">
                        <p class="m-0">Daftar</p>
                    </div>
                </div>

                <div class="container">
                    <div id="content-login">
                        <p class="text-center mb-3 font-color-orient font-size-24 mt-3">Masuk</p>
                        <div class="input-group mb-3">
                            <img src="{{ url('images/icons/icon-username.svg') }}" alt="Icon Username">
                            <input type="text" class="form-control" placeholder="Nama Pengguna atau Email">
                        </div>

                        <div class="input-group mb-3">
                            <img src="{{ url('images/icons/icon-password.svg') }}" alt="Icon Username">
                            <input type="password" class="form-control" placeholder="Password">
                        </div>

                        <p class="font-size-17 font-color-orient text-center">Lupa Password?</p>

                        <button class="mt-2 btn btn-action mb-4">Masuk</button>
                    </div>
                    <div id="content-register">
                        <p class="text-center mb-3 font-color-orient font-size-24 mt-3">Daftar</p>
                        <div class="input-group mb-3">
                            <img src="{{ url('images/icons/icon-username.svg') }}" alt="Icon Username">
                            <input type="text" class="form-control" placeholder="Nama Pengguna">
                        </div>

                        <div class="input-group mb-3">
                            <img src="{{ url('images/icons/icon-email.svg') }}" alt="Icon Email">
                            <input type="text" class="form-control" placeholder="Email">
                        </div>

                        <div class="input-group mb-3">
                            <img src="{{ url('images/icons/icon-password.svg') }}" alt="Icon Password">
                            <input type="password" class="form-control" placeholder="Password">
                        </div>

                        <div class="form-check ml-3 my-4">
                            <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                            <label class="form-check-label font-size-12 font-color-orient" for="flexRadioDefault1">
                                Saya bersedia mendapatkan pesan promosi <br />
                                atau artikel dari BNI Plan BLife ke email saya
                            </label>
                        </div>

                        <button class="mt-2 btn btn-action mb-3">Buat akun Plan BLife kamu</button>

                        <p class="font-size-17 font-color-orient text-center mb-4">Sudah punya akun? Masuk</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="navigation--mobile">
        <div class="container">
            <div class="justify-content-space-between align-items-center">
                <div class="logo">
                    <img src="{{ url('images/additional/bni-life-color-logo.svg') }}" alt="Logo BNI" width="80">
                </div>

                <div class="icons">
                    <img src="{{ url('images/icons/search-icon.svg') }}" alt="Menu Icon">
                    <img class="ml-3 menu" src="{{ url('images/icons/menu-icon.svg') }}" alt="Menu Icon">
                </div>
            </div>
        </div>
    </div>

    <div class="navigation--slide">
        <div class="title">
            <p class="font-size-18">Menu</p>
            <p class="close">X</p>
        </div>

        <nav>
            <ul>
                <li class="font-size-17">
                    <a href="/about">Tentang Kami</a>
                </li>
                <li class="font-size-17 dropdown">
                    <a data-bs-toggle="collapse" href="#product" role="button" aria-expanded="false" aria-controls="product">
                        Produk
                    </a>
                    <div class="collapse" id="product">
                        <ul>
                            <li><a href="#">Paket Perjalanan</a></li>
                            <li><a href="#">Paket Pendidikan</a></li>
                            <li><a href="#">Paket Kesehatan</a></li>
                        </ul>
                    </div>
                </li>
                <li class="font-size-17">
                    <a href="/service">Layanan</a>
                </li>
                <li class="font-size-17">
                    <a href="/contact">Hubungi Kami</a>
                </li>
            </ul>
        </nav>

        <div class="seperate-user">
            <p class="font-size-15">Hallo, John Doe</p>
        </div>

        <nav>
            <ul>
                <li class="font-size-17">
                    <img src="{{ url('/images/icons/icon-menu-home.svg') }}" alt="Icon Profil">
                    <p>Profil</p>
                </li>
                <li class="font-size-17">
                    <img src="{{ url('/images/icons/icon-menu-claim.svg') }}" alt="Icon Claim">
                    <p>Klaim</p>
                </li>
                <li class="font-size-17">
                    <img src="{{ url('/images/icons/icon-menu-polish.svg') }}" alt="Icon Polish">
                    <p>E-Polish</p>
                </li>
                <li class="font-size-17">
                    <img src="{{ url('/images/icons/icon-menu-payment.svg') }}" alt="Icon Pembayaran">
                    <p>Pembayaran</p>
                </li>
            </ul>
        </nav>

        <div class="action--bottom" data-bs-toggle="modal" data-bs-target="#login">
            <p class="font-size-15">Masuk</p>
        </div>
    </div>
</div>