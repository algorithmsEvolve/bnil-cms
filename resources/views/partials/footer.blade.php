<footer id="footer" class="footer-v2">
    <div class="container-fluid">
        <div class="col-lg-12 footer--informations">
            <div class="footer--information">
                <div class="title--footer">
                    <p class="text-transform-uppercase font-weight-700 font-size-16 m-0 mb-1">
                        KANTOR PUSAT
                    </p>
                </div>
                <div class="display-flex align-items-center">
                    <div class="information--icon">
                        <img src="{{ url('/images/v2/icons/location-footer.svg') }}" alt="Icon Location">
                    </div>

                    <div class="information--text">
                        <a href="https://www.google.co.id/maps/place/PT.+BNI+Life+Insurance/@-6.223084,106.7997075,14z/data=!4m9!1m2!2m1!1spt.+bni+life+insurance!3m5!1s0x2e69f14db2cefaa5:0x75e9652c146ec124!8m2!3d-6.2305323!4d106.8207731!15sChZwdC4gYm5pIGxpZmUgaW5zdXJhbmNlIgOIAQFaLgoVcHQgYm5pIGxpZmUgaW5zdXJhbmNlIhVwdCBibmkgbGlmZSBpbnN1cmFuY2WSARFpbnN1cmFuY2VfY29tcGFueQ" style="color:#ffffff;text-decoration: none;">
                            <p class="font-weight-400 font-size-12 m-0">
                                Centennial Tower, Lantai 9 <br /> Jl. Gatot Subroto Kav 24-25, Jakarta 12930, Indonesia
                            </p>
                        </a>
                    </div>
                </div>
            </div>

            <div class="footer--information">
                <div class="title--footer">
                    <p class="text-transform-uppercase font-weight-700 font-size-16 m-0 mb-1">
                        PUSAT INFORMASI
                    </p>
                </div>

                <div class="display-flex align-items-center">
                    <div class="information--icon">
                        <img src="{{ url('/images/v2/icons/information-footer.svg') }}" alt="Icon Phone">
                    </div>

                    <div class="information--text">
                        <p class="font-weight-700 font-size-30 m-0 line-height-35">
                            1-500-045
                        </p>
                    </div>
                </div>
            </div>

            <div class="footer--information">
                <div class="title--footer">
                    <p class="text-transform-uppercase font-weight-700 font-size-16 m-0 mb-1">
                        IKUTI KAMI
                    </p>
                </div>

                <div class="display-flex align-items-center">
                    <a target="_blank" style="color: white;" href="https://www.facebook.com/BNILifeID/"><img class="mr-3" src="{{ url('/images/v2/icons/socials/facebook-footer.svg') }}" alt="Icon Facebook"></a>
                    <a target="_blank" style="color: white;" href="https://twitter.com/bnilifeid"><img class="mr-3" src="{{ url('/images/v2/icons/socials/twitter-footer.svg') }}" alt="Icon Twitter"></a>
                    <a target="_blank" style="color: white;" href="https://www.youtube.com/channel/UCFBKohX52ePqnFRMwm56saQ"><img class="mr-3" src="{{ url('/images/v2/icons/socials/youtube-footer.svg') }}" alt="Icon Youtube"></a>
                    <a target="_blank" style="color: white;" href="https://www.instagram.com/bnilifeid/"><img class="mr-3" src="{{ url('/images/v2/icons/socials/instagram-footer.svg') }}" alt="Icon Instagram"></a>
                    <a target="_blank" style="color: white;" href="https://www.linkedin.com/company/bni-life/mycompany/"><img class="mr-3" src="{{ url('/images/v2/icons/socials/linkedin-footer.svg') }}" alt="Icon Linkedin"></a>
                </div>
            </div>

            <p class="only-show-mobile footer--copyright font-size-11 font-weight-400 width-100 margin-0">BNI Life
                © {{date('Y')}}. Hak cipta dilindungi undang-undang. BNI Life terdaftar dan diawasi oleh
                Otoritas Jasa Keuangan. <br />
            </p>
        </div>

        <div class="line"></div>

        <div class="col-lg-12 footer--menu">
            <div class="row">
                <div class="col-lg-5">
                    <div class="row">
                        <a href="https://www.bni-life.co.id/syarat-ketentuan" class="menu--link font-size-11 font-weight-400 font-color-white margin-0">Syarat
                            dan Ketentuan</a>
                        <a href="https://www.bni-life.co.id/kebijakan-privasi" class="menu--link font-size-11 font-weight-400 font-color-white margin-0">Kebijakan Privasi</a>
                    </div>
                </div>
                <div class="col-lg-7 text-right">
                    <div class="row">
                        <p class="only-show-desktop footer--copyright font-size-11 font-weight-400 width-100 margin-0">BNI Life
                            © {{date('Y')}}. Hak cipta dilindungi undang-undang. BNI Life terdaftar dan diawasi oleh
                            Otoritas Jasa Keuangan.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>