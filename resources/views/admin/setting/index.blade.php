@extends('admin.layouts.master')

@section('title') Setting @endsection

@section('content')

@component('common-components.breadcrumb')
@slot('title') Setting @endslot
@endcomponent

@section('style')

@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form action="{{route('admin.settings.store')}}" method="POST" enctype="multipart/form-data">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                @foreach ($settings as $setting)
                                @php
                                $type="text";
                                $value=$setting->SETTING_VALUE;
                                $name=$setting->SETTING_ID;
                                if($setting->SETTING_TYPE=='PDF'){
                                $type="file";
                                $value="";
                                }
                                $input = '<input type="'.$type.'" name="'.$name.'" value="'.$value.'" class="form-control">';
                                @endphp
                                <td>
                                    <label for="">{{$setting->SETTING_DESC}}</label>
                                </td>
                                <td>
                                    {!!$input!!}
                                </td>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end row -->
@endsection