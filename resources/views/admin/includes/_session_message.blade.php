@if(Session::has('alert-success'))
    <div class="alert alert-primary" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="alert-body">{!! Session::get('alert-success')  !!}</div>
    </div>
@endif
@if(Session::has('alert-error'))
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>    
        <div class="alert-body">
            {!! Session::get('alert-error')  !!}
        </div>
    </div>
@endif