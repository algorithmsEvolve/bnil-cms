@extends('admin.layouts.master')

@section('title') Terms and Conditions @endsection

@section('content')

@component('common-components.breadcrumb')
@slot('title') Terms and Conditions @endslot
@endcomponent

@section('style')

@endsection

<form action="{{route('admin.terms.store' , $data->id)}}" method="post" enctype="multipart/form-data">
<div class="row">
    @csrf
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                @include('admin.includes._session_message')
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <label for="">Detail</label>
                        <textarea class="form-control" id="detail" placeholder="Deskripsi" rows="3" name="detail" required> 
                            {{$data->DETAIL}}
                        </textarea>
                        @if($errors->has('detail'))
                            <span class="help-block" style="color : red">{{$errors->first('detail')}}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <button type="submit" class="btn btn-primary btn-block">SIMPAN</button>
                <!-- <a href="" class="btn btn-warning btn-block">KEMBALI</a> -->
            </div>
        </div>
    </div>
</div>
</form>

<!-- end row -->
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>

<script>
    $(document).ready(function() {
        CKEDITOR.replace('detail', {
            customConfig: '/assets/plugins/ckeditor/config.js'
        });
    })
</script>
@endsection