@extends('admin.layouts.master')

@section('title') About Us @endsection

@section('content')

@component('common-components.breadcrumb')
@slot('title') About Us @endslot
@endcomponent

@section('style')

@endsection

<form action="{{route('admin.about.store' , $data->id)}}" method="post" enctype="multipart/form-data">
<div class="row">
    @csrf
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                @include('admin.includes._session_message')
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <label for="">Title</label>
                        <input type="text" name="title" id="" value="{{$data->TITLE}}" class="form-control" required>
                        @if($errors->has('title'))
                            <span class="help-block" style="color : red">{{$errors->first('title')}}</span>
                        @endif
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <label for="">Quote</label>
                        <textarea class="form-control" id="quote" placeholder="Deskripsi" rows="3" name="qoute" required> 
                            {{$data->QOUTE}}
                        </textarea>
                        @if($errors->has('qoute'))
                            <span class="help-block" style="color : red">{{$errors->first('qoute')}}</span>
                        @endif
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <label for="">Description</label>
                        <textarea class="form-control" id="description" placeholder="Deskripsi" rows="3" name="desc" required> 
                            {{$data->DESC}}
                        </textarea>
                        @if($errors->has('desc'))
                            <span class="help-block" style="color : red">{{$errors->first('desc')}}</span>
                        @endif
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <label for="">Background Banner</label>
                        <input type="file" name="images[0]" id="" class="form-control" >
                        <img style="width: 100px" src="{{ url('/aboutus/image0.jpg') }}" onerror="this.onerror=null;this.src='{{ url('/images/noimage/no_image.jpeg') }}';" >
                        @if($errors->has('images.0'))
                            <span class="help-block" style="color : red">Extension must be .jpg, Max File 2MB</span>
                        @endif
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <label for="">Image</label>
                        <input type="file" name="images[1]" id="" class="form-control" >
                        <img style="width: 100px" src="{{ url('/aboutus/image1.jpg') }}" onerror="this.onerror=null;this.src='{{ url('/images/noimage/no_image.jpeg') }}';">
                        @if($errors->has('images.1'))
                            <span class="help-block" style="color : red">Extension must be .jpg, Max File 2MB</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <button type="submit" class="btn btn-primary btn-block">SIMPAN</button>
                <!-- <a href="" class="btn btn-warning btn-block">KEMBALI</a> -->
            </div>
        </div>
    </div>
</div>
</form>

<!-- end row -->
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>

<script>
    $(document).ready(function() {
        CKEDITOR.replace('description', {
            customConfig: '/assets/plugins/ckeditor/config.js'
        });

        CKEDITOR.replace('quote', {
            customConfig: '/assets/plugins/ckeditor/config.js'
        });
    })
</script>
@endsection