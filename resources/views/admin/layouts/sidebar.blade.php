<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>
                
                {{-- <li>
                    <a href="{{ route('admin.perisai-plus') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.perisai-plus') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Banner</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.perisai-plus') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Kirim E-Polis</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.perisai-plus') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Transaksi</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.perisai-plus') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Nasabah</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Pages</span>
                    </a>
                    <ul class="sub-menu mm-collapse" aria-expanded="false" style="height: 0px;">
                        <li><a href="email-inbox.html">Tentang Kami</a></li>
                        <li><a href="email-inbox.html">Layanan</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Promosi</span>
                    </a>
                    <ul class="sub-menu mm-collapse" aria-expanded="false" style="height: 0px;">
                        <li><a href="email-inbox.html">Produk</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('admin.perisai-plus') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">FAQ</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.perisai-plus') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Testimoni</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Produk</span>
                    </a>
                    <ul class="sub-menu mm-collapse" aria-expanded="false" style="height: 0px;">
                        <li><a href="email-inbox.html">Kategori</a></li>
                        <li><a href="email-inbox.html">Produk</a></li>
                        <li><a href="email-read.html">Plan</a></li>
                    </ul>
                </li> --}}
                <li>
                    <a href="{{ route('admin.perisai-plus') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Perisai Plus</span>
                    </a>
                </li>
                {{-- <li>
                    <a href="{{ route('admin.hubungi-kami') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Hubungi Kami</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.hubungi-kami') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Mitra Kami</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.hubungi-kami') }}">
                        <i class="mdi mdi-view-list"></i>
                        <span href="layouts-horizontal">Video Panduan</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.hubungi-kami') }}">
                        <i class="mdi mdi-settings"></i>
                        <span href="layouts-horizontal">Statistik</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('admin.settings.index') }}">
                        <i class="mdi mdi-settings"></i>
                        <span href="layouts-horizontal">Settings</span>
                    </a>
                </li> --}}

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->