@extends('admin.layouts.master')

@section('title') Hubungi Kami @endsection

@section('content')

@component('common-components.breadcrumb')
@slot('title') Hubungi Kami @endslot
@endcomponent

@section('style')
<link rel="stylesheet" href="{{asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
@endsection

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                <div class="row mb-3">
                    <div class="col-lg-4">
                        <label for="satrtDate">Tanggal Mulai</label>
                        <input type="date" class="form-control" id="startDate">
                    </div>
                    <div class="col-lg-4">
                        <label for="satrtDate">Tanggal Akhir</label>
                        <input type="date" class="form-control" id="endDate">
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table data-table">
                        <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No. Telpon</th>
                                <th>Alamat</th>
                                <th>Pesan</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end row -->
@endsection

@section('scripts')
<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/colreorder/1.5.4/js/dataTables.colReorder.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.0.3/js/dataTables.dateTime.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script>
    $(document).ready(function() {
        const startDate = $("#startDate").val();
        const endDate = $("#endDate").val();

        function loadData(startDate, endDate) {
            $('.data-table').dataTable({
                "bLengthChange": true,
                "searching": true,
                "processing": true,
                "serverSide": true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        'copy',
                        'excel',
                        'csv',
                        'print'
                    ]
                }],
                "ajax": {
                    url: `{{route('admin.hubungi-kami.datatables')}}`,
                    data: {
                        startDate,
                        endDate
                    }
                },
                "columns": [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'NAME',
                        name: 'NAME'
                    },
                    {
                        data: 'EMAIL',
                        name: 'EMAIL'
                    },
                    {
                        data: 'PHONE',
                        name: 'PHONE'
                    },
                    {
                        data: 'ADDRESS',
                        name: 'ADDRESS'
                    },
                    {
                        data: 'MESSAGE',
                        name: 'MESSAGE'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                ],
            });
        }

        loadData()

        $("#startDate").change(() => {
            const startDate = $("#startDate").val();
            const endDate = $("#endDate").val();

            $('.data-table').DataTable().destroy();
            loadData(startDate, endDate)
        })

        $("#endDate").change(() => {
            const startDate = $("#startDate").val();
            const endDate = $("#endDate").val();

            $('.data-table').DataTable().destroy();
            loadData(startDate, endDate)
        })
    })
</script>
@endsection