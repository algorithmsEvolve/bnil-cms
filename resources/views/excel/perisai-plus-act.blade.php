<table>
  <thead>
  <tr>
      <th>Tanggal Aktivasi</th>
      <th>Nama Pemegang Kartu</th>
      <th>No HP Pemegang Kartu</th>
      <th>No. Kartu Kredit</th>
  </tr>
  </thead>
  <tbody>
  @foreach($data as $item)
      <tr>
          <td>{{ $item['ACTIVATION_DATE'] }}</td>
          <td>{{ $item['NAMA_NASABAH'] }}</td>
          <td>{{ $item['NOMOR_HP'] }}</td>
          <td>{{ $item['NOMOR_KARTU'] }}</td>
      </tr>
  @endforeach
  </tbody>
</table>