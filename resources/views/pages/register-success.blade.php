@extends('layouts/index')

@section('title', 'register')

@section('css')
<link href="{{ url('/css/pages/register.css') }}" rel="stylesheet">
@endsection

@section('js')
<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{env('CLIENTKEY_MIDTRANS')}}"></script>
<script type="text/javascript">
    $(function() {
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        });
    });
</script>
<script>
    $(document).ready(function() {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());

        if (params.email) $("#email").html(params.email)
        if (params.name) $("#name").html(params.name)
    })
</script>
@endsection

@section('topNavigation')
@include('../partials/navigation')
@endsection

@section('bottomNavigation')
@include('../partials/footer')
@endsection

@section('content')
<div id="register">
    <div class="banner" style="background: url('{{asset('images/banners/banner-register.svg')}}')">
        <div class="banner-text">
            <div class="steps">
                <div class="step step-riwayat">
                    <!-- <span class="arrow">V</span> -->
                    <span id="menu-text-1" class="text active">Riwayat Kesehatan</span>
                </div>
                <div class="seperate step-riwayat"></div>
                <div class="step"><span id="menu-text-2" class="text active">Data Diri</span></div>
                <div class="seperate"></div>
                <div class="step"><span id="menu-text-3" class="text active">Review Data</span></div>
                <div class="seperate"></div>
                <div class="step"><span id="menu-text-4" class="text active">Selesai</span></div>
            </div>
        </div>
    </div>

    <div class="form-absolute">
        <div id="step-4">
            <div class="card text-center">
                <div class="image">
                    <img src="{{ url('images/icons/success-register.svg') }}" alt="Success Register Icon">
                    <p class="font-size-25 font-color-orient">Selamat <span id="name">John Doe</span>, Kamu Berhasil <br /> Merencanakan Proteksi untuk Perjalananmu!</p>
                    <p class="font-size-20">Kami akan mengirimkan bukti email bukti transaksi <br /> beserta dokumen e-polish kamu segera ke</p>
                    <p class="font-size-25" id="email">johndoe@email.com</p>
                </div>
            </div>

            <div class="text-center">
                <div class="button btn btn-prev">Verifikasi Email Kamu</div>
                <div class="button btn btn-next">Lihat Profil Kamu</div>
            </div>
        </div>
    </div>
</div>
@endsection