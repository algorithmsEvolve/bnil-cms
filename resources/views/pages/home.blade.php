@extends('layouts/index')

@section('title', 'home')

@section('css')
<link href="{{ url('/css/pages/home.css') }}" rel="stylesheet">
<link href="{{ url('/css/pages-v2/home.css') }}" rel="stylesheet">
<link href="{{ url('/css/pages/responsive/home.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
@endsection

@section('js')
<script>
    var rECAPTCHA_SITE_KEY = "{{ env('RECAPTCHA_SITE_KEY') }}";
</script>
<script src="https://www.google.com/recaptcha/api.js?render={{env('RECAPTCHA_SITE_KEY')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{ url('/js/pages/home.js') }}"></script>
<script src="{{ url('/js/pages/contact.js') }}"></script>
@endsection

@section('topNavigation')
@include('../partials/navigation')
@endsection

@section('bottomNavigation')
@include('../partials/footer')
@endsection

@section('content')
<div id="home">
    <div class="banner">
        <div class="arrows">
            <div class="arrow arrow-left">
                <img src="{{ url('images/icons/banner-arrow-previous.svg') }}" alt="Arrow Left">
            </div>

            <div class="arrow arrow-right">
                <img src="{{ url('images/icons/banner-arrow-next.svg') }}" alt="Arrow Right">
            </div>
        </div>

        <div class="banner-container">
            <div class="banner-scroll">
                <div class="banner-content">
                    <div class="banner-item">
                        <div class="banner-image">
                            <img src="{{ url('/images/banners/banner-1.jpeg')  }}" />
                        </div>

                        <div class="banner-text">
                            <p class="font-size-44 font-color-orient">Jadikan <b>Perjalanan</b> Aman dan Menyenangkan</p>
                            <button class="btn btn-show-more only-show-desktop">
                                <p class="mb-0">Dapatkan Sekarang</p>
                                <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="">
                            </button>
                        </div>

                        <div class="banner-slide-point">
                            <p>01</p>
                            <div class="banner-slide-piont__line">
                                <div class="banner-slide-piont__line--available"></div>
                                <div class="banner-slide-piont__line--active"></div>
                            </div>
                            <p>04</p>
                        </div>

                        <button class="btn btn-show-more only-show-mobile">
                            <p class="mb-0">Dapatkan Sekarang</p>
                            <img src="{{ url('images/icons/icon-right-row.svg') }}" alt="">
                        </button>
                    </div>
                </div>

            </div>

            <div class="bullets">
            </div>
        </div>
    </div>

    <div class="packet">
        <p class="font-size-20 mb-0 letter-spacing-3 text-center">TAHAP KEHIDUPAN</p>
        <p class="font-size-50 text-center">Pelajari Kebutuhanmu</p>


        <div class="packet-steps">
            <div class="packet-steps-contents">
                <div class="packet-steps-content">
                    <div class="packet-steps-content__story">
                        <div class="packet-steps-content__story--border-top"></div>
                        <div class="packet-steps-content__story--title">
                            <div class="packet-steps-content__story--title__icon">
                                <img src="{{ url('images/icons/steps/icon-step-1-nonactive.svg') }}">
                            </div>
                            <div class="packet-steps-content__story--title__text">
                                <p class="font-size-15 mb-0">
                                    Hi, aku Syifa saat ini aku sedang menikmati awal masa kerjaku
                                </p>
                            </div>
                        </div>
                        <hr>
                        <div class="packet-steps-content__story--content">
                            <p class="font-size-15 mb-0">
                                “Di usia produktifku ini, karena mobilitasku yang tinggi mulai dari keseharian pulang-pergi kantor dan juga hobi traveling sehingga penting bagiku untuk merencanakan perjalanan dengan perlindungan terbaik. Biar lebih tenang, aku beli asuransi perjalanan”
                            </p>
                        </div>
                    </div>
                    <div class="packet-steps-content__people">
                        <img src="{{ url('images/icons/steps/step-1.svg') }}">
                    </div>
                    <div class="packet-steps-content__buy">
                        <div class="packet-steps-content__buy--border-top"></div>
                        <div class="packet-steps-content__buy--title">
                            <p class="font-size-20 mb-0">Tahap Kehidupan Bekerja</p>
                        </div>
                        <hr>
                        <div class="packet-steps-content__buy--content">
                            <p class="font-size-15 mb-0">
                                Beli Asuransi
                            </p>
                            <p class="font-size-30 mb-1 font-color-orange">
                                Perjalanan
                            </p>
                            <p class="font-size-15 mb-0">
                                Harga Premi
                            </p>
                            <p class="mb-1 font-color-orange">
                                <span class="font-size-30">Rp. 1.499</span>
                                <span class="font-size-20">/Bulan</span>
                            </p>
                            <p class="font-size-15 mb-0">
                                Uang Pertanggungan s.d
                            </p>
                            <p class="font-size-30 mb- font-color-orange">
                                Rp 50 Juta
                            </p>
                            <button class="btn packet-steps-content__buy--content__button">
                                <p class="mb-0">Lihat Rekomendasi</p>
                                <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="packet-steps-control">
                <div class="packet-steps-icons">
                    <div class="packet-steps-icon packet-steps-icon-active">
                        <!-- <div class="packet-steps-icon__nonactive">
                        <img src="{{ url('images/icons/steps/icon-step-1-nonactive.svg') }}">
                    </div> -->
                        <div class="packet-steps-icon__active">
                            <img src="{{ url('images/icons/steps/icon-step-1-active.svg') }}">
                        </div>
                        <p class="packet-steps-icon__text font-size-20 font-color-orange">Bekerja</p>
                    </div>
                    <div class="packet-steps-icon">
                        <div class="packet-steps-icon__nonactive">
                            <img src="{{ url('images/icons/steps/icon-step-2-nonactive.svg') }}">
                        </div>
                        <!-- <div class="packet-steps-icon__active">
                        <img src="{{ url('images/icons/steps/icon-step-2-active.svg') }}">
                    </div> -->
                        <p class="packet-steps-icon__text font-size-20 font-color-orange">Menikah</p>
                    </div>
                    <div class="packet-steps-icon">
                        <div class="packet-steps-icon__nonactive">
                            <img src="{{ url('images/icons/steps/icon-step-3-nonactive.svg') }}">
                        </div>
                        <!-- <div class="packet-steps-icon__active">
                        <img src="{{ url('images/icons/steps/icon-step-3-active.svg') }}">
                    </div> -->
                        <p class="packet-steps-icon__text font-size-20 font-color-orange">Keluarga</p>
                    </div>
                    <div class="packet-steps-icon">
                        <div class="packet-steps-icon__nonactive">
                            <img src="{{ url('images/icons/steps/icon-step-4-nonactive.svg') }}">
                        </div>
                        <!-- <div class="packet-steps-icon__active">
                        <img src="{{ url('images/icons/steps/icon-step-4-active.svg') }}">
                    </div> -->
                        <p class="packet-steps-icon__text font-size-20 font-color-orange">Hari Tua</p>
                    </div>
                </div>

                <div class="packet-steps-line">
                    <div class="packet-steps-line__nonactive"></div>
                    <div class="packet-steps-line__active"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="plan">
        <div class="plan--background"></div>
        <div class="plan--content">
            <div class="content--title mb-3">
                <p class="font-size-20 mb-0 letter-spacing-3 text-center font-color-white">PRODUK</p>
                <p class="font-size-39 text-center font-color-white">Mulai Merencanakan</p>
            </div>
            <div class="content--tabs align-items-center">
                <div class="content--tab content--tab--active" data-index="0">
                    <p class="font-size-16 mb-0 font-color-orient" data-index="0">Perjalanan</p>
                </div>
                <div class="content--tab" data-index="1">
                    <p class="font-size-16 mb-0 font-color-orient" data-index="1">Pendidikan</p>
                </div>
                <div class="content--tab" data-index="2">
                    <p class="font-size-16 mb-0 font-color-orient" data-index="2">Kesehatan</p>
                </div>
                <div class="content--tab" data-index="3">
                    <p class="font-size-16 mb-0 font-color-orient" data-index="3">Tabungan</p>
                </div>
            </div>
        </div>
        <div class="packet-container">
            <div class="packet-scroll">

                <div class=" packet-item content--text mt-2">
                    <div class="only-show-desktop">
                        <div class="justify-content-center mt-4">
                            <div class="card card-packet card-packet-planner">
                                <div class="card-packet--border-top"></div>
                                <!-- <div class="card-packet--badge">PRODUK UNGGULAN</div> -->
                                <div class="card-packet--content">
                                    <img src="{{ url('images/icons/packets/packet-1.png') }}" alt="Icon Paket Simple">
                                    <p class="mt-2 font-size-17">Asuransi Kecelakaan</p>
                                    <p class="font-size-30">Perjalanan Simpel</p>

                                    <div class="mb-3 justify-content-space-between">
                                        <div class="card-packet--content__premi">
                                            <p class="font-size-17">Premi mulai dari</p>
                                            <p class="font-size-25">Rp. 2.200
                                            </p>
                                        </div>
                                        <div class="card-packet--content__premi">
                                            <p class="font-size-17">Uang Pertanggungan s.d</p>
                                            <p class="font-size-25">Rp. 50 Juta
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-packet--items">
                                    <ul>
                                        <li>
                                            <div class="card-packet--items__icon">
                                                <img src="{{ url('images/icons/icon-checklist.png') }}" alt="">
                                            </div>
                                            <p class="font-size-15">
                                                Santunan Meninggal Dunia karena Kecelakaan
                                            </p>
                                        </li>
                                    </ul>

                                    <a href="#" class="btn btn-card">
                                        <span>Pilih Rencana</span>
                                        <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="Icon Right Row">
                                    </a>
                                </div>
                            </div>

                            <div class="card card-packet card-packet-planner">
                                <div class="card-packet--border-top"></div>
                                <div class="card-packet--badge">PRODUK UNGGULAN</div>
                                <div class="card-packet--content">
                                    <img src="{{ url('images/icons/packets/packet-2.png') }}" alt="Icon Paket Simple">
                                    <p class="mt-2 font-size-17">Asuransi Kecelakaan</p>
                                    <p class="font-size-30">Perjalanan Komplit</p>

                                    <div class="mb-3 justify-content-space-between">
                                        <div class="card-packet--content__premi">
                                            <p class="font-size-17">Premi mulai dari</p>
                                            <p class="font-size-25">Rp. 4.159
                                            </p>
                                        </div>
                                        <div class="card-packet--content__premi">
                                            <p class="font-size-17">Uang Pertanggungan s.d</p>
                                            <p class="font-size-25">Rp. 20 Juta
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-packet--items">
                                    <ul>
                                        <li>
                                            <div class="card-packet--items__icon">
                                                <img src="{{ url('images/icons/icon-checklist.png') }}" alt="">
                                            </div>
                                            <p class="font-size-15">
                                                Santunan Meninggal Dunia karena Kecelakaan
                                            </p>
                                        </li>
                                        <li>
                                            <div class="card-packet--items__icon">
                                                <img src="{{ url('images/icons/icon-checklist.png') }}" alt="">
                                            </div>
                                            <p class="font-size-15">
                                                Santunan Meninggal Dunia karena Penyakit
                                            </p>
                                        </li>
                                        <li>
                                            <div class="card-packet--items__icon">
                                                <img src="{{ url('images/icons/icon-checklist.png') }}" alt="">
                                            </div>
                                            <p class="font-size-15">
                                                Santunan Cacat Tetap Total atau Sebagian karena Kecelakaan
                                            </p>
                                        </li>
                                        <li>
                                            <div class="card-packet--items__icon">
                                                <img src="{{ url('images/icons/icon-checklist.png') }}" alt="">
                                            </div>
                                            <p class="font-size-15">
                                                Santunan Tunai Harian Rawat Inap Rumah Sakit karena Kecelakaan
                                            </p>
                                        </li>
                                        <li>
                                            <div class="card-packet--items__icon">
                                                <img src="{{ url('images/icons/icon-checklist.png') }}" alt="">
                                            </div>
                                            <p class="font-size-15">
                                                Santunan Penggantian Biaya Operasi/Pembedahan karena Kecelakaan per satu periode asuransi
                                            </p>
                                        </li>
                                    </ul>

                                    <a href="#" class="btn btn-card">
                                        <span>Pilih Rencana</span>
                                        <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="Icon Right Row">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Section Keunggukan -->
    <div class="keunggulan">
        <div class="content--title mb-3">
            <p class="font-size-20 mb-0 letter-spacing-3 text-center font-color-orient">KEUNGGULAN</p>
            <p class="font-size-39 text-center font-color-orient">Kenapa Plan BLife?</p>
        </div>
        <div class="content--text mt-2">
            <div class="card">
                <div class="image--card">
                    <img src="{{ url('images/icons/benefits/benefit-1.png') }}" alt="Benefit 1">
                </div>
                <p class="font-size-20 mb-0">Terpercaya</p>
                <ul>
                    <li>
                        <p class="font-size-15">Layanan asuransi digital BNI Life
                        </p>
                    </li>
                    <li>
                        <p class="font-size-15">Didukung pemegang saham terbesar PT BNI (Persero) Tbk dan Sumitomo Life Insurance Company
                        </p>
                    </li>
                    <li>
                        <p class="font-size-15">Terdaftar & diawasi oleh Otoritas Jasa Keuangan (OJK)</p>
                    </li>
                </ul>
                <a href="#" class="btn benefit-button">
                    <span>Tentang Kami</span>
                    <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="Icon Right Row">
                </a>
            </div>

            <div class="card">
                <div class="image--card">
                    <img src="{{ url('images/icons/benefits/benefit-2.png') }}" alt="Benefit 1">
                </div>
                <p class="font-size-20 mb-0">Mudah & Cepat</p>
                <p class="font-size-15 mb-0">
                    3 langkah mudah untuk memiliki perlindungan asuransi digital:
                </p>
                <ul>
                    <li>
                        <p class="font-size-15">Pilih produk
                        </p>
                    </li>
                    <li>
                        <p class="font-size-15">Isi data
                        </p>
                    </li>
                    <li>
                        <p class="font-size-15">Bayar</p>
                    </li>
                </ul>
                <a href="#" class="btn benefit-button">
                    <span>Cara Beli</span>
                    <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="Icon Right Row">
                </a>
            </div>
            <div class="card">
                <div class="image--card">
                    <img src="{{ url('images/icons/benefits/benefit-3.png') }}" alt="Benefit 1">
                </div>
                <p class="font-size-20 mb-0">Harga Terjangkau</p>
                <p class="font-size-15 mb-0">
                    Premi terjangkau mulai dari Rp 1rb-an! Sekali Bayar, tanpa komitmen jangka panjang!
                </p>
                <a href="#" class="btn benefit-button">
                    <span>Dapatkan Sekarang</span>
                    <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="Icon Right Row">
                </a>
            </div>
        </div>

        <div class="content--text mt-5 pt-3">
            <div class="card">
                <div class="image--card">
                    <img src="{{ url('images/icons/benefits/benefit-1.png') }}" alt="Benefit 1">
                </div>
                <p class="font-size-20 mb-0">Terpercaya</p>
                <ul>
                    <li>
                        <p class="font-size-15">Layanan asuransi digital BNI Life
                        </p>
                    </li>
                    <li>
                        <p class="font-size-15">Didukung pemegang saham terbesar PT BNI (Persero) Tbk dan Sumitomo Life Insurance Company
                        </p>
                    </li>
                    <li>
                        <p class="font-size-15">Terdaftar & diawasi oleh Otoritas Jasa Keuangan (OJK)</p>
                    </li>
                </ul>
                <a href="#" class="btn benefit-button">
                    <span>Tentang Kami</span>
                    <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="Icon Right Row">
                </a>
            </div>

            <div class="card">
                <div class="image--card">
                    <img src="{{ url('images/icons/benefits/benefit-2.png') }}" alt="Benefit 1">
                </div>
                <p class="font-size-20 mb-0">Mudah & Cepat</p>
                <p class="font-size-15 mb-0">
                    3 langkah mudah untuk memiliki perlindungan asuransi digital:
                </p>
                <ul>
                    <li>
                        <p class="font-size-15">Pilih produk
                        </p>
                    </li>
                    <li>
                        <p class="font-size-15">Isi data
                        </p>
                    </li>
                    <li>
                        <p class="font-size-15">Bayar</p>
                    </li>
                </ul>
                <a href="#" class="btn benefit-button">
                    <span>Cara Beli</span>
                    <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="Icon Right Row">
                </a>
            </div>
            <div class="card">
                <div class="image--card">
                    <img src="{{ url('images/icons/benefits/benefit-3.png') }}" alt="Benefit 1">
                </div>
                <p class="font-size-20 mb-0">Harga Terjangkau</p>
                <p class="font-size-15 mb-0">
                    Premi terjangkau mulai dari Rp 1rb-an! Sekali Bayar, tanpa komitmen jangka panjang!
                </p>
                <a href="#" class="btn benefit-button">
                    <span>Dapatkan Sekarang</span>
                    <img src="{{ url('images/icons/icon-white-right-arrow.png') }}" alt="Icon Right Row">
                </a>
            </div>
        </div>
    </div>
    <!-- Close Section Keunggulan -->

    <div class="testimonial">
        <div class="arrows">
            <div class="arrow arrow-left arrow-left-testimonial">
                <img src="{{ url('images/icons/left-arrow-testimonial.svg') }}" alt="Arrow Left">
            </div>

            <div class="arrow arrow-right arrow-right-testimonial">
                <img src="{{ url('images/icons/right-arrow-testimonial.svg') }}" alt="Arrow Right">
            </div>
        </div>

        <div class="content--title mb-3">
            <p class="font-size-20 mb-0 letter-spacing-3 text-center font-color-orient">TESTIMONI</p>
            <p class="font-size-39 text-center font-color-orange">Apa Kata Mereka?</p>
        </div>
        <div class="justify-content-center mt-2">
            <div class="testimonial--container">
                <div class="testimonial--scroll">
                    <div class="testimonial--content">
                        <div class="testimonial--item">
                            <div class="testimonial--item__image">
                                <img src="{{ url('/images/users/testimonial-1.svg') }}" alt="Testimonial People">
                            </div>
                            <div class="testimonial--item__text">
                                <img src="{{ url('/images/icons/icon-quote.svg') }}" alt="Quote Icon">

                                <p class="mt-4 mb-0 font-color-orient font-size-15 text-style-italic">aku seneng banget bisa merencakanan perjalanan hidupku dengan PLAN B LIFE dari bank BNI, semua nya dipermudah dan gak bikin ribet!</p>
                                <hr>
                                <p class="font-color-orient mb-0 font-size-16">Rizky Febian | Penyanyi</p>
                                <p class="font-color-orient mb-0 font-size-10 text-style-italic">Perencana Perjalanan & Kesehatan</p>
                            </div>
                        </div>
                    </div>

                    <div class="testimonial--content">
                        <div class="testimonial--item">
                            <div class="testimonial--item__image">
                                <img src="{{ url('/images/users/testimonial-1.svg') }}" alt="Testimonial People">
                            </div>
                            <div class="testimonial--item__text">
                                <img src="{{ url('/images/icons/icon-quote.svg') }}" alt="Quote Icon">

                                <p class="mt-4 mb-0 font-color-orient font-size-15 text-style-italic">aku seneng banget bisa merencakanan perjalanan hidupku dengan PLAN B LIFE dari bank BNI, semua nya dipermudah dan gak bikin ribet!</p>
                                <hr>
                                <p class="font-color-orient mb-0 font-size-16">Rizky Febian | Penyanyi</p>
                                <p class="font-color-orient mb-0 font-size-10 text-style-italic">Perencana Perjalanan & Kesehatan</p>
                            </div>
                        </div>
                    </div>

                    <div class="testimonial--content">
                        <div class="testimonial--item">
                            <div class="testimonial--item__image">
                                <img src="{{ url('/images/users/testimonial-1.svg') }}" alt="Testimonial People">
                            </div>
                            <div class="testimonial--item__text">
                                <img src="{{ url('/images/icons/icon-quote.svg') }}" alt="Quote Icon">

                                <p class="mt-4 mb-0 font-color-orient font-size-15 text-style-italic">aku seneng banget bisa merencakanan perjalanan hidupku dengan PLAN B LIFE dari bank BNI, semua nya dipermudah dan gak bikin ribet!</p>
                                <hr>
                                <p class="font-color-orient mb-0 font-size-16">Rizky Febian | Penyanyi</p>
                                <p class="font-color-orient mb-0 font-size-10 text-style-italic">Perencana Perjalanan & Kesehatan</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mitra d-flex justify-between align-center">
        <div class="content--title">
            <p class="content--title--sub font-size-20 font-color-orient">PARTNER</p>
            <p class="font-size-50 font-color-orient">Mitra Kami</p>
        </div>
        <div class="content--items">
            <div class="content--item">
                <img src="{{ url('/images/icons/merchant-icon-1.svg') }}" alt="Futuready">
            </div>
            <div class="content--item">
                <img src="{{ url('/images/icons/merchant-icon-2.svg') }}" alt="Futuready">
            </div>
            <div class="content--item">
                <img src="{{ url('/images/icons/merchant-icon-3.svg') }}" alt="Futuready">
            </div>
            <div class="content--item">
                <img src="{{ url('/images/icons/merchant-icon-4.svg') }}" alt="Futuready">
            </div>
        </div>
    </div>

    <div class="contact">
        <input type="hidden" id="_recaptcha" name="_recaptcha" value="">
        <input type="hidden" id="action" name="action" value="hubungi_kami">

        <div class="icon--mobile only-show-mobile">
            <p class="title font-size-39 text-right mb-3">Hubungi Kami</p>
            <p class="title font-size-39 text-right mb-3">Selamat datang di Plan BLife, apa yang bisa kami bantu?</p>
            <img src="{{ url('/images/icons/contact-people.svg') }}" alt="Contact People">
        </div>
        <div class="background"></div>

        <div class="container">
            <div class="icon only-show-desktop">
                <img src="{{ url('/images/icons/contact-people.svg') }}" alt="Contact People">
            </div>
            <div class="form">
                <p class="contact--title title font-size-20 mb-0 font-color-white only-show-desktop">Hubungi Kami</p>
                <p class="title font-size-30 mb-3">Selamat datang di Plan BLife, <br /> apa yang bisa kami bantu?</p>

                <div class="contact--form">
                    <div class="contact--form__border-top"></div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <input placeholder="Nama Lengkap*" type="text" class="form-control" id="name" />
                            </div>

                            <div class="col-lg-6">
                                <input placeholder="Alamat E-mail*" type="email" class="form-control" id="email" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mt-3">
                        <div class="row">
                            <div class="col-lg-6">
                                <input placeholder="+62 8xxx xxxx xxxx* " type="phone" class="form-control" id="phone" />
                            </div>

                            <div class="col-lg-6">
                                <select name="" id="" class="form-control">
                                    <option value="">Kategori*</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mt-3">
                        <div class="row">
                            <div class="col-lg-12">
                                <input placeholder="Subjek*" type="phone" class="form-control" id="phone" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 mt-3">
                        <div class="row">
                            <div class="col-lg-12">
                                <textarea placeholder="Pesan Kamu*" name="pesan" id="pesan" cols="30" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 mt-2">
                        <div class="row">
                            <div class="col-lg-12 content-button-send">
                                <button id="send" class="btn btn-send">Kirim</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Start Payment Section -->
    <div class="home--payment">
        <div class="container d-flex justify-content-between align-items-center">
            <div class="logo">
                <img class="color" src="{{ url('images/v2/logo/logo-bni-planblife.svg') }}" alt="Logo Lifeplan">
            </div>

            <div class="payments d-flex justify-content-center flex-column">
                <p class="mb-0 font-size-20">Pembayaran :</p>
                <div class="payment">
                    <img class="color" src="{{ url('images/icons/payments.svg') }}" alt="Payment Icon">
                </div>
            </div>
        </div>
        <!-- End Payment Section -->
    </div>

    @endsection