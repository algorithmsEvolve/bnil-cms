@extends('layouts/index')

@section('title', 'register')

@section('css')
<link href="{{ url('/css/pages/register.css') }}" rel="stylesheet">
@endsection

@section('js')
<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{env('CLIENTKEY_MIDTRANS')}}"></script>
<script type="text/javascript">
    $(function() {
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
        });
    });
</script>
<script src="{{ url('/js/pages/register.js') }}"></script>
@endsection

@section('topNavigation')
@include('../partials/navigation')
@endsection

@section('bottomNavigation')
@include('../partials/footer')
@endsection

@section('content')
<div id="register">
    <div class="banner" style="background: url('{{asset('images/banners/banner-register.svg')}}')">
        <div class="banner-text">
            <p class="title font-size-12">
                Rencana {{$plan->product->category->NAME}} > {{$plan->product->PRODUCT_NAMA}} > Pendaftaran
            </p>

            <div class="steps">
                <div class="step step-riwayat">
                    <!-- <span class="arrow">V</span> -->
                    <span id="menu-text-1" class="text active">Riwayat Kesehatan</span>
                </div>
                <div class="seperate step-riwayat"></div>
                <div class="step"><span id="menu-text-2" class="text">Data Diri</span></div>
                <div class="seperate"></div>
                <div class="step"><span id="menu-text-3" class="text">Review Data</span></div>
                <div class="seperate"></div>
                <div class="step"><span id="menu-text-4" class="text">Selesai</span></div>
            </div>
        </div>
    </div>

    <div class="form-absolute" hide-riwayat="{{$hideRiwayat}}">
        <div id="step-1">
            <div class="card text-center">
                <p class="font-size-20 font-color-orient">Sedikit tentang kesehatan Kamu </p>

                <div class="point">
                    <p class="font-size-12 font-color-orient mb-2">Point 1</p>
                    <p class="font-size-12">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vestibulum aenean duis id. Iaculis nulla ultrices mattis platea venenatis, amet, quisque aliquet.
                    </p>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown-point-1" data-bs-toggle="dropdown" aria-expanded="false">
                            Tidak
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li>
                                <a href="#">
                                    <p class="dropdown-option dropdown-item m-0 py-1 px-3" data-point="1" data-value="false">Tidak</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <p class="dropdown-option dropdown-item m-0 py-1 px-3" data-point="1" data-value="true">Ya</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <hr class="line-card">

                <div class="point">
                    <p class="font-size-12 font-color-orient mb-2">Point 2</p>
                    <p class="font-size-12">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vestibulum aenean duis id. Iaculis nulla ultrices mattis platea venenatis, amet, quisque aliquet.
                    </p>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown-point-2" data-bs-toggle="dropdown" aria-expanded="false">
                            Tidak
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li>
                                <a href="#">
                                    <p class="dropdown-option dropdown-item m-0 py-1 px-3" data-point="2" data-value="false">Tidak</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <p class="dropdown-option dropdown-item m-0 py-1 px-3" data-point="2" data-value="true">Iya</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <hr class="line-card">

                <div class="point">
                    <p class="font-size-12 font-color-orient mb-2">Point 3

                    </p>
                    <p class="font-size-12">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus vestibulum aenean duis id. Iaculis nulla ultrices mattis platea venenatis, amet, quisque aliquet.
                    </p>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdown-point-3" data-bs-toggle="dropdown" aria-expanded="false">
                            Tidak
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li>
                                <a href="#">
                                    <p class="dropdown-option dropdown-item m-0 py-1 px-3" data-point="3" data-value="false">Tidak</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <p class="dropdown-option dropdown-item m-0 py-1 px-3" data-point="3" data-value="true">Iya</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <div class="button btn btn-next">Selanjutnya</div>
            </div>
        </div>

        <div id="step-2">
            <div class="card" id="card-1">
                <p class="font-size-20 font-color-orient text-center">Form Pemegang Polis</p>

                <div class="forms">
                    <div class="form">
                        <input type="hidden" id="params" name="params" value="{{$json_params}}">
                        <div class="form-group">
                            <label for="NAMA_LENGKAP">Nama Lengkap</label>
                            <input id="NAMA_LENGKAP" type="text" class="form-control" />
                            <p class="error hint error-NAMA_LENGKAP"></p>
                        </div>

                        <div class="form-group">
                            <label for="JENIS_KELAMIN">Jenis Kelamin</label>
                            <select id="JENIS_KELAMIN" type="text" class="form-control">
                                <option value="1">Laki Laki</option>
                                <option value="0">Perempuan</option>
                            </select>
                            <p class="error hint error-JENIS_KELAMIN"></p>
                        </div>

                        <div class="form-group">
                            <label for="EMAIL">E-mail</label>
                            <input id="EMAIL" type="email" class="form-control" />
                            <p class="error hint error-EMAIL"></p>
                        </div>

                        <div class="form-group">
                            <label for="PHONE">Nomor HP</label>
                            <input id="PHONE" type="phone" class="form-control input-phonenumber" />
                            <p class="hint">*Diutamakan terdaftar pada WhatsApp</p>
                            <p class="error hint error-PHONE"></p>
                        </div>

                        <div class="form-group">
                            <label for="NIK">Nomor KTP</label>
                            <input id="NIK" type="number" class="form-control input-ktp" />
                            <p class="error hint error-NIK"></p>
                        </div>

                        <div class="form-group">
                            <label for="TEMPAT_LAHIR">Tempat Lahir</label>
                            <input id="TEMPAT_LAHIR" type="text" class="form-control" />
                            <p class="error hint error-TEMPAT_LAHIR"></p>
                        </div>

                        <div class="form-group">
                            <label for="TANGGAL_LAHIR">Tanggal Lahir</label>
                            <input id="TANGGAL_LAHIR" type="date" class="form-control" />
                            <p class="error hint error-TANGGAL_LAHIR"></p>
                        </div>
                    </div>
                    <div class="line-separate"></div>
                    <div class="form">
                        <div class="form-group">
                            <label for="ALAMAT">Alamat</label>
                            <input id="ALAMAT" type="text" class="form-control" />
                            <p class="error hint error-ALAMAT"></p>
                        </div>

                        <div class="form-group">
                            <label for="PROVINSI">Provinsi</label>
                            <select id="PROVINSI" type="text" class="form-control">
                                @foreach($provinces as $province)
                                <option value="{{ $province->id }}">{{ $province->name }}</option>
                                @endforeach
                            </select>
                            <p class="error hint error-PROVINSI"></p>
                        </div>

                        <div class="form-group">
                            <label for="KOTA">Kota / Kabupaten</label>
                            <select id="KOTA" type="text" class="form-control"></select>
                            <p class="error hint error-KOTA"></p>
                        </div>

                        <div class="form-group">
                            <label for="KECAMATAN">Kecamatan</label>
                            <select id="KECAMATAN" type="text" class="form-control"></select>
                            <p class="error hint error-KECAMATAN"></p>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-8">
                                    <label for="KELURAHAN">Kelurahan</label>
                                    <select id="KELURAHAN" type="text" class="form-control"></select>
                                    <p class="error hint error-KELURAHAN"></p>
                                </div>
                                <div class="col-lg-4">
                                    <label for="KODE_POS">Kode POS</label>
                                    <input id="KODE_POS" type="number" class="form-control" />
                                    <p class="error hint error-KODE_POS"></p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="PEKERJAAN">Pekerjaan</label>
                            <input id="PEKERJAAN" type="text" class="form-control" />
                            <p class="error hint error-PEKERJAAN"></p>
                        </div>

                        <div class="form-group">
                            <label for="PENGELUARAN">Rata-rata Pengeluaran Perbulan</label>
                            <select class="form-control" name="PENGELUARAN" id="PENGELUARAN">
                                <option value="1">Rp. 1.000.000 - Rp. 2.000.000</option>
                            </select>
                            <p class="error hint error-PENGELUARAN"></p>
                        </div>
                    </div>
                </div>
            </div>

            <hr class="mt-5 mb-5">

            {{-- <div class="card" id="card-2" style="height: 630px;">
                <p class="font-size-20 font-color-orient text-center">Form Tertanggung</p>

                <div class="forms">
                    <div class="form">
                        <div class="form-group">
                            <label for="TERTANGGUNG_STATUS_TERTANGGUNG">Status Tanggungan</label>
                            <select id="TERTANGGUNG_STATUS_TERTANGGUNG" type="text" class="form-control">
                                <option value="1">Laki Laki</option>
                                <option value="2">Perempuan</option>
                            </select>
                            <p class="error hint error-TERTANGGUNG_STATUS_TERTANGGUNG"></p>
                        </div>

                        <div class="form-group">
                            <label for="TERTANGGUNG_NAMA_LENGKAP">Nama Lengkap</label>
                            <input id="TERTANGGUNG_NAMA_LENGKAP" type="text" class="form-control" />
                            <p class="error hint error-TERTANGGUNG_NAMA_LENGKAP"></p>
                        </div>

                        <div class="form-group">
                            <label for="TERTANGGUNG_JENIS_KELAMIN">Jenis Kelamin</label>
                            <select id="TERTANGGUNG_JENIS_KELAMIN" type="text" class="form-control">
                                <option value="1">Laki Laki</option>
                                <option value="0">Perempuan</option>
                            </select>
                            <p class="error hint error-TERTANGGUNG_JENIS_KELAMIN"></p>
                        </div>

                        <div class="form-group">
                            <label for="TERTANGGUNG_EMAIL">E-mail <span class="optional">(Opsional)</span></label>
                            <input id="TERTANGGUNG_EMAIL" type="text" class="form-control" />
                            <p class="error hint error-TERTANGGUNG_EMAIL"></p>
                        </div>

                        <div class="form-group">
                            <label for="TERTANGGUNG_NOMOR_HP">Nomor HP <span class="optional">(Opsional)</span></label>
                            <input id="TERTANGGUNG_NOMOR_HP" type="phone" class="form-control input-phonenumber" />
                            <p class="hint">*Diutamakan terdaftar pada WhatsApp</p>
                            <p class="error hint error-TERTANGGUNG_NOMOR_HP"></p>
                        </div>
                    </div>
                    <div class="line-separate"></div>
                    <div class="form">
                        <div class="form-group">
                            <label for="TERTANGGUNG_NIK">Nomor KTP</label>
                            <input id="TERTANGGUNG_NIK" type="number" class="form-control input-ktp" />
                            <p class="error hint error-TERTANGGUNG_NIK"></p>
                        </div>

                        <div class="form-group">
                            <label for="TERTANGGUNG_TEMPAT_LAHIR">Tempat Lahir</label>
                            <input id="TERTANGGUNG_TEMPAT_LAHIR" type="text" class="form-control" />
                            <p class="error hint error-TERTANGGUNG_TEMPAT_LAHIR"></p>
                        </div>

                        <div class="form-group">
                            <label for="TERTANGGUNG_TANGGAL_LAHIR">Tanggal lahir</label>
                            <input id="TERTANGGUNG_TANGGAL_LAHIR" type="date" class="form-control" />
                            <p class="error hint error-TERTANGGUNG_TANGGAL_LAHIR"></p>
                        </div>

                        <div class="form-group">
                            <label>Selfie dengan KTP</label>
                            <label class="upload" for="upload-file-tertanggung-foto-ktp" id="upload-TERTANGGUNG_FOTO_KTP">
                                <img src="{{ url('images/icons/upload.svg') }}" alt="Icon Upload">
                                <p class="font-size-10 text-center p-0 m-0">Unggah file swafoto bersama KTP mu <br /> berupa .jpg atau .png max 3 MB</p>
                            </label>
                            <div class="preview-ktp" id="preview-TERTANGGUNG_FOTO_KTP">
                                <button class="btn-danger btn-sm delete-file" data-field="TERTANGGUNG_FOTO_KTP">Hapus</button>
                                <img src="" alt="" id="image-TERTANGGUNG_FOTO_KTP">
                            </div>
                            <input type="text" id="TERTANGGUNG_FOTO_KTP" style="display: none;">
                            <input type="file" name="upload-file-tertanggung-foto-ktp" id="upload-file-tertanggung-foto-ktp" style="display: none;" data-field="TERTANGGUNG_FOTO_KTP" class="upload-file">
                            <span class=" font-size-10">Nama file KTP.jpg</span>
                            <p class="error hint error-TERTANGGUNG_FOTO_KTP"></p>
                        </div>
                    </div>
                </div>
            </div> --}}

            {{-- <hr class="mt-5 mb-5"> --}}

            <div class="card" id="card-3" style="height: 630px;">
                <p class="font-size-20 font-color-orient text-center">Form Ahli Waris</p>

                <div class="forms">
                    <div class="form">
                        <div class="form-group">
                            <label for="AW_STATUS_PENERIMA">Status Ahli Waris</label>
                            <select id="AW_STATUS_PENERIMA" type="text" class="form-control">
                                <option value="1">Anak</option>
                                <option value="2">Istri</option>
                            </select>
                            <p class="error hint error-AW_STATUS_PENERIMA"></p>
                        </div>

                        <div class="form-group">
                            <label for="AW_NAMA_LENGKAP">Nama Lengkap</label>
                            <input id="AW_NAMA_LENGKAP" type="text" class="form-control" />
                            <p class="error hint error-AW_NAMA_LENGKAP"></p>
                        </div>

                        <div class="form-group">
                            <label for="AW_JENIS_KELAMIN">Jenis Kelamin</label>
                            <select id="AW_JENIS_KELAMIN" type="text" class="form-control">
                                <option value="1">Laki Laki</option>
                                <option value="0">Perempuan</option>
                            </select>
                            <p class="error hint error-AW_JENIS_KELAMIN"></p>
                        </div>

                        <div class="form-group">
                            <label for="AW_EMAIL">E-mail <span class="optional">(Opsional)</span></label>
                            <input id="AW_EMAIL" type="text" class="form-control" />
                            <p class="error hint error-AW_EMAIL"></p>
                        </div>

                        <div class="form-group">
                            <label for="AW_NOMOR_HP">Nomor HP <span class="optional">(Opsional)</span></label>
                            <input id="AW_NOMOR_HP" type="phone" class="form-control input-phonenumber" />
                            <p class="hint">*Diutamakan terdaftar pada WhatsApp</p>
                            <p class="error hint error-AW_NOMOR_HP"></p>
                        </div>
                    </div>
                    <div class="line-separate"></div>
                    <div class="form">
                        <div class="form-group">
                            <label for="AW_NIK">Nomor KTP</label>
                            <input id="AW_NIK" type="number" class="form-control input-ktp" />
                            <p class="error hint error-AW_NIK"></p>
                        </div>

                        <div class="form-group">
                            <label for="AW_TEMPAT_LAHIR">Tempat Lahir</label>
                            <input id="AW_TEMPAT_LAHIR" type="text" class="form-control" />
                            <p class="error hint error-AW_TEMPAT_LAHIR"></p>
                        </div>

                        <div class="form-group">
                            <label for="AW_TANGGAL_LAHIR">Tanggal lahir</label>
                            <input id="AW_TANGGAL_LAHIR" type="date" class="form-control" />
                            <p class="error hint error-AW_TANGGAL_LAHIR"></p>
                        </div>

                        <div class="form-group">
                            <label>Selfie dengan KTP</label>
                            <label class="upload" for="upload-file-ahli-waris-ktp" id="upload-AW_FOTO_KTP">
                                <img src="{{ url('images/icons/upload.svg') }}" alt="Icon Upload">
                                <p class="font-size-10 text-center p-0 m-0">Unggah file swafoto bersama KTP mu <br /> berupa .jpg atau .png max 3 MB</p>
                            </label>
                            <div class="preview-ktp" id="preview-AW_FOTO_KTP">
                                <button class="btn-danger btn-sm delete-file" data-field="AW_FOTO_KTP">Hapus</button>
                                <img src="" alt="" id="image-AW_FOTO_KTP">
                            </div>
                            <input type="text" id="AW_FOTO_KTP" style="display: none;">
                            <input type="file" name="upload-file-ahli-waris-ktp" id="upload-file-ahli-waris-ktp" style="display: none;" data-field="AW_FOTO_KTP" class="upload-file">
                            <span class=" font-size-10">Nama file KTP.jpg</span>
                            <p class="error hint error-AW_FOTO_KTP"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <div class="button btn btn-prev">Kembali</div>
                <div class="button btn btn-next">Selanjutnya</div>
            </div>
        </div>

        <div id="step-3">
            <div class="card">
                <p class="font-size-20 m-0 p-0 font-color-orient text-center">Review Data</p>
                <p class="font-size-12 m-0 p-0 font-color-boulder text-center">Periksa kembali informasi pesanan dibawah ini</p>

                <div class="content-review mt-2">
                    <div class="price pt-3">
                        <p class="font-size-15 font-color-boulder m-0">Jumlah yang harus dibayar</p>
                        <p class="font-size-25 font-color-flamingo">Rp. {{$price}}</p>

                        <div class="tnc only-show-mobile">
                            <input type="checkbox" id="tnc">
                            <label for="tnc">Saya sudah membaca dan memahami <span class="font-color-flamingo">Syarat & Ketentuan dan Ringkasan informasi produk</span> yang berlaku</label>
                        </div>
                    </div>

                    <div class="line-separate"></div>

                    <div class="data pt-3">
                        <div class="accordion accordion-flush" id="accordionFlushExample">
                            <div class="accordion-content">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingOne">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                            Form Pemegang Polis
                                        </button>
                                    </h2>
                                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body" id="first-tab-preview">
                                        </div>
                                    </div>
                                </div>
                                <div class="edit-button">
                                    <p class="font-size-15 font-color-orient">Edit</p>
                                </div>
                            </div>
                            {{-- <div class="accordion-content">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingTwo">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                            Form Tertanggung
                                        </button>
                                    </h2>
                                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body" id="second-tab-preview"></div>
                                    </div>
                                </div>
                                <div class="edit-button">
                                    <p class="font-size-15 font-color-orient">Edit</p>
                                </div>
                            </div> --}}
                            <div class="accordion-content">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingThree">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                            Form Ahli Waris
                                        </button>
                                    </h2>
                                    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body" id="third-tab-preview"></div>
                                    </div>
                                </div>
                                <div class="edit-button">
                                    <p class="font-size-15 font-color-orient">Edit</p>
                                </div>
                            </div>
                        </div>

                        <div class="tnc only-show-desktop">
                            <input type="checkbox" id="tnc">
                            <label for="tnc">Saya sudah membaca dan memahami <span class="font-color-flamingo">Syarat & Ketentuan dan Ringkasan informasi produk</span> yang berlaku</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center">
                <div class="button btn btn-next">Lanjut ke Pembayaran</div>
            </div>
        </div>

        <div id="step-4">
            <div class="card text-center">
                <div class="image">
                    <img src="{{ url('images/icons/success-register.svg') }}" alt="Success Register Icon">
                    <p class="font-size-25 font-color-orient">Selamat John Doe, Kamu Berhasil <br /> Merencanakan Proteksi untuk Perjalananmu!</p>
                    <p class="font-size-20">Kami akan mengirimkan bukti email bukti transaksi <br /> beserta dokumen e-polish kamu segera ke</p>
                    <p class="font-size-25">johndoe@email.com</p>
                </div>
            </div>

            <div class="text-center">
                <div class="button btn btn-prev">Verifikasi Email Kamu</div>
                <div class="button btn btn-next">Lihat Profil Kamu</div>
            </div>
        </div>

        <div id="step-5">
            <div class="card text-center p-5">
                <div class="image pt-2">
                    <img src="{{ url('images/icons/loading-register.svg') }}" alt="Loading Register Icon">
                    <p class="mt-4 font-size-25 font-color-orient">Harap menunggu. <br /> Proses pembayaranmu sedang berlangsung...</p>
                </div>
            </div>
        </div>

        <div id="step-7">
            <div class="card text-center p-5">
                <div class="image pt-2">
                    <img src="{{ url('images/icons/cs-service.svg') }}" alt="Customer Service Register Icon">
                    <p class="mt-4 font-size-25 font-color-orient">Terimakasih sudah mendaftar. <br /> Agen kami akan segera menghubungi Anda</p>
                </div>
            </div>
        </div>

        <div id="step-8">
            <div class="card text-center p-5">
                <div class="image pt-2">
                    <img src="{{ url('images/icons/failed-register.svg') }}" alt="Failed Register Icon">
                    <p class="mt-4 font-size-25 font-color-orient">Maaf,<br /> Kamu tidak bisa melanjutkan proses Pembelian</p>
                    <p class="font-size-20">Kami berupaya memberikan perlindungan terbaik bagi kamu. <br /> Namun mohon maaf, riwayat kesehatan kamu tidak memenuhi persyaratan <br /> pembelian produk ini. Silahkan hubungi petugas kami</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection