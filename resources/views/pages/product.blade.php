@extends('layouts/index')

@section('title', $product->PRODUCT_NAMA)

@section('css')
<link href="{{ url('/css/pages/packet.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="{{ url('/js/pages/product.js') }}"></script>

<script type="text/javascript">
    $(function() {
        $(".datepicker").datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true,
            startDate: new Date('{{$max_date}}'),
            endDate: new Date('{{$min_date}}'),
        });
    });
</script>
@endsection

@section('topNavigation')
@include('../partials/navigation')
@endsection

@section('bottomNavigation')
@include('../partials/footer')
@endsection

@section('content')
<div id="packet">
    <div class="banner">
        <div class="banner-text">
            <p class="title">
                Jaminan Maksimal {{$product->category->NAME == "Perjalanan" || $product->category->NAME == "Pendidikan" ? '' : $product->category->NAME}} {{$product->PRODUCT_NAMA}}
            </p>

            <ul>
                <li>
                    <img src="{{ url('images/icons/checklist.svg') }}" alt="Icon Checklist">
                    <p>Manfaat Meninggal Dunia karena Sebab Alami (Natural Death) Rp. 25.000.000,-</p>
                </li>
                <li>
                    <img src="{{ url('images/icons/checklist.svg') }}" alt="Icon Checklist">
                    <p>Manfaat Meninggal Dunia karena Kecelakaan Rp. 50.000.000,-</p>
                </li>
                <li>
                    <img src="{{ url('images/icons/checklist.svg') }}" alt="Icon Checklist">
                    <p>Manfaat Hidup/Tahapan (setiap Tahun ke-6, ke-12, dan ke 18)</p>
                </li>
            </ul>
        </div>

        <div class="people">
            <img src="{{ url('images/banners/packet-people.svg') }}" alt="Packet People Banner">
        </div>
    </div>

    <div class="content container mt-5 pt-5 mb-5 pb-2 font-color-orient">
        <div class="content--title">
            <p class="text-center font-size-30">Informasi Produk {{$product->category->NAME == "Perjalanan" ? '' : $product->category->NAME}} {{$product->PRODUCT_NAMA}}</p>
        </div>
        <div class="accordion" id="accordionExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        Manfaat Asuransi
                    </button>
                </h2>
                <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        {{$product->MANFAAT}}
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Ketentuan Umum
                    </button>
                </h2>
                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        {{$product->KETENTUAN_UMUM}}
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingThree">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Pengecualian
                    </button>
                </h2>
                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        {{$product->PENGECUALIAN}}
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="headingFour">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Mekanisme Klaim
                    </button>
                </h2>
                <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                    <div class="accordion-body">
                        {{$product->MEKANISME_KLAIM}}
                    </div>
                </div>
            </div>
        </div>

        <p class="text-center font-size-14"> <a href="{{asset($product->FILE_PDF)}}" download>Lihat Detail Produk</a></p>
    </div>

    <div class="content calculator container mt-5 pt-5 mb-5 pb-5">
        <div class="row">
            <div class="col-lg-6 font-color-orient">
                <p class="font-size-25 mb-1">Kalkulator</p>
                <p class="font-size-35 mb-3 line-height-35">Sesuaikan dengan masa asuransi yang Kamu inginkan</p>
                <p class="font-size-16 mb-1">Buatkan rencana pendidikan terbaik untuk kesehatanmu tanpa rasa khawatir akan masa depan.</p>
                <input type="hidden" name="product_id" id="product_id" value="{{$product_id}}">
                @if ($product->USIA_TERTANGGUNG == 1)
                <div class="form-group mb-4 mt-3">
                    <label for="dateborn">Tanggal Lahir</label>
                    <div class="input-group">
                        <img src="{{ url('images/icons/date.svg') }}" alt="Icon Date">
                        <input placeholder="Pilih Tanggal Lahir" type="text" class="form-control datepicker" id="birth_date" name="tgl_awal">
                    </div>
                    <p class="text-right font-color-red font-size-13">*Usia Masuk Peserta {{$product->USIA_MIN}} tahun - {{$product->USIA_MAX}} tahun</p>
                </div>
                @endif
                @foreach ($plans as $plan)
                <div class="assurance-choice {{ $product->USIA_TERTANGGUNG == 1 ? 'assurance-choice-disabled' : '' }} {{$loop->first ? 'assurance-choice__active' : ''}}" id="assurance-{{$plan->id}}" data-id="{{$plan->id}}">
                    <div class="bullet" data-id="{{$plan->id}}"></div>
                    <div class="description" data-id="{{$plan->id}}">
                        <p class="font-size-16" data-id="{{$plan->id}}">Manfaat Asuransi</p>
                        <p class="font-size-20" data-id="{{$plan->id}}">{{$plan->plan_manfaats->first()->MANFAAT}}</p>
                    </div>
                    <div class="price" data-id="{{$plan->id}}">
                        <p class="font-size-16 premi_title" data-id="{{$plan->id}}">Premi {{$plan->getFirstPlan()['waktu']}}</p>
                        <p class="font-size-20" id="premi_nominal_{{ $plan->id }}" data-id="{{$plan->id}}">Rp. {{$plan->getFirstPlan()['nominal']}}</p>
                    </div>
                </div>
                @endforeach

                <p class="font-size-16 font-color-orient text-center mb-0 mt-4">Pilih Masa Asuransi Kamu</p>
                @if ($product->USIA_TERTANGGUNG == 1)
                <input type="range" disabled class="form-range" value="1" min="1" max="{{$product->getIntervalPremi()['max_waktu']}}" id="range_time">
                @else
                <input type="range" class="form-range" value="1" min="1" max="{{$product->getIntervalPremi()['max_waktu']}}" id="range_time">
                @endif
                <div class="justify-content-space-between">
                    @if ($product->getIntervalPremi()['bulanan'])
                    <p class="font-size-16 font-color-orient">Bulanan</p>
                    @endif
                    @if ($product->getIntervalPremi()['triwulan'])
                    <p class="font-size-16 font-color-orient">3 Bulan</p>
                    @endif
                    @if ($product->getIntervalPremi()['semester'])
                    <p class="font-size-16 font-color-orient">6 Bulan</p>
                    @endif
                    @if ($product->getIntervalPremi()['tahunan'])
                    <p class="font-size-16 font-color-orient">12 Bulan</p>
                    @endif
                </div>
            </div>
            <div class="col lg-1"></div>
            <div class="col-lg-5">
                <div class="card-calculator">
                    <div class="header">
                        <div class="image">
                            <img src="{{ url('images/icons/paket-rencana-kesehatan.svg') }}" alt="Paket Rencana Kesehatan">
                        </div>
                        <div class="text">
                            <p class="font-size-16">Rencana {{$product->category->NAME == "Perjalanan" ? '' : $product->category->NAME}}</p>
                            <p class="font-size-20">{{$product->PRODUCT_NAMA}}</p>
                        </div>
                    </div>

                    <div class="content">
                        <div class="benefit">
                            <p class="font-size-16">Manfaat Asuransi</p>
                            <p class="font-size-20" id="benefit-assurance-nominal">{{$plan_first['manfaat']}}</p>
                        </div>
                        <div class="premi">
                            <p class="font-size-16 premi_title">Premi {{$plan->getFirstPlanDetail()['waktu']}}</p>
                            <p class="font-size-20" id="premi_select_nominal">Rp. {{$plan_first['nominal']}}</p>
                        </div>
                    </div>

                    <hr>

                    <p class="font-size-16">Produk Unggulan yang meringankan beban biaya Rumah Sakit, baik Rawat Inap maupun ICU</p>

                    <ul id="list-benefits">
                        @foreach ($plan_first['desc'] as $desc_plan)
                        <li>
                            <img src="{{ url('images/icons/checklist.svg') }}" alt="Icon Checklist">
                            <p>{{$desc_plan}}</p>
                        </li>
                        @endforeach
                    </ul>

                    <button type="button" id="select-plan" class="btn">Pilih Manfaat</button>
                </div>

                <p class="text-center font-size-16 font-color-orient mt-4">Kontak kami untuk pertanyaan lebih lanjut</p>
            </div>
        </div>
    </div>
</div>
@endsection