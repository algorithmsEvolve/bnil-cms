@extends('layouts/index')

@section('title', 'Form Asuransi Perisai Plus')

@section('css')
<link href="{{ asset('/css/pages/perisai.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js" integrity="sha512-nOQuvD9nKirvxDdvQ9OMqe2dgapbPB7vYAMrzJihw5m+aNcf0dX53m6YxM4LgA9u8e9eg9QX+/+mPu8kCNpV2A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    var rECAPTCHA_SITE_KEY = "{{ env('RECAPTCHA_SITE_KEY') }}";
</script>
<script src="https://www.google.com/recaptcha/api.js?render={{env('RECAPTCHA_SITE_KEY')}}"></script>
<script src="{{ asset('/js/pages/perisai.js') }}"></script>
@endsection

@section('topNavigation')
@include('../partials/navigation-perisai')
@endsection

@section('bottomNavigation')
@include('../partials/footer')
@endsection

@section('content')

<div id="perisai">
    <div id="step-tc" class="container display-none">
        <div class="mt-4 pt-4">
            <p class="font-size-14 font-weight-700 text-center">Syarat dan Ketentuan serta Pernyataan <br /> Dalam Pembelian Produk Asuransi <br /> (“Pernyataan Calon Nasabah”)</p>

            <p class="font-size-12 text-justify">Dengan mengisi dan melengkapi kelengkapan data diri yang dipersyaratkan untuk mengikuti program asuransi jiwa ini, saya mengajukan permohonan asuransi jiwa kepada PT. BNI Life Insurance (Pihak Penanggung) yang mana dalam hal ini saya bertindak sebagai calon pemegang polis dan menyatakan bahwa:
            </p>

            <ul>
                <li class="font-size-12 text-justify mb-2">Saya mengajukan diri untuk mendapatkan perlindungan asuransi yang dikeluarkan oleh BNI Life selaku penanggung. Saya telah membaca, mengerti, menjawab dan mengisi dengan lengkap dan benar informasi yang tercantum dalam permohonan Asuransi ini sesuai dengan keadaan sebenarnya. Saya memahami dan menyetujui bahwa dalam hal saya memasukan data yang tidak sesuai dengan keadaan sebenarnya, maka BNI Life berhak membatalkan kepesertaan dan tidak membayarkan apapun sebagaimana yang tercantum dalam Bukti Kepesertaan (Polis).
                </li>
                <li class="font-size-12 text-justify mb-2">Saya menyetujui bahwa perlindungan asuransi mulai berlaku sejak tanggal berlaku Bukti Kepesertaan (Polis).
                </li>
                <li class="font-size-12 text-justify mb-2">Saya memahami dan menyetujui bahwa BNI Life hanya akan menerbitkan Bukti Kepesertaan (Polis) dalam bentuk digital atau elektronik yang akan saya terima melalui nomor handphone sebagai mana yang tertera dalam pengajuan asuransi ini dan dokumen bagian dari polis yang berupa ringkasan polis akan tetap disampaikan oleh Perusahaan dalam bentuk hardcopy kepada nasabah.</li>
                <li class="font-size-12 text-justify mb-2">Saya menyatakan telah membaca, memahami dan menyetujui seluruh informasi yang tertuang dalam Ringkasan Informasi Produk dan/atau Layana atas produk asuransi ini
                </li>
            </ul>

            <div class="text-center mt-4 mb-4">
                <a href="{{asset($riplay_perisaiplus->SETTING_VALUE ?? '')}}" target="_blank" class="font-color-orange-700 text-decoration-underline">
                    Download Ringkasan Informasi Produk
                </a>
            </div>

            <div class="text-center mt-4 mb-5">
                <button type="button" id="agree-tc" class="btn border-radius-4 text-uppercase background-orange-gradient font-color-white font-size-16 font-weight-700">
                    <div class="button-submit-loading spinner-border spinner-border-sm text-primary" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                    <span class="button-submit-text">setuju</span>
                </button>
            </div>
        </div>
    </div>

    <div id="step-4" class=" display-none background-orange-gradient w-100 height-600px justify-content-center align-items-center">
        <div class="perisai--card w-90 border-radius-20 background-white justify-content-center align-items-center flex-flow-column">
            <img src="{{ url('images/icons/icon-success-perisai.svg') }}" alt="Icon Success">
            <p class="text-center font-size-14 font-weight-700 font-color-orange mt-1 mb-0">Terima Kasih <br /> Pengajuan aktivasi Asuransi Perisai Plus <br /> Anda telah kami terima</p>
            <p class="font-size-12 font-weight-400 mt-2 font-color-gray">Pengajuan Anda akan segera kami proses.
            </p>
        </div>
    </div>

    <div id="step-5" class=" display-none background-orange-gradient w-100 height-600px justify-content-center align-items-center">
        <div class="perisai--card w-90 border-radius-20 background-white justify-content-center align-items-center flex-flow-column">
            <img src="{{ url('images/icons/icon-failed-perisai.svg') }}" alt="Icon Success">
            <p class="text-center font-size-14 font-weight-400 mt-3 mb-0">Aktivasi Asuransi Perisai Plus Anda telah kami <br /> batalkan, silakan kembali ke halaman <br /> <b>MyCard</b> jika Anda ingin melakukan <br /> aktivasi kembali
            </p>
            </p>
        </div>
    </div>

    <div id="step-1">
        <div class="pt-8 pb-5 background-orange-gradient w-100 justify-content-center align-items-center text-center">
            <div class="mt-5 pt-5 container flex-flow-column">
                <img src="{{ url('/images/icons/icon-perisai-plus.svg') }}" alt="Icon Perisai Plus">

                <p class="font-color-white font-size-18 mt-4">
                    Selamat Datang di Layanan Aktivasi <br />
                    Online Asuransi Perisai Plus. <br /> <br />
                    Apa itu Asuransi Perisai Plus?
                    <a href="#" class="font-weight-bold text-decoration-underline font-color-blue" data-bs-toggle="modal" data-bs-target="#perisaiPlusModal">Read More</a> <br /> <br />
                    Untuk informasi detail terkait asuransi Perisai Plus dapat dibaca <br />
                    <a href="{{asset($riplay_perisaiplus->SETTING_VALUE ?? '')}}" target="_blank" class="font-weight-bold text-decoration-underline font-color-blue">di sini</a>
                    <br /> <br /> Silakan isi data berikut ini
                </p>
            </div>
        </div>

        <div class="modal fade" id="perisaiPlusModal" tabindex="-1" aria-labelledby="perisaiPlusModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered"">
                <div class=" modal-content">
                <div class="modal-body">
                    <div class="border-radius-20 background-white justify-content-center align-items-center flex-flow-column">
                        <p class="font-size-15 font-weight-400 mt-2 font-color-gray text-center">
                            <strong>Tentang Asuransi Perisai Plus</strong>
                        </p>
                        <p class="font-size-15 font-weight-400 mt-2 font-color-gray text-center">
                            Untuk memberikan perlindungan yang maksimal bagi Anda sekeluarga, BNI bekerjasama dengan BNI Life menawarkan produk asuransi yang dapat Anda peroleh sesuai kebutuhan Anda, diantaranya Asuransi Perisai Plus.
                            <br /><br />
                            Dengan mengikuti Asuransi Perisai Plus, saldo tagihan Kartu Kredit BNI Anda akan terlindungi apabila terjadi resiko kematian, cacat (ketidakmampuan sementara), cacat tetap akibat sakit/kecelakaan serta perlindungan terhadap 40 penyakit kritis (Critical Illness). Benefit Perisai Plus dapat Anda nikmati hanya dengan Premi sebesar 0.69% dari total saldo tagihan.
                        </p>
                    </div>

                    <div class="justify-between">
                        <button type="button" class="btn btn-primary btn-question-no" data-bs-dismiss="modal" aria-label="Close">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="perisai--form pt-4 pb-4">
        <div class="container">
            <div class="text-center">
                <p class="font-size-14 font-weight-700">DATA NASABAH</p>
            </div>

            <form action="post">
                <div class="form-group mt-2 pt-3">
                    <input type="hidden" id="_recaptcha" name="_recaptcha" value="">
                    <input type="hidden" id="action" name="action" value="perisai_plus">
                    <label class="font-size-14 font-weight-400" for="name">Nama Pemegang Kartu</label>
                    <input type="text" autocomplete="off" class="form-control" id="name" onkeypress="return (event.charCode > 64 && event.charCode < 91) || (event.charCode > 96 && event.charCode < 123) || (event.charCode==32)">
                    <p class="font-size-11 ml-1 mb-0 pb-0">Diisi dengan Nama yang terdaftar di Kartu Kredit BNI</p>
                    <span class="error error-name"></span>
                </div>

                <div class="form-group">
                    <label class="font-size-14 font-weight-400" for="phone">Nomor HP</label>
                    <input type="text" autocomplete="off" placeholder="081123xxx" maxlength="14" class=" form-control input-number" id="phone">
                    <p class="font-size-11 ml-1 mb-0 pb-0">Diisi dengan nomor HP yang terdaftar di Kartu Kredit BNI</p>
                    <p class="error error-phone"></p>
                </div>

                <div class="form-group">
                    <label class="font-size-14 font-weight-400" for="card_type">Tipe Kartu</label>
                    <select type="text" autocomplete="off" class="form-control" id="card_type">
                        <option value="" default>Pilih Kartu</option>
                        @foreach ($tipe_kartu as $tipe_kartu_item)
                        <option value="{{$tipe_kartu_item->id}}" data-text="{{$tipe_kartu_item->TIPE_NAME}}">{{$tipe_kartu_item->TIPE_NAME}}</option>
                        @endforeach
                    </select>
                    <span class="error error-card_type"></span>
                </div>

                <div class="form-group">
                    <label class="font-size-14 font-weight-400" for="number_card">4 Digit Terakhir kartu</label>
                    <input autocomplete="off" class="form-control number_card" id="number_card" type="text" maxlength="4" placeholder="XXXX">
                    <span class="error error-number_card"></span>
                </div>

                <div class="align-items-start mt-4 pt-2">
                    <div class="ml-4">
                        <input type="checkbox" id="tc" name="tc" class="form-check-input w-15px">
                    </div>
                    <label class="w-90 ml-5 font-size-14 font-weight-400" for="tc">Saya sudah membaca dan memahami <a href="#" id="openTc" class="font-color-orange text-decoration-underline">Syarat & Ketentuan dan Ringkasan informasi produk</a> yang berlaku</label>
                </div>

                <div class="justify-content-space-between actions mt-4 pt-2">
                    <!-- id="accept-cancel"  -->
                    <button type="button" class="btn text-uppercase border-radius-4" data-bs-toggle="modal" data-bs-target="#confirmationModal">batal</button>
                    <button disabled id="step-1-next" type="button" class="btn-no-padding btn text-uppercase border-radius-4 background-orange-gradient font-color-white font-weight-bold">
                        <div class="button-submit-loading spinner-border spinner-border-sm text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <span class="button-submit-text">lanjutkan</span>
                    </button>
                </div>
            </form>

            <!-- Modal Konfirmasi Batal -->
            <div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="confirmationLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="border-radius-20 background-white justify-content-center align-items-center flex-flow-column">
                                <img src="{{ url('images/icons/icon-question.svg') }}" alt="Icon Success">
                                <p class="font-size-12 font-weight-400 mt-2 font-color-gray text-center">
                                    Apakah Anda yakin akan membatalkan pendaftaran Asuransi Perisai Plus?
                                </p>
                            </div>

                            <div class="justify-between">
                                <button id="accept-cancel" type="button" class="btn btn-secondary btn-question-yes" data-bs-dismiss="modal">Ya</button>
                                <button type="button" class="btn btn-primary btn-question-no" data-bs-dismiss="modal">Tidak</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="step-2" class="display-none">
    <div class="background-orange-gradient w-100 height-300px justify-content-center align-items-center text-center">
        <div class="container flex-flow-column">
            <img src="{{ url('/images/icons/icon-like-perisai.svg') }}" alt="Icon Like">

            <p class="font-color-white font-size-16 mt-4">Terima Kasih telah melakukan pendaftaran <br /> mohon untuk dapat mengecek kembali data Anda dibawah ini :
            </p>
        </div>
    </div>

    <div class="perisai--form perisai--form__like pt-4 pb-4">
        <div class="container">
            <div class="text-center">
                <p class="font-size-14 font-weight-700">Ringkasan Informasi <br /> Pendaftaran Asuransi Perisai Plus</p>
            </div>

            <form action="post">
                <div class="form-group mt-2 pt-3">
                    <div class="justify-content-space-between">
                        <label class="font-size-12 font-color-netral font-weight-400" for="name">Nama Pemegang Kartu</label>
                        <p key="input-name" show="text" class="edit font-size-12 font-weight-400 font-color-blue-800 mb-0">Edit</p>
                    </div>
                    <p id="text-name" class="font-size-14 font-weight-400 font-color-gray-700 mt-0">Yuki Edwinanto</p>
                    <input type="text" class="form-control input-form" id="input-name">
                    <span class="error error-name"></span>
                </div>

                <div class="form-group">
                    <div class="justify-content-space-between">
                        <label class="font-size-12 font-color-netral font-weight-400" for="phone">Nomor HP</label>
                        <p key="input-phone" show="text" class="edit font-size-12 font-weight-400 font-color-blue-800 mb-0">Edit</p>
                    </div>
                    <p id="text-phone" class="font-size-14 font-weight-400 font-color-gray-700 mt-0">081 234 567 890</p>
                    <input type="text" placeholder="081123xxx" maxlength="14" class="form-control input-form input-number" id="input-phone">
                    <span class="error error-phone"></span>
                </div>

                <div class="form-group">
                    <div class="justify-content-space-between">
                        <label class="font-size-12 font-color-netral font-weight-400" for="type">Tipe Kartu</label>
                        <p key="input-card_type" show="text" class="edit font-size-12 font-weight-400 font-color-blue-800 mb-0">Edit</p>
                    </div>

                    <p id="text-card_type" class="font-size-14 font-weight-400 font-color-gray-700 mt-0">Kartu Kredit BNI Visa Silver</p>
                    <select type="text" class="input-form form-control" id="input-card_type">
                        <option value="" default>Pilih Kartu</option>
                        @foreach ($tipe_kartu as $tipe_kartu_item)
                        <option value="{{$tipe_kartu_item->id}}" data-text="{{$tipe_kartu_item->TIPE_NAME}}">{{$tipe_kartu_item->TIPE_NAME}}</option>
                        @endforeach
                    </select>
                    <span class="error error-card_type"></span>
                </div>

                <div class="form-group">
                    <div class="justify-content-space-between">
                        <label class="font-size-12 font-color-netral font-weight-400" for="number_card">4 Digit Terakhir Kartu</label>
                        <p key="input-number_card" show="text" class="edit font-size-12 font-weight-400 font-color-blue-800 mb-0">Edit</p>
                    </div>

                    <p id="text-number_card" class="font-size-14 font-weight-400 font-color-gray-700 mt-0">XXXX</p>
                    <input type="text" class="number_card input-form form-control" id="input-number_card" maxlength="4" placeholder="XXXX">
                    <span class="error error-number_card"></span>
                </div>

                <div class="alert alert-danger" style="display: none;">
                    <p class="text-center m-0">Request Timeout!</p>
                </div>

                <div class="justify-content-space-between actions mt-4 pt-2">
                    <button type="button" class="btn text-uppercase border-radius-4" id="backToFirst">kembali</button>
                    <button type="button" id="step-2-next" class="btn-no-padding btn text-uppercase border-radius-4 background-orange-gradient font-color-white font-weight-bold">
                        <div class="button-submit-loading spinner-border spinner-border-sm text-primary" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                        <span class="button-submit-text">lanjutkan</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div id="step-3" class="display-none container text-center verification mt-5 p-lg-5 p-3">
    <img src="{{ url('images/icons/icon-verification-perisai.svg') }}" alt="Icon Verification">

    <div class="text-center mt-4">
        <p>Kami telah kirimkan <b>6 digit kode aktivasi</b> <br /> melalui SMS ke nomor Handphone yang terdaftar
            <br /><br />
            Silakan masukan <b>6 digit kode aktivasi</b> Anda
        </p>

        <div class="justify-content-space-between otp--body">
            <input id="first-otp" number-top="1" type="number" maxlength="1" class="form-control otp--input">
            <input number-top="2" type="number" maxlength="1" class="form-control otp--input">
            <input number-top="3" type="number" maxlength="1" class="form-control otp--input">
            <input number-top="4" type="number" maxlength="1" class="form-control otp--input">
            <input number-top="5" type="number" maxlength="1" class="form-control otp--input">
            <input number-top="6" type="number" maxlength="1" class="form-control otp--input">
        </div>

        <p class="mb-3" id="message--otp">Kode OTP salah</p>

        <p>Belum terima kode aktivasi? <span id="countdown" class="ml-3 font-weight-600 font-color-blue">05:00</span></p>

        <p class="font-color-yellow font-weight-600 pt-4 resend--otp">Kirim ulang kode aktivasi</p>
    </div>
</div>
</div>
@endsection