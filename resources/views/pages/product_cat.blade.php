@extends('layouts/index')

@section('title', $category->NAME)

@section('css')
<link href="{{ url('/css/pages/product.css') }}" rel="stylesheet">
<style>
    #product .banner:before {
        background-image: url('{{ url($category->BANNER_IMAGE) }}');
        background-size: cover;
    }
</style>
@endsection

<!-- @section('js')
<script src="{{ url('/js/pages/home.js') }}"></script>
@endsection -->

@section('topNavigation')
@include('../partials/navigation-v2')
@endsection

@section('bottomNavigation')
@include('../partials/footer')
@endsection

@section('content')
<div id="product">
    <div class="banner">
        <!-- <img src="{{ url($category->BANNER_IMAGE) }}" alt="Banner Produk"> -->

        <div class="banner-text">
            <p class="title font-size-34">
                {{$category->NAME}}
            </p>
        </div>
    </div>

    <div class="content container mt-5 pt-5 mb-5 pb-2">
        <div class="content--title">
            <p class="text-center font-size-30">Keunggulan Produk {{$category->NAME}}</p>
        </div>
        <div class="content--text mt-2">
            <div class="card card-reflect">
                <img src="{{ url('images/icons/asuransi-kematian.svg') }}" alt="Icon Asuransi Kematia n">
                <p>Santunan meninggal karena kecelakaan</p>
            </div>
            <div class="card card-reflect">
                <img src="{{ url('images/icons/asuransi-kecelakaan.svg') }}" alt="Icon Asuransi Kematia n">
                <p>Santunan Cacat Permanen Akibat Kecelakaan</p>
            </div>
            <div class="card card-reflect">
                <img src="{{ url('images/icons/santuan-rumah-sakit.svg') }}" alt="Icon Asuransi Kematia n">
                <p>Santunan Biaya Perawatan Rumah Sakit</p>
            </div>
            <div class="card card-reflect">
                <img src="{{ url('images/icons/asuransi-perjalanan.svg') }}" alt="Icon Asuransi Kematia n">
                <p>Melindungi rencana perjalanan Anda mulai dari Rp. 12.600,-</p>
            </div>
        </div>
    </div>

    <div class="content content--packet container mt-5 pt-5 mb-5 pb-5">
        <div class="content--title">
            <p class="text-center font-size-30">Paket Rencana {{$category->NAME}} BNI Plan B Life</p>
        </div>
        <div class="content--text mt-2">
            @foreach ($category->products as $product)
            <div class="card card-packet">
                <div class="hover-not-show">
                    <img src="{{ url('images/icons/paket-simple.svg') }}" alt="Icon Paket Simple">
                    <div class="mb-3">
                        <p>Rencana {{$category->NAME}}</p>
                        <p class="big-size">{{$product->PRODUCT_NAMA}}</p>
                    </div>

                    <div class="mb-3">
                        <p>Mulai dari</p>
                        <p>
                            <span class="big-size">Rp. {{$product->getStartFromPrice()}}</span>
                            <span>/ Bulan</span>
                        </p>
                    </div>
                </div>

                <div class="hover-show">
                    <p>Manfaat</p>
                    <ul>
                        <li>
                            <img src="{{ url('images/icons/checklist.svg') }}" alt="Icon Checklist">
                            <p>Santunan meninggal dunia karena kecelakaan</p>
                        </li>
                    </ul>
                </div>

                <a href="{{route('produk.single_product', $product->SLUG)}}" class="btn btn-card">
                    <span>Pilih Rencana</span>
                    <img src="{{ url('images/icons/icon-right-row.svg') }}" alt="Icon Right Row">
                </a>
            </div>
            @endforeach
        </div>
    </div>

    <div class="banner-advertiser mt-5 pt-5">
        <div class="text">
            <div class="title-promo">
                <p class="m-0">PROMO AKHIR BULAN</p>
            </div>
            <p class="description-promo font-size-23 font-color-black m-0">Potongan Harga untuk setiap Rencana Perjalanan</p>
            <p class="discount-promo">25%</p>
            <button class="btn-action">
                <span>Ambil Promo</span>
                <img src="{{ url('images/icons/icon-right-arrow-white.svg') }}" alt="Icon Right Arrow">
            </button>
            <p class="tnc font-size-12 font-color-black">Syarat & Ketentuan Berlaku</p>
        </div>
        <div class="image-people">
            <img src="{{ url('images/icons/contact-people.svg') }}" alt="Icon People">
        </div>
    </div>
</div>
@endsection