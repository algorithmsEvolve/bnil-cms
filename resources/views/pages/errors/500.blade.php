<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bad Request - BNI</title>
</head>

<body>
    <div style="height: 100vh; display: flex; justify-content: center; align-items: center;">
        <img src="{{ asset('images/additional/500.svg') }}" alt="">
    </div>
</body>

</html>