@extends('layouts/index')

@section('title', 'Kontak')

@section('css')
<link href="{{ url('/css/pages/contact.css') }}" rel="stylesheet">
@endsection

@section('js')
<script>
    var rECAPTCHA_SITE_KEY = "{{ env('RECAPTCHA_SITE_KEY') }}";
</script>
<script src="https://www.google.com/recaptcha/api.js?render={{env('RECAPTCHA_SITE_KEY')}}"></script>
<script src="{{ url('/js/pages/contact.js') }}"></script>
@endsection

@section('topNavigation')
@include('../partials/navigation')
@endsection

@section('bottomNavigation')
@include('../partials/footer')
@endsection

@section('content')
<div id="contact">
    <input type="hidden" id="_recaptcha" name="_recaptcha" value="">
    <input type="hidden" id="action" name="action" value="hubungi_kami">

    <div class="banner">
        <div class="banner-form">
            <p class="font-size-35 ml-2">Kamu memiliki pertanyaan <br /> seputar Kami?</p>
            <p class="font-size-20 ml-2">Kami siap membantu kamu....</p>

            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <label for="name">Nama Lengkap</label>
                        <input type="text" class="form-control border-radius-5" id="name" />
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <label for="email">Email</label>
                        <input type="email" class="form-control border-radius-5" id="email" />
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mt-3">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <label for="phone">No. Telepon</label>
                        <input type="phone" class="form-control border-radius-5" id="phone" />
                    </div>

                    <div class="col-lg-6 col-md-12">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control border-radius-5" id="alamat" />
                    </div>
                </div>
            </div>
            <div class="col-lg-12 mt-2">
                <div class="row">
                    <div class="col-lg-12">
                        <label for="pesan">Pesan</label>
                        <textarea name="pesan" id="pesan" cols="30" rows="5" class="form-control border-radius-5"></textarea>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 mt-2 center-mobile">
                <div class="row">
                    <div class="col-lg-12">
                        <button id="send" class="btn btn-send border-radius-15">Kirim</button>
                    </div>
                </div>
            </div>
        </div>

        <img src="{{ url('/images/icons/contact-people.svg') }}" alt="Contact People">
    </div>
</div>
@endsection