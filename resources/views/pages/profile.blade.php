@extends('layouts/index')

@section('title', 'profile')

@section('css')
<link href="{{ url('/css/pages/profile.css') }}" rel="stylesheet">
@endsection

@section('js')

@endsection

@section('content')
<div id="profile">
    <div class="only-show-desktop">
        <div id="profile-navigation">
            <div class="logo">
                <img src="{{ url('images/additional/bni-life-color-logo.svg') }}" alt="Logo BNI Life">
            </div>

            <ul>
                <li>
                    <a href="#" class="active">
                        <img src="{{ url('/images/icons/icon-menu-home.svg') }}" alt="Icon Profil">
                        <p>Profil</p>
                    </a>
                </li>
                <li>
                    <a href="#"><img src="{{ url('/images/icons/icon-menu-claim.svg') }}" alt="Icon Claim">
                        <p>Klaim</p>
                    </a>
                </li>
                <li>
                    <a href="#"><img src="{{ url('/images/icons/icon-menu-polish.svg') }}" alt="Icon Polish">
                        <p>E-Polish</p>
                    </a>
                </li>
                <li>
                    <a href="#"><img src="{{ url('/images/icons/icon-menu-payment.svg') }}" alt="Icon Pembayaran">
                        <p>Pembayaran</p>
                    </a>
                </li>
            </ul>
        </div>
        <div id="profile-content">
            <div class="header">
                <div class="title">
                    <p class="font-size-22 font-color-black">Profil Nasabah</p>
                </div>
                <div class="search">
                    <div class="input-group mb-3">
                        <div class="icon"><img src="{{ url('/images/icons/icon-search.svg') }}" alt="Icon Search"></div>
                        <input type="text" class="form-control" placeholder="Apa yang kamu cari?" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>

            <div class="content" id="content-profile-nasabah">
                <div class="banner mt-5">
                    <div class="text">
                        <p class="font-size-30 font-color-orient mb-0 pb-0">Hallo, John Doe</p>
                        <p class="font-size-14 pt-0 font-color-black">“Bagaimanapun orang tetap akan khawatir dengan masa depan yang tidak pasti. Dengan Asuransi maka kamu bisa meminimalisir kekawatiran tersebut.”</p>
                    </div>
                    <div class="image">
                        <img src="{{ url('/images/icons/icon-content-profile.svg') }}" alt="Icon Profile">
                    </div>
                </div>

                <div class="product mt-4">
                    <p class="font-size-17 font-color-black">Produk Saya</p>

                    <div class="box-product">
                        <div class="image">
                            <img src="{{ url('images/icons/icon-kesehatan-profile.svg') }}" alt="">
                        </div>
                        <div class="name">
                            <p class="font-size-14 m-0">Rencana Kesehatan</p>
                            <p class="font-size-16 m-0">Medis Reguler</p>
                        </div>
                        <!-- <div class="separate"></div> -->
                        <!-- <div class="price">
                            <p class="font-size-14 m-0">Manfaat Asuransi</p>
                            <p class="font-size-16 m-0">Rp. 25.000.000</p>
                        </div> -->
                        <div class="download">
                            <img src="{{ url('/images/icons/icon-download-profile.svg') }}" alt="">
                            <p class="m-0 ml-3 font-size-20 font-color-orient">Unduh</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="profile-aside">
            <div class="header">
                <div class="logout">
                    <p class="m-0">Keluar</p>
                    <img class="ml-2" src="{{ url('/images/icons/icon-logout.svg') }}" alt="Icon Logout">
                </div>
            </div>
            <div class="profile">
                <div class="image">
                    <img src="" alt="">
                </div>
                <div class="biodata mt-4">
                    <p class="name font-weight-bold font-size-17">John Doe</p>
                    <p class="birthdate font-size-14">18 Agustus 2000</p>
                </div>
            </div>
            <div class="menu mt-4">
                <div class="title">
                    <p>Data Profil</p>
                </div>

                <ul>
                    <li>
                        <div class="profile--menu" data-bs-toggle="collapse" href="#pemegangPolish" role="button" aria-expanded="false" aria-controls="pemegangPolish">
                            <div class="image"></div>
                            <p class="m-0 text-left font-size-14">Pemegang Polish</p>
                            <img src="{{ url('/images/icons/icon-arrow-bottom.svg') }}" alt="">
                        </div>
                        <div class="collapse mt-4" id="pemegangPolish">
                            <div class="card card-body">
                                Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="profile--menu" data-bs-toggle="collapse" href="#pemegangPolish" role="button" aria-expanded="false" aria-controls="pemegangPolish">
                            <div class="image"></div>
                            <p class="m-0 text-left font-size-14">Tertanggung</p>
                            <img src="{{ url('/images/icons/icon-arrow-bottom.svg') }}" alt="">
                        </div>
                        <div class="collapse mt-4" id="pemegangPolish">
                            <div class="card card-body">
                                Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="profile--menu" data-bs-toggle="collapse" href="#pemegangPolish" role="button" aria-expanded="false" aria-controls="pemegangPolish">
                            <div class="image"></div>
                            <p class="m-0 text-left font-size-14">Ahli Waris</p>
                            <img src="{{ url('/images/icons/icon-arrow-bottom.svg') }}" alt="">
                        </div>
                        <div class="collapse mt-4" id="pemegangPolish">
                            <div class="card card-body">
                                Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="only-show-mobile">
        <div id="profile-aside">
            <div class="header">
                <div class="logout">
                    <p class="m-0">Keluar</p>
                    <img class="ml-2" src="{{ url('/images/icons/icon-logout.svg') }}" alt="Icon Logout">
                </div>
            </div>
            <div class="profile">
                <div class="image">
                    <img src="" alt="">
                </div>
                <div class="biodata mt-4">
                    <p class="name font-weight-bold font-size-17">John Doe</p>
                    <p class="birthdate font-size-14">18 Agustus 2000</p>
                </div>
            </div>
        </div>

        <div class="menu--mobile">
            <ul>
                <li class="active">Profil</li>
                <li>Klaim</li>
                <li>e-Polish</li>
                <li>Pembayaran</li>
            </ul>
        </div>

        <div class="menu mt-4">
            <div class="title">
                <p class="font-color-black">Data Profil</p>
            </div>

            <ul>
                <li>
                    <div class="profile--menu" data-bs-toggle="collapse" href="#pemegangPolish" role="button" aria-expanded="false" aria-controls="pemegangPolish">
                        <div class="image"></div>
                        <p class="m-0 text-left font-size-14">Pemegang Polish</p>
                        <img src="{{ url('/images/icons/icon-arrow-bottom.svg') }}" alt="">
                    </div>
                    <div class="collapse mt-4" id="pemegangPolish">
                        <div class="card card-body">
                            Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                        </div>
                    </div>
                </li>
                <li>
                    <div class="profile--menu" data-bs-toggle="collapse" href="#pemegangPolish" role="button" aria-expanded="false" aria-controls="pemegangPolish">
                        <div class="image"></div>
                        <p class="m-0 text-left font-size-14">Tertanggung</p>
                        <img src="{{ url('/images/icons/icon-arrow-bottom.svg') }}" alt="">
                    </div>
                    <div class="collapse mt-4" id="pemegangPolish">
                        <div class="card card-body">
                            Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                        </div>
                    </div>
                </li>
                <li>
                    <div class="profile--menu" data-bs-toggle="collapse" href="#pemegangPolish" role="button" aria-expanded="false" aria-controls="pemegangPolish">
                        <div class="image"></div>
                        <p class="m-0 text-left font-size-14">Ahli Waris</p>
                        <img src="{{ url('/images/icons/icon-arrow-bottom.svg') }}" alt="">
                    </div>
                    <div class="collapse mt-4" id="pemegangPolish">
                        <div class="card card-body">
                            Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="container-profile">
            <hr class="my-5">

            <div class="banner mt-1">
                <div class="text">
                    <p class="font-size-30 font-color-orient mb-0 pb-0">Hallo, John Doe</p>
                    <p class="font-size-14 pt-0 font-color-black">“Bagaimanapun orang tetap akan khawatir dengan masa depan yang tidak pasti. Dengan Asuransi maka kamu bisa meminimalisir kekawatiran tersebut.”</p>
                </div>
                <div class="image">
                    <img src="{{ url('/images/icons/icon-content-profile.svg') }}" alt="Icon Profile">
                </div>
            </div>

            <hr class="my-5">
        </div>

        <div class="product mt-4">
            <p class="font-color-black">Produk Saya</p>

            <div class="box-product">
                <div class="image">
                    <img src="{{ url('images/icons/icon-kesehatan-profile.svg') }}" alt="">
                </div>
                <div class="content">
                    <div class="name">
                        <p class="font-size-14 m-0">Rencana Kesehatan</p>
                        <p class="font-size-16 m-0">Medis Reguler</p>
                    </div>
                    <!-- <div class="separate"></div> -->
                    <!-- <div class="price">
                            <p class="font-size-14 m-0">Manfaat Asuransi</p>
                            <p class="font-size-16 m-0">Rp. 25.000.000</p>
                        </div> -->
                    <div class="download">
                        <img src="{{ url('/images/icons/icon-download-profile.svg') }}" alt="">
                        <p class="m-0 ml-3 font-size-20 font-color-orient">Unduh</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection