@extends('layouts/index')

@section('title', 'About')

@section('css')
<link href="{{ url('/css/pages/about.css') }}" rel="stylesheet">
@endsection

@section('topNavigation')
@include('../partials/navigation')
@endsection

@section('bottomNavigation')
@include('../partials/footer')
@endsection

@section('content')
<div id="about">
    <div class="banner">
        <div class="banner-text">
            <p class="title font-size-34">
                Kenali Kami
            </p>
            <p class="font-size-16">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum neque
                ex, ac malesuada massa sollicitudin ac. Ut ut imperdiet ligula.</p>
        </div>

        <img src="{{ url('/images/icons/icon-about-people.svg')  }}" alt="Icon People About" />
    </div>

    <div class="only-show-mobile container mt-5">
        <div class="content--title">
            <p class="text-center font-size-30">Kenali Kami</p>
        </div>
        <div class="content--text mt-2">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum neque
                ex, ac malesuada massa sollicitudin ac. Ut ut imperdiet ligula.
            </p>
        </div>
    </div>

    <div class="content container mt-5 pt-5 mb-5 pb-2">
        <div class="content--title">
            <p class="text-center font-size-30">Plan BLife</p>
        </div>
        <div class="content--text mt-2">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum neque ex, ac malesuada
                massa sollicitudin ac. Ut ut imperdiet ligula. Maecenas consectetur, dui sit amet egestas
                sollicitudin, libero lectus maximus ligula, quis facilisis arcu nisi et diam. Fusce tincidunt, diam
                sed blandit mattis, nulla metus ultricies eros, at elementum diam leo et urna. Curabitur leo purus,
                gravida vel commodo ac, efficitur id nunc. Sed cursus tempor leo, nec congue felis varius et. Nulla
                molestie nibh quis velit faucibus lobortis. Phasellus congue libero sit amet eros luctus, vitae
                cursus nisl porta. Fusce pulvinar velit id nisi sodales consequat. Fusce aliquam congue erat eu
                hendrerit.
                <br />
                <br />
                Donec pharetra tempus cursus. Quisque ut commodo velit. Maecenas convallis nulla at dui varius
                aliquam. Nulla dapibus eget nisi a tristique. Pellentesque at eros lacinia, tincidunt nibh a, tempor
                dui. Mauris convallis mi mi, ac lobortis tellus congue quis. Aliquam et lacinia arcu. Praesent in
                nulla in risus imperdiet egestas. Proin eros nisl, elementum non lectus venenatis, pulvinar mollis
                orci.
            </p>
        </div>
    </div>

    <div class="banner-middle">
        <p class="font-size-30 font-color-white">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
            condimentum neque ex, ac malesuada massa sollicitudin ac. Ut ut imperdiet ligula."</p>
    </div>

    <div class="content container mt-5 pt-2 mb-5 pb-5">
        <div class="content--text">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc condimentum neque ex, ac malesuada
                massa sollicitudin ac. Ut ut imperdiet ligula. Maecenas consectetur, dui sit amet egestas
                sollicitudin, libero lectus maximus ligula, quis facilisis arcu nisi et diam. Fusce tincidunt, diam
                sed blandit mattis, nulla metus ultricies eros, at elementum diam leo et urna. Curabitur leo purus,
                gravida vel commodo ac, efficitur id nunc. Sed cursus tempor leo, nec congue felis varius et. Nulla
                molestie nibh quis velit faucibus lobortis. Phasellus congue libero sit amet eros luctus, vitae
                cursus nisl porta. Fusce pulvinar velit id nisi sodales consequat. Fusce aliquam congue erat eu
                hendrerit.
                <br />
                <br />
                Donec pharetra tempus cursus. Quisque ut commodo velit. Maecenas convallis nulla at dui varius
                aliquam. Nulla dapibus eget nisi a tristique. Pellentesque at eros lacinia, tincidunt nibh a, tempor
                dui. Mauris convallis mi mi, ac lobortis tellus congue quis. Aliquam et lacinia arcu. Praesent in
                nulla in risus imperdiet egestas. Proin eros nisl, elementum non lectus venenatis, pulvinar mollis
                orci.
            </p>
        </div>
    </div>
</div>
@endsection