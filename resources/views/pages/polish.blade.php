@extends('layouts/index')

@section('title', 'Form Polish')

@section('css')
<link href="{{ url('/css/pages/polish.css') }}" rel="stylesheet">
@endsection

@section('content')

<div id="polish">
    <div class="container">
        <!-- Card Congratulations -->
        <!-- <div class="card">
            <div class="image">
                <img src="{{ url('/images/icons/icon-checklist.svg') }}" alt="Icon Berhasil">
            </div>

            <div class="content">
                <p class="text-center font-weight-700 font-size-34 font-color-orange">Selamat John Doe, Kamu Berhasil <br /> Merencanakan Proteksi untuk Perjalananmu!</p>
                <p class="text-center font-size-20">Kami akan mengirimkan bukti email bukti transaksi <br /> beserta dokumen e-polish kamu segera ke</p>
                <p class="text-center font-weight-bold font-size-24">johndoe@email.com</p>

                <div class="actions justify-content-center">
                    <button class="btn background-blue-gradient color-white">Verifikasi Email Kamu</button>
                    <button class="btn background-gray-gradient color-white">Lihat Akun Kamu</button>
                </div>
            </div>
        </div> -->
        <!-- Card Congratulations -->

        <!-- Card Customer Service -->
        <!-- <div class="card">
            <div class="image">
                <img src="{{ url('/images/icons/icon-customer-service.svg') }}" alt="Icon Customer Service">
            </div>

            <div class="content mt-4">
                <p class="text-center font-weight-700 font-size-34 font-color-orange">Terimakasih sudah mendaftar. <br /> Agen kami akan segera menghubungi Anda</p>
            </div>
        </div> -->
        <!-- Card Customer Service -->

        <!-- Card Customer Service -->
        <div class="card">
            <div class="image">
                <img src="{{ url('/images/icons/icon-sad.svg') }}" alt="Icon Tidak Berhasil">
            </div>

            <div class="content mt-4">
                <p class="text-center font-weight-700 font-size-34 font-color-orange">Maaf, <br /> Kamu tidak bisa melanjutkan proses Pembelian</p>
                <p class="text-center font-size-20">Kami berupaya memberikan perlindungan terbaik bagi kamu. <br /> Namun mohon maaf, riwayat kesehatan kamu tidak memenuhi persyaratan <br /> pembelian produk ini.</p>
            </div>
        </div>
        <!-- Card Customer Service -->
    </div>
</div>

@endsection