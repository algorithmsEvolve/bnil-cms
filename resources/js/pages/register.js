$(document).ready(() => {
    class Register {
        constructor() {
            this.step = 1;
            this.totalStep = 8;
            this.marginBottom = [[650, 650], [1700, 4150], [500, 600], [500]];
            this.openAccordion = false;

            this.setShowStep();
            this.setMarginBottomRegister();

            // Save data snap
            this.snapResponse = "";

            // Api Helper
            this.baseUrl = "/api/product";
            this.apiUrls = {
                confirmPage: `${this.baseUrl}/confirm-page`,
                storeNasabah: `${this.baseUrl}/store-nasabah`,
                getRegency: `/api/wilayah/get-kabupaten`,
                getDistrict: `/api/wilayah/get-kecamatan`,
                getVillage: `/api/wilayah/get-desa`
            };

            // Data Rata rata pendapatan
            this.incomes = [
                "Rp. 0 s.d Rp. 5.000.000,-",
                "Rp. 5.000.001,- s.d Rp. 10.000.000,-",
                "Rp. 10.000.001,- s.d Rp. 20.000.000,-",
                "Rp. 20.000.001,- s.d Rp. 50.000.000,-",
                "Diatas Rp. 10.000.000,-"
            ];

            // Data status ahli warus
            this.heirStatuses = [
                "Ayah/Ibu Kandung",
                "Suami/Istri",
                "Saudara Kandung Perempuan/Laki-laki",
                "Anak Kandung Perempuan/Laki-laki"
            ];

            // Data status tertanggung
            this.insuredStatuses = [
                "Ayah/Ibu Kandung",
                "Suami/Istri",
                "Saudara Kandung Perempuan/Laki-laki",
                "Anak Kandung Perempuan/Laki-laki"
            ];

            // Daftar fields form
            this.fields = [
                "params",
                "NIK",
                "NAMA_LENGKAP",
                "JENIS_KELAMIN",
                "TANGGAL_LAHIR",
                "EMAIL",
                "PROVINSI",
                "KOTA",
                "KECAMATAN",
                "KELURAHAN",
                "ALAMAT",
                "KODE_POS",
                "PHONE",
                "STATUS_PERNIKAHAN",
                "TEMPAT_LAHIR",
                "PENGELUARAN",
                "PEKERJAAN",
                "POTO_PROFILE",
                // "TERTANGGUNG_STATUS_TERTANGGUNG",
                // "TERTANGGUNG_NIK",
                // "TERTANGGUNG_NAMA_LENGKAP",
                // "TERTANGGUNG_JENIS_KELAMIN",
                // "TERTANGGUNG_EMAIL",
                // "TERTANGGUNG_NOMOR_HP",
                // "TERTANGGUNG_TEMPAT_LAHIR",
                // "TERTANGGUNG_TANGGAL_LAHIR",
                // "TERTANGGUNG_FOTO_KTP",
                "AW_NIK",
                "AW_STATUS_PENERIMA",
                "AW_NAMA_LENGKAP",
                "AW_JENIS_KELAMIN",
                "AW_EMAIL",
                "AW_NOMOR_HP",
                "AW_TEMPAT_LAHIR",
                "AW_TANGGAL_LAHIR",
                "AW_FOTO_KTP"
            ];

            this.fieldsPreview = [
                [
                    ["Nama Lengkap", "NAMA_LENGKAP"],
                    ["Jenis Kelamin", "JENIS_KELAMIN"],
                    ["Nomer HP", "PHONE"][("Nomer KTP", "NIK")],
                    ["E-mail", "EMAIL"],
                    ["Tempat Lahir", "TEMPAT_LAHIR"],
                    ["Tanggal Lahir", "TANGGAL_LAHIR"],
                    ["Alamat", "ALAMAT"],
                    ["Provinsi", "PROVINSI"],
                    ["Kota", "KOTA"],
                    ["Kecamatan", "KECAMATAN"],
                    ["Kelurahan", "KELURAHAN"],
                    ["Kode POS", "KODE_POS"],
                    ["Pekerjaan", "PEKERJAAN"],
                    ["Rata-rata Pengeluaran Perbulan", "PENGELUARAN"]
                ],
                // [
                //     ["Status", "TERTANGGUNG_STATUS_TERTANGGUNG"],
                //     ["NIK", "TERTANGGUNG_NIK"],
                //     ["Nama Lengkap", "TERTANGGUNG_NAMA_LENGKAP"],
                //     ["Jenis Kelamin", "TERTANGGUNG_JENIS_KELAMIN"],
                //     ["E-mail", "TERTANGGUNG_EMAIL"],
                //     ["Nomor Telepon", "TERTANGGUNG_NOMOR_HP"],
                //     ["Tempat Lahir", "TERTANGGUNG_TEMPAT_LAHIR"],
                //     ["Tanggal Lahir", "TERTANGGUNG_TANGGAL_LAHIR"]
                //     // "TERTANGGUNG_FOTO_KTP",
                // ],
                [
                    ["Status", "AW_STATUS_PENERIMA"],
                    ["NIK", "AW_NIK"],
                    ["Nama Lengkap", "AW_NAMA_LENGKAP"],
                    ["Jenis Kelamin", "AW_JENIS_KELAMIN"],
                    ["E-mail", "AW_EMAIL"],
                    ["Nomor Telepon", "AW_NOMOR_HP"],
                    ["Tempat Lahir", "AW_TEMPAT_LAHIR"],
                    ["Tanggal Lahir", "AW_TANGGAL_LAHIR"]
                    // "TERTANGGUNG_FOTO_KTP",
                ]
            ];

            $(".preview-ktp").hide();
            this.setActiveMenu();
            this.generateDataSelect();
        }

        // Fungsi untuk generate data select option
        generateDataSelect() {
            $("#PENGELUARAN").html("");
            $("#PENGELUARAN").append(
                `<option value="">Pilih Rata-rata Pengeluaran Perbulan</option>`
            );

            this.incomes.forEach((income, index) => {
                $("#PENGELUARAN").append(
                    `<option value="${index + 1}">${income}</option>`
                );
            });

            $("#TERTANGGUNG_STATUS_TERTANGGUNG").html("");
            $("#TERTANGGUNG_STATUS_TERTANGGUNG").append(
                `<option value="">Pilih Status Tanggungan</option>`
            );
            this.heirStatuses.forEach((heir, index) => {
                $("#TERTANGGUNG_STATUS_TERTANGGUNG").append(
                    `<option value="${index + 1}">${heir}</option>`
                );
            });

            $("#AW_STATUS_PENERIMA").html("");
            $("#AW_STATUS_PENERIMA").append(
                `<option value="">Pilih Status Ahli Waris</option>`
            );
            this.insuredStatuses.forEach((insured, index) => {
                $("#AW_STATUS_PENERIMA").append(
                    `<option value="${index + 1}">${insured}</option>`
                );
            });
        }

        // Fungsi untuk menampilkan dan menyembunyikan step
        setShowStep() {
            for (let i = 1; i <= 8; i++) {
                if (i !== this.step) {
                    $(`#step-${i}`).hide();
                } else {
                    $(`#step-${i}`).show();
                }
            }
        }

        // Fungsi untuk mengubah margin bottom register
        setMarginBottomRegister() {
            $("#register").css(
                "margin-bottom",
                this.marginBottom[this.step - 1][
                    window.innerWidth <= 768 ? 1 : 0
                ] + "px"
            );
        }

        // Fungsi untuk mengaktifkan menu register
        setActiveMenu() {
            for (let i = 1; i <= 8; i++) {
                if (i === this.step) {
                    $(`#menu-text-${i}`)
                        .parent()
                        .addClass("step-active");
                } else {
                    $(`#menu-text-${i}`)
                        .parent()
                        .removeClass("step-active");
                }

                if (i <= this.step) {
                    $(`#menu-text-${i}`).addClass("active");
                } else {
                    $(`#menu-text-${i}`).removeClass("active");
                }
            }
        }

        // Fungsi untuk next step
        async moveStep(next) {
            let stop = false;
            if (this.step === 2) {
                const error = await this.confirmationData();

                if (error) stop = true;
            }

            if (this.step === 3) {
                const error = await this.sendPersonalData();

                // if (error) stop = true;
                stop = true;
            }

            if (stop) return;

            if (next) {
                this.step++;
            } else {
                this.step--;
            }
            this.setShowStep();
            this.setMarginBottomRegister();
            this.setActiveMenu();

            if (this.step === 2) {
                this.getRegency();
            }

            window.scroll({ top: 0, behavior: "smooth" });
        }

        getField(field) {
            return $(`#${field}`).val();
        }

        resetError() {
            this.fields.forEach(field => {
                $(`.error-${field}`).text("");
            });
        }

        async sendPersonalData() {
            const data = {};
            let error = false;
            this.fields.forEach(field => (data[field] = this.getField(field)));
            this.resetError();

            await $.ajax({
                type: "POST",
                url: this.apiUrls.storeNasabah,
                data
            }).then(response => {
                if (response.code === "403") {
                    Object.keys(response.message).forEach(key => {
                        $(`.error-${key}`).text(response.message[key][0]);
                    });

                    error = true;
                } else {
                    snap.pay(response.message);

                    error = false;
                }
            });

            window.scrollTo({ top: 0, behavior: "smooth" });

            return error;
        }

        async confirmationData() {
            const data = {};
            let error = false;
            this.fields.forEach(field => (data[field] = this.getField(field)));
            this.resetError();

            await $.ajax({
                type: "POST",
                url: this.apiUrls.confirmPage,
                data
            }).then(response => {
                if (response.code === "403") {
                    Object.keys(response.message).forEach(key => {
                        console.log(`.error-${key}`);
                        $(`.error-${key}`).text(response.message[key][0]);
                    });

                    error = true;
                } else {
                    $("#first-tab-preview").html("");
                    let tabs_first = $("#first-tab-preview").html();

                    this.fieldsPreview[0].forEach(field => {
                        try {
                            $("#first-tab-preview").html(
                                (tabs_first += `
                                <div class="form-group m-0 p-0">
                                    <label for="" class="font-size-12 m-0">${
                                        field[0]
                                    }</label>
                                    <p class="m-0 font-size-15" id="preview-${
                                        field[0]
                                    }">${data[field[1]]}</p>
                                </div>
                                <hr/>
                            `)
                            );
                        } catch (error) {}
                    });

                    // $("#second-tab-preview").html("");
                    // let tabs_second = $("#second-tab-preview").html();

                    // this.fieldsPreview[1].forEach(field => {
                    //     try {
                    //         $("#second-tab-preview").html(
                    //             (tabs_second += `
                    //             <div class="form-group m-0 p-0">
                    //                 <label for="" class="font-size-12 m-0">${
                    //                     field[0]
                    //                 }</label>
                    //                 <p class="m-0 font-size-15" id="preview-${
                    //                     field[0]
                    //                 }">${data[field[1]]}</p>
                    //             </div>
                    //             <hr/>
                    //         `)
                    //         );
                    //     } catch (error) {}
                    // });

                    $("#third-tab-preview").html("");
                    let tabs_third = $("#third-tab-preview").html();

                    this.fieldsPreview[1].forEach(field => {
                        try {
                            $("#third-tab-preview").html(
                                (tabs_third += `
                                <div class="form-group m-0 p-0">
                                    <label for="" class="font-size-12 m-0">${
                                        field[0]
                                    }</label>
                                    <p class="m-0 font-size-15" id="preview-${
                                        field[0]
                                    }">${data[field[1]]}</p>
                                </div>
                                <hr/>
                            `)
                            );
                        } catch (error) {}
                    });

                    error = false;
                }
            });

            window.scrollTo({ top: 0, behavior: "smooth" });

            return error;
        }

        async getRegency() {
            try {
                $("#KOTA").html("");

                await $.ajax({
                    type: "POST",
                    url: this.apiUrls.getRegency,
                    data: {
                        id_prov: $("#PROVINSI").val()
                    }
                }).then(response => {
                    response.message.forEach(regency => {
                        $("#KOTA").append(
                            `<option value="${regency.id}">${regency.name}</option>`
                        );
                    });
                });

                this.getDistrict();

                return true;
            } catch (error) {
                console.log(error);
            }
        }

        async getDistrict() {
            try {
                $("#KECAMATAN").html("");

                await $.ajax({
                    type: "POST",
                    url: this.apiUrls.getDistrict,
                    data: {
                        id_kab: $("#KOTA").val()
                    }
                }).then(response => {
                    response.message.forEach(district => {
                        $("#KECAMATAN").append(
                            `<option value="${district.id}">${district.name}</option>`
                        );
                    });
                });

                this.getVillage();

                return true;
            } catch (error) {
                console.log(error);
            }
        }

        async getVillage() {
            try {
                $("#KELURAHAN").html("");

                await $.ajax({
                    type: "POST",
                    url: this.apiUrls.getVillage,
                    data: {
                        id_kec: $("#KECAMATAN").val()
                    }
                }).then(response => {
                    response.message.forEach(village => {
                        $("#KELURAHAN").append(
                            `<option value="${village.id}">${village.name}</option>`
                        );
                    });
                });

                return true;
            } catch (error) {
                console.log(error);
            }
        }
    }

    const register = new Register();

    $(".btn-next").on("click", () => {
        register.moveStep(true);
    });

    $(".btn-prev").on("click", () => {
        register.moveStep(false);
    });

    $(".upload-file").on("change", e => {
        const image = e.target.files[0];
        const reader = new FileReader();
        reader.readAsDataURL(image);
        reader.onload = event => {
            const id = $(e.target).attr("data-field");

            $(`#${id}`).val(event.target.result);

            $(`#upload-${id}`).hide();
            $(`#preview-${id} `).show();
            $(`#image-${id}`).attr("src", event.target.result);
        };
    });

    $(".delete-file").on("click", e => {
        const id = $(e.target).attr("data-field");

        $(`#${id}`).val("");

        $(`#upload-${id}`).show();
        $(`#preview-${id}`).hide();
    });

    $("#PROVINSI").on("change", e => {
        $("#KOTA").html("");
        $("#KECAMATAN").html("");
        $("#KELURAHAN").html("");

        register.getRegency();
    });

    $("#KOTA").on("change", e => {
        $("#KECAMATAN").html("");
        $("#KELURAHAN").html("");

        register.getDistrict();
    });

    $("#KECAMATAN").on("change", e => {
        $("#KELURAHAN").html("");

        register.getVillage();
    });

    $(".input-phonenumber").keypress(e => {
        const allowInput = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        return (
            allowInput.includes(parseInt(String.fromCharCode(e.keyCode))) &&
            e.target.value.length + 2 <= 14
        );
    });

    $(".input-ktp").keypress(e => {
        const allowInput = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        return (
            allowInput.includes(parseInt(String.fromCharCode(e.keyCode))) &&
            e.target.value.length + 2 <= 17
        );
    });

    $(".accordion-button").on("click", () => {
        register.openAccordion = $("*[aria-expanded='true']").length > 0;

        if (register.openAccordion) {
            $("#register").css("margin-bottom", "1300px");
        } else {
            register.setMarginBottomRegister();
        }
    });

    // $("#AW_STATUS_PENERIMA").on("change", e => {
    //     const fields = [
    //         "NAMA_LENGKAP",
    //         "JENIS_KELAMIN",
    //         "EMAIL",
    //         "NOMOR_HP",
    //         "NIK",
    //         "TEMPAT_LAHIR",
    //         "TANGGAL_LAHIT"
    //     ];

    //     if (e.target.value === "1") {
    //         fields.forEach(field => {
    //             if (field === "TANGGAL_LAHIT") field = "TANGGAL_LAHIR";

    //             $(`#AW_${field}`).val($(`#${field}`).val());
    //             $(`#AW_${field}`).attr("disabled", true);
    //         });
    //     }
    // });

    $(".dropdown-option").click(e => {
        const value = $(e.target).attr("data-value");
        const point = $(e.target).attr("data-point");

        $("#dropdown-point-" + point).html(value === "true" ? "Ya" : "Tidak");
    });

    if ($(".form-absolute").attr("hide-riwayat") === "true") {
        register.moveStep(true);
        $(".step-riwayat").hide();
    }
});
