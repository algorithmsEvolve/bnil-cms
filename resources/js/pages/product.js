$(document).ready(() => {
    class Product {
        constructor() {
            this.data = {};
            this.times = ["MONTHLY", "THREE_MONTH", "SIX_MONTH", "ANNUALY"];

            this.apiUrls = {
                getPlan: "/api/product/get-plan"
            };
            this.query = {
                planSelectId: "",
                time: ""
            };

            this.fetchPlan();
        }

        async fetchPlan() {
            try {
                await $.ajax({
                    type: "POST",
                    url: this.apiUrls.getPlan,
                    data: {
                        product_id: $("#product_id").val(),
                        usia: $("#birth_date").val(),
                        waktu: $("#range_time").val()
                    }
                }).then(response => {
                    this.data = response.message;
                    console.log(this.data);

                    this.times.forEach(time => {
                        if (this.query.time === "") {
                            if (this.data.plan[0][time]) {
                                this.query["time"] = time;
                            }
                        }
                    });

                    this.query["planSelectId"] = this.data.plan[0].id;
                });

                return true;
            } catch (error) {
                // window.scrollTo({ top: 0, behavior: "smooth" });
                console.log(error);

                Object.keys(error.responseJSON[0]).forEach(key => {
                    $(`.error-${key}`).text(error.responseJSON[0][key][0]);
                });

                return false;
            }
        }

        async changeTimeSelect() {
            await this.fetchPlan();

            if ($(`.assurance-choice`).hasClass("assurance-choice-disabled")) {
                if ($("#birth_date").val() !== "") {
                    $(`.assurance-choice`).removeClass(
                        "assurance-choice-disabled"
                    );
                    $("#range_time").attr("disabled", false);
                } else {
                    $(`.assurance-choice`).addClass(
                        "assurance-choice-disabled"
                    );
                    $("#range_time").attr("disabled", true);
                }
            }

            let premiTitle = "";

            switch (this.data.table_name) {
                case "MONTHLY":
                    premiTitle = "Bulanan";
                    break;

                case "THREE_MONTH":
                    premiTitle = "3 Bulan";
                    break;

                case "SIX_MONTH":
                    premiTitle = "6 Bulan";
                    break;

                case "ANNUALY":
                    premiTitle = "Tahunan";
                    break;

                default:
                    break;
            }
            $(".premi_title").text(`Premi ${premiTitle}`);

            this.data.plan.forEach(plan => {
                $(`#premi_nominal_${plan.ID_PLAN}`).text(
                    this.formatToRupiah(
                        plan[this.data.table_name].split(".")[0]
                    )
                );
            });

            const plan = this.data.plan.filter(
                plan =>
                    parseInt(plan.id) ===
                    parseInt($(".assurance-choice__active").attr("data-id"))
            )[0];

            const planNominal = this.formatToRupiah(
                plan[this.data.table_name].split(".")[0]
            );

            this.query = {
                planSelectId: plan.id,
                time: this.data.table_name
            };

            $("#premi_select_nominal").html(planNominal);
        }

        formatToRupiah(angka, separator = ".", prefix = "Rp. ") {
            let number_string = angka.replace(/[^,\d]/g, "").toString(),
                split = number_string.split(","),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? "." : "";
                rupiah += separator + ribuan.join(".");
            }

            rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
            return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
        }

        changePlan(e) {
            if (
                $(`#assurance-${$(e.target).attr("data-id")}`).hasClass(
                    "assurance-choice-disabled"
                )
            ) {
                return false;
            }

            $(".assurance-choice").removeClass("assurance-choice__active");
            $(`#assurance-${$(e.target).attr("data-id")}`).addClass(
                "assurance-choice__active"
            );
            const benefit = this.data.plan_manfaat.filter(plan => {
                return (
                    parseInt(plan.ID_PLAN) ===
                    parseInt($(e.target).attr("data-id"))
                );
            })[0];

            $("#list-benefits").html("");
            JSON.parse(benefit.DESC).forEach(list => {
                $("#list-benefits").html(`
                    ${$("#list-benefits").html()}
                    <li>
                        <img src="/images/icons/checklist.svg" alt="Icon Checklist">
                        <p>${list}</p>
                    </li>
                `);
            });

            $("#benefit-assurance-nominal").html(benefit.MANFAAT);

            const plan = this.data.plan.filter(
                plan =>
                    parseInt(plan.ID_PLAN) ===
                    parseInt($(".assurance-choice__active").attr("data-id"))
            )[0];

            const planNominal = this.formatToRupiah(
                plan[this.data.table_name].split(".")[0]
            );

            this.query = {
                planSelectId: plan.id,
                time: this.data.table_name
            };
            $("#premi_select_nominal").html(planNominal);
        }
    }

    const product = new Product();

    $("#range_time").on("change", () => {
        product.changeTimeSelect();
    });

    $("#birth_date").on("change", () => {
        product.changeTimeSelect();
    });

    $(".assurance-choice").on("click", e => {
        product.changePlan(e);
    });

    // Select Plan
    $("#select-plan").on("click", e => {
        // console.log(
        //     `/register?p=${
        //         product.query.planSelectId
        //     }&w=${product.query.time.toLowerCase()}`
        // );
        window.location = `/register?p=${
            product.query.planSelectId
        }&w=${product.query.time.toLowerCase()}`;
    });
});
