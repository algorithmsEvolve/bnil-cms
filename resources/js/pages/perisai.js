$("document").ready(function() {
    class Perisai {
        constructor() {
            this.step = 1;
            this.arrNumbers = [1, 2, 3, 4, 5, 6];

            this.changeStep(1);

            this.arrNumbers.forEach(number => {
                $(`#step-${number}-next`).click(function() {
                    perisai.changeStep(number + 1);
                });
            });
            $("#step-tc").hide();

            this.baseUrl = "/api/perisai-plus";
            this.apiUrls = {
                confirmationPage: `${this.baseUrl}/confirm-page`,
                createOtp: `${this.baseUrl}/create-otp`,
                store: `${this.baseUrl}/store`
            };

            this.userData = {};

            this.hideSpinner();

            this.minutes = 4;
            this.seconds = 60;
        }

        showSpinner() {
            $(".button-submit-loading").show();
            $(".button-submit-text").hide();
        }

        hideSpinner() {
            $(".button-submit-loading").hide();
            $(".button-submit-text").show();
        }

        createRecaptaClient() {
            grecaptcha.ready(function() {
                grecaptcha
                    .execute(rECAPTCHA_SITE_KEY, { action: "perisai_plus" })
                    .then(function(token) {
                        $("input[id='_recaptcha'").val(token);
                    });
            });
        }

        async changeStep(step) {
            let stop = false;
            this.showSpinner();
            await this.createRecaptaClient();

            if (step === 2) {
                const result = await this.confirmationPage();

                stop = !result;
            } else if (step === 3) {
                const result = await this.createOtp();

                stop = !result;
            }

            if (stop) return this.hideSpinner();

            this.step = step;

            this.arrNumbers.forEach(number => {
                $(`#step-${number}`).hide();
            });

            $(`#step-${this.step}`).show();

            if (this.step === 1) {
                $("#footer").css("margin-top", "706px");
            } else if (this.step === 2) {
                $("#footer").css("margin-top", "506px");

                this.fillForm();
            } else if (this.step === 3) {
                $("#message--otp").hide();
                $(`[number-top="1"]`).select();
                $(".resend--otp").hide();
                $("#first-otp").attr("autofocus", true);

                this.startCountdown();
                $("#footer").css("margin-top", "auto");
            } else {
                $("#footer").css("margin-top", "auto");
            }

            this.hideSpinner();

            window.scrollTo({ top: 0, behavior: "smooth" });
        }

        startCountdown() {
            setInterval(() => {
                if (this.minutes < 0) {
                    $("#countdown").html(`00:00`);
                    $(".resend--otp").show();
                    return;
                }

                this.seconds--;

                if (this.seconds <= 0) {
                    this.seconds = 60;
                    this.minutes--;
                }

                $("#countdown").html(
                    `0${this.minutes}:${this.seconds.length === 1 ? "0" : ""}${
                        this.seconds
                    }`
                );
            }, 1000);
        }

        async confirmationPage() {
            $(`.error`).text("");

            const card_type = $("select[id='card_type']").val();

            this.userData = {
                _recaptcha: $("input[id='_recaptcha'").val(),
                action: $("input[id='action'").val(),
                name: $("input[id='name']").val(),
                phone: $("input[id='phone']").val(),
                card_type,
                card_type_name: $(`option[value='${card_type}']`).attr(
                    "data-text"
                ),
                number_card: $("input[id='number_card']")
                    .val()
                    .split(" ")
                    .join("")
            };

            try {
                await $.ajax({
                    type: "POST",
                    url: this.apiUrls.confirmationPage,
                    data: {
                        ...this.userData,
                        number_card: this.encryptedCC(this.userData.number_card)
                    }
                });

                window.scrollTo({ top: 0, behavior: "smooth" });

                return true;
            } catch (error) {
                // window.scrollTo({ top: 0, behavior: "smooth" });

                Object.keys(error.responseJSON[0]).forEach(key => {
                    $(`.error-${key}`).text(error.responseJSON[0][key][0]);
                });

                return false;
            }
        }

        // Untuk enkripsi nomer kartu kredit
        encryptedCC(str) {
            try {
                let encoded = "";
                for (let i = 0; i < str.length; i++) {
                    let a = str.charCodeAt(i);
                    let b = a ^ 123; // bitwise XOR with any number, e.g. 123
                    encoded = encoded + String.fromCharCode(b);
                }
                return window.btoa(encoded);
            } catch (error) {
                return str;
            }
        }

        async createOtp() {
            $(`.error`).text("");
            $(".alert-danger").hide();
            let status = true;

            try {
                this.userData._recaptcha = $("input[id='_recaptcha'").val();
                this.userData.number_card = this.userData.number_card
                    .split(" ")
                    .join("");

                await $.ajax({
                    type: "POST",
                    url: this.apiUrls.createOtp,
                    data: {
                        ...this.userData,
                        number_card: this.encryptedCC(this.userData.number_card)
                    }
                })
                    .then(resp => {
                        window.scrollTo({ top: 0, behavior: "smooth" });

                        status = true;
                    })
                    .catch(e => {
                        if (e.status === 500) {
                            $(".alert-danger").show();
                        } else {
                            Object.keys(e.responseJSON[0]).forEach(key => {
                                $(`.error-${key}`).text(
                                    e.responseJSON[0][key][0]
                                );
                            });
                        }

                        status = false;
                    });
            } catch (error) {
                status = false;
            }

            this.hideSpinner();

            return status;
        }

        async store() {
            $(`.error`).text("");
            let status = true;
            const self = this;

            this.userData.otp = "";

            Array.prototype.forEach.call(
                document.getElementsByClassName("otp--input"),
                function(otp) {
                    self.userData.otp += otp.value;
                }
            );
            this.userData._recaptcha = $("input[id='_recaptcha'").val();
            try {
                await $.ajax({
                    type: "POST",
                    url: this.apiUrls.store,
                    data: {
                        ...this.userData,
                        number_card: this.encryptedCC(this.userData.number_card)
                    }
                });

                window.scrollTo({ top: 0, behavior: "smooth" });

                this.changeStep(6);

                status = true;
            } catch (error) {
                // window.scrollTo({ top: 0, behavior: "smooth" });
                $("#message--otp").show();

                status = false;
            }

            return status;
        }

        fillForm() {
            Object.keys(this.userData).forEach(key => {
                let value = this.userData[key];

                if (key === "number_card") {
                    value = generateCCformat(value);
                }

                if (key === "card_type") return;

                if (key === "card_type_name") key = "card_type";

                $(`#text-${key}`).text(value);
            });
        }
    }

    const perisai = new Perisai();

    function generateCCformat(value) {
        let v = value.replace(/^[a-zA-Z]+$/g, "").replace(/[^0-9]/gi, "");
        let matches = v.match(/\d{4,16}/g);
        let match = (matches && matches[0]) || "";
        let parts = [];

        for (let i = 0, len = match.length; i < len; i += 4) {
            parts.push(match.substring(i, i + 4));
        }

        if (parts.length) {
            return parts.join(" ");
        } else {
            return v;
        }
    }

    $(".input-form").hide();
    $(".edit").click(e => {
        const key = e.target.getAttribute("key"),
            show = e.target.getAttribute("show"),
            text = $(`#text-${key.split("-")[1]}`),
            input = $(`#${key}`);

        if (show === "text") {
            e.target.innerHTML = "Selesai";

            // input.val(text.text());
            input.val(perisai.userData[key.split("-")[1]]);
            input.show();
            text.hide();
            e.target.setAttribute("show", "input");
        } else {
            e.target.innerHTML = "Edit";
            input.hide();
            text.show();

            if (key === "input-card_type") {
                text.text($("option[id='input-card_type']").attr("data-text"));
            } else {
                text.text(input.val());
            }
            perisai.userData[key.split("-")[1]] = input.val();
            e.target.setAttribute("show", "text");
        }
    });

    $(".input-form").keyup(e => {
        perisai.userData[
            $(e.target)
                .attr("id")
                .split("-")[1]
        ] = e.target.value;
    });

    async function processOtpInput(e) {
        const code = e.keyCode,
            number = e.target.getAttribute("number-top");

        if (e.target.value.length > 1) {
            let otp = e.target.value;
            e.target.value = parseInt(otp.toString().split("")[0]);
        }
        if (
            (code >= 48 && code <= 57) ||
            (code >= 96 && code <= 105) ||
            code === 229
        ) {
            $("#message--otp").hide();
            $(`[number-top="${parseInt(number) + 1}"]`).select();

            if (number >= 6) {
                await perisai.createRecaptaClient();
                const result = await perisai.store();

                if (result) {
                    perisai.changeStep(4);
                } else {
                    $(".otp--input").val("");
                    $(`[number-top="1"`).select();
                }
            }
        } else if (code === 8) {
            $(`[number-top="${parseInt(number) - 1}"]`).select();
            $(`[number-top="${parseInt(number) - 1}"]`).val("");
        }
    }

    // OTP Function input
    // $(".otp--input").keypress(async e => {
    //     processOtpInput(e);
    // });

    $(".otp--input").keyup(async e => {
        processOtpInput(e);
    });

    // $(".otp--input").keydown(async e => {
    //     processOtpInput(e);
    // });

    function numberCardProcess(e) {
        if (
            [
                113,
                119,
                101,
                114,
                116,
                121,
                117,
                105,
                111,
                112,
                91,
                93,
                92,
                97,
                115,
                100,
                102,
                103,
                104,
                106,
                107,
                108,
                59,
                39,
                122,
                120,
                99,
                118,
                98,
                110,
                109,
                44,
                46,
                47
            ].includes(e.keyCode)
        )
            e.preventDefault();
        e.target.value = generateCCformat(e.target.value);
    }

    // $(".number_card").keypress(e => {
    //     numberCardProcess(e);
    // });

    // $(".number_card").keydown(e => {
    //     numberCardProcess(e);
    // });

    // $(".number_card").keypress(e => {
    //     numberCardProcess(e);
    // });

    $(".number_card").keyup(e => {
        numberCardProcess(e);
    });

    $("#tc").change(e => {
        if (e.target.checked) {
            $("#step-1-next").attr("disabled", false);

            $("#step-tc").show();
            $("#step-1").hide();

            $("#footer").css("margin-top", "auto");

            setTimeout(() => {
                window.scrollTo({ top: 0, behavior: "smooth" });
            }, 100);
        } else {
            $("#step-1-next").attr("disabled", true);
        }
    });

    $(".resend--otp").click(async e => {
        $(".otp--input").val("");
        $("#message--otp").hide();
        await perisai.createOtp();

        perisai.minutes = 4;
        perisai.seconds = 60;
        // alert("OTP berhasil dikirim");

        $(".resend--otp").hide();
    });

    $("#accept-cancel").click(e => {
        perisai.changeStep(5);
    });

    $("#backToFirst").click(e => {
        perisai.changeStep(1);
    });

    $("#openTc").click(e => {
        $("#step-tc").show();
        $("#step-1").hide();

        $("#footer").css("margin-top", "auto");

        setTimeout(() => {
            window.scrollTo({ top: 0, behavior: "smooth" });
        }, 100);
    });

    $("#agree-tc").click(e => {
        perisai.changeStep(1);
        $("#step-tc").hide();

        $("#tc").prop("checked", true);
        $("#step-1-next").attr("disabled", false);
    });

    $(".input-number").keyup(e => {
        const allowInput = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

        const numberArr = [];
        e.target.value.split("").forEach(val => {
            if (allowInput.includes(parseInt(val))) {
                numberArr.push(val);
            }
        });

        e.target.value = numberArr.join("");
    });
});
