$(document).ready(() => {
    class Contact {
        constructor() {
            this.url = "/api/hubungi-kami";
            this.createRecaptaClient();
        }

        createRecaptaClient() {
            grecaptcha.ready(function() {
                grecaptcha
                    .execute(rECAPTCHA_SITE_KEY, { action: "hubungi_kami" })
                    .then(function(token) {
                        $("input[id='_recaptcha'").val(token);
                    });
            });
        }

        async sendMessage() {
            try {
                await $.ajax({
                    type: "POST",
                    url: this.url,
                    data: {
                        name: $("#name").val(),
                        email: $("#email").val(),
                        alamat: $("#alamat").val(),
                        phone: $("#phone").val(),
                        pesan: $("#pesan").val(),
                        _recaptcha: $("input[id='_recaptcha'").val(),
                        action: $("input[id='action'").val()
                    }
                });

                window.scrollTo({ top: 0, behavior: "smooth" });

                return true;
            } catch (error) {
                Object.keys(error.responseJSON[0]).forEach(key => {
                    $(`.error-${key}`).text(error.responseJSON[0][key][0]);
                });

                return false;
            }
        }
    }

    const contact = new Contact();

    $("#send").on("click", () => {
        contact.sendMessage();
    });
});
