$(document).ready(() => {
    function generateWitdhSlider() {
        const widthBannerContainer = $(".banner-container").css("width");
        $(".banner-content").css("width", widthBannerContainer);

        const widthWindow = window.innerWidth;
        $(".slider--item").css("width", widthWindow / 3 + 50);
        $(".slider--item__active").css("width", widthWindow / 3 + 176);

        $(".packet-item").css("width", widthWindow);
    }

    generateWitdhSlider();

    $(window).resize(() => {
        generateWitdhSlider();
    });

    // Generate bullet
    for (let index = 0; index < $(".banner-content").length; index++) {
        $(".bullets").append(
            `<div class="bullet ${
                index === 0 ? "bullet-active" : ""
            }"></div>   `
        );
    }

    // Slider banner home
    let indexSlide = 0;

    function scrollBanner() {
        const widthBannerContainer = $(".banner-container").css("width");

        $(".banner-scroll").animate(
            {
                marginLeft:
                    parseInt(widthBannerContainer.split("px")[0]) *
                        indexSlide *
                        -1 +
                    "px"
            },
            500
        );

        $(".bullet").removeClass("bullet-active");
        $($(".bullet")[indexSlide]).addClass("bullet-active");
    }

    $(".arrow-right").on("click", () => {
        if (indexSlide + 1 >= $(".banner-content").length) {
            indexSlide = 0;
        } else {
            indexSlide++;
        }

        scrollBanner();
    });

    $(".arrow-left").on("click", () => {
        if (indexSlide - 1 < 0) {
            indexSlide = $(".banner-content").length - 1;
        } else {
            indexSlide--;
        }

        scrollBanner();
    });

    // Slider banner panduan
    let indexSlideVideo = 0;

    function scrollVideoPanduan() {
        $(".slider--scroll").animate(
            {
                marginLeft:
                    (widthWindow / 3 + 50 + 20) * indexSlideVideo * -1 + -100
            },
            500
        );

        // Array.from(document.getElementsByClassName("slider--item")).forEach(
        //     (slider, index) => {
        //         if (index === 1) {
        //             $(slider).addClass("slider--item__active");
        //         } else {
        //             $(slider).removeClass("slider--item__active");
        //         }
        //     }
        // );
    }

    $(".arrow-right-panduan").on("click", async () => {
        // first.remove();

        if (indexSlideVideo + 1 >= $(".slider--item").length) {
            indexSlideVideo = 0;
        } else {
            indexSlideVideo++;
        }

        await scrollVideoPanduan();

        $(".slider--item")
            .first()
            .clone()
            .appendTo(".slider--scroll");
    });

    $(".arrow-left-panduan").on("click", async () => {
        if (indexSlideVideo - 1 < 0) {
            indexSlideVideo = $(".slider--item").length - 1;
        } else {
            indexSlideVideo--;
        }

        await scrollVideoPanduan();

        const first = $(".slider--item").last();
        await $(".slider--scroll").prepend(first);
    });

    // Slider plan
    let indexPlan = 0;

    $("#packet-hr-1").hide();
    $("#packet-hr-2").hide();
    $("#packet-hr-3").hide();
    $("#packet-hr-4").hide();

    $(".content--tab").click(e => {
        const index = $(e.target).attr("data-index");
        indexPlan = parseInt(index);
        // console.log(index)

        [0, 1, 2, 3].forEach(number => {
            if (number === parseInt(index)) {
                $(`#packet-hr-${number}`).show();
            } else {
                $(`#packet-hr-${number}`).hide();
            }
        });

        $(".packet-scroll").animate({
            marginLeft: widthWindow * indexPlan * -1
        });
    });

    // Slider Testimonial
    let indexTestimonial = 0;
    $(".testimonial--content").css("width", window.innerWidth);

    function scrollTestimonial() {
        $(".testimonial--scroll").animate(
            {
                marginLeft: window.innerWidth * indexTestimonial * -1
            },
            500
        );
    }

    $(".arrow-right-testimonial").on("click", () => {
        if (indexTestimonial + 1 >= $(".testimonial--content").length) {
            indexTestimonial = 0;
        } else {
            indexTestimonial++;
        }

        scrollTestimonial();
    });

    $(".arrow-left-testimonial").on("click", () => {
        if (indexTestimonial - 1 < 0) {
            indexTestimonial = $(".testimonial--content").length - 1;
        } else {
            indexTestimonial--;
        }

        scrollTestimonial();
    });

    // Animate show hide planner
    // $(".card-packet-planner").hover(e => {
    //     const id = $(e.target).attr("id");

    //     if (e.type === "mouseenter") {
    //         $(`#${id}`).css(
    //             "background",
    //             `linear-gradient(124deg, #ef5a25, rgb(210 123 1)),
    //         linear-gradient(106deg, hsla(0, 0%, 100%, 0), hsla(0, 0%, 100%, 0.69))`
    //         );

    //         $(`#${$(e.target).attr("id")} .hover-show`).show();
    //         $(`#${$(e.target).attr("id")} .hover-not-show`).hide();
    //     } else {
    //         $(`#${id}`).css("background", "#006884");

    //         $(`#${$(e.target).attr("id")} .hover-show`).hide();
    //         $(`#${$(e.target).attr("id")} .hover-not-show`).show();
    //     }
    // });
});
