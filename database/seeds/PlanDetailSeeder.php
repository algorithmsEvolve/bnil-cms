<?php

use Illuminate\Database\Seeder;

class PlanDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('plan_details')->insert(array (
            array (
                'ID_PLAN' => 1, // Digi_A
                'MONTHLY' => null,
                'THREE_MONTH' => null,
                'SIX_MONTH' => 12600,
                'ANNUALY' => 18000,
                'USIA_MIN' => null,
                'USIA_MAX' => null,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 2, // Digi_B
                'MONTHLY' => null,
                'THREE_MONTH' => null,
                'SIX_MONTH' => 16800,
                'ANNUALY' => 24000,
                'USIA_MIN' => null,
                'USIA_MAX' => null,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 3, // Digi_C
                'MONTHLY' => null,
                'THREE_MONTH' => null,
                'SIX_MONTH' => 21000,
                'ANNUALY' => 30000,
                'USIA_MIN' => null,
                'USIA_MAX' => null,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 4, // Pandai_A
                'MONTHLY' => null,
                'THREE_MONTH' => 20000,
                'SIX_MONTH' => 35000,
                'ANNUALY' => 50000,
                'USIA_MIN' => null,
                'USIA_MAX' => null,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 5, // Smart_A
                'MONTHLY' => 232000,
                'THREE_MONTH' => 689000,
                'SIX_MONTH' => 1358000,
                'ANNUALY' => 2640000,
                'USIA_MIN' => 18,
                'USIA_MAX' => 35,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 5, // Smart_A
                'MONTHLY' => 243000,
                'THREE_MONTH' => 721000,
                'SIX_MONTH' => 1421000,
                'ANNUALY' => 2763000,
                'USIA_MIN' => 36,
                'USIA_MAX' => 40,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 5, // Smart_A
                'MONTHLY' => 756000,
                'THREE_MONTH' => 20000,
                'SIX_MONTH' => 1490000,
                'ANNUALY' => 2898000,
                'USIA_MIN' => 41,
                'USIA_MAX' => 45,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 5, // Smart_A
                'MONTHLY' => 255000,
                'THREE_MONTH' => 757000,
                'SIX_MONTH' => 1493000,
                'ANNUALY' => 2909000,
                'USIA_MIN' => 46,
                'USIA_MAX' => 50,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 6, // Smart_B
                'MONTHLY' => 464000,
                'THREE_MONTH' => 1377000,
                'SIX_MONTH' => 2715000,
                'ANNUALY' => 5280000,
                'USIA_MIN' => 18,
                'USIA_MAX' => 35,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 6, // Smart_B
                'MONTHLY' => 485000,
                'THREE_MONTH' => 1441000,
                'SIX_MONTH' => 2841000,
                'ANNUALY' => 5525000,
                'USIA_MIN' => 35,
                'USIA_MAX' => 40,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 6, // Smart_B
                'MONTHLY' => 509000,
                'THREE_MONTH' => 1512000,
                'SIX_MONTH' => 2980000,
                'ANNUALY' => 5796000,
                'USIA_MIN' => 41,
                'USIA_MAX' => 45,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 6, // Smart_B
                'MONTHLY' => 510000,
                'THREE_MONTH' => 1514000,
                'SIX_MONTH' => 2985000,
                'ANNUALY' => 5805000,
                'USIA_MIN' => 46,
                'USIA_MAX' => 50,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 7, // Solusi_A
                'MONTHLY' => 70000,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 736500,
                'USIA_MIN' => 18,
                'USIA_MAX' => 29,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 7, // Solusi_A
                'MONTHLY' => 85500,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 897000,
                'USIA_MIN' => 30,
                'USIA_MAX' => 39,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 7, // Solusi_A
                'MONTHLY' => 104000,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 1089500,
                'USIA_MIN' => 40,
                'USIA_MAX' => 49,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 8, // Maxi_A
                'MONTHLY' => 79600,
                'THREE_MONTH' => 226100,
                'SIX_MONTH' => 435500,
                'ANNUALY' => 837400,
                'USIA_MIN' => 18,
                'USIA_MAX' => 29,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 8, // Maxi_A
                'MONTHLY' => 98800,
                'THREE_MONTH' => 280600,
                'SIX_MONTH' => 540400,
                'ANNUALY' => 1039100,
                'USIA_MIN' => 30,
                'USIA_MAX' => 39,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 8, // Maxi_A
                'MONTHLY' => 204800,
                'THREE_MONTH' => 582000,
                'SIX_MONTH' => 1120900,
                'ANNUALY' => 2155600,
                'USIA_MIN' => 40,
                'USIA_MAX' => 49,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 9, // Definite_A
                'MONTHLY' => 171000,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 1800000,
                'USIA_MIN' => 18,
                'USIA_MAX' => 29,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 9, // Definite_A
                'MONTHLY' => 228000,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 2400000,
                'USIA_MIN' => 30,
                'USIA_MAX' => 39,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 9, // Definite_A
                'MONTHLY' => 342000,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 3600000,
                'USIA_MIN' => 40,
                'USIA_MAX' => 44,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 9, // Definite_A
                'MONTHLY' => 513000,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 5400000,
                'USIA_MIN' => 45,
                'USIA_MAX' => 49,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 9, // Definite_A
                'MONTHLY' => 712500,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 7500000,
                'USIA_MIN' => 50,
                'USIA_MAX' => 55,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),

            array (
                'ID_PLAN' => 10, // Definite_B
                'MONTHLY' => 199500,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 2100000,
                'USIA_MIN' => 18,
                'USIA_MAX' => 29,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 10, // Definite_B
                'MONTHLY' => 285000,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 3000000,
                'USIA_MIN' => 30,
                'USIA_MAX' => 39,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 10, // Definite_B
                'MONTHLY' => 484500,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 5100000,
                'USIA_MIN' => 40,
                'USIA_MAX' => 44,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 10, // Definite_B
                'MONTHLY' => 684000,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 7200000,
                'USIA_MIN' => 45,
                'USIA_MAX' => 49,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 10, // Definite_B
                'MONTHLY' => 969000,
                'THREE_MONTH' => null,
                'SIX_MONTH' => null,
                'ANNUALY' => 10200000,
                'USIA_MIN' => 50,
                'USIA_MAX' => 55,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
        ));
    }
}
