<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(array (
            array (
                'password' => bcrypt('Bnil.admin'),
                'email' => 'bnilife@mail.test',
                'USER_STATUS' => 1,
                'role' => 0,
                'created_at' => '2021-06-07 17:19:04',
                'updated_at' => '2021-06-07 17:19:04',
            ),
        ));
    }
}
