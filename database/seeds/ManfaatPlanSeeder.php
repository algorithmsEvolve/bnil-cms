<?php

use Illuminate\Database\Seeder;

class ManfaatPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('manfaats')->insert(array (
            array (
                'ID_PLAN' => 1,
                'MANFAAT' => "Rp. 30.000.000,-",
                'DESC' => '["Uang pertanggungan jika meninggal dunia karena kecelakaan sebesar Rp. 30.000.000,-"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 2,
                'MANFAAT' => "Rp. 40.000.000,-",
                'DESC' => '["Uang pertanggungan jika meninggal dunia karena kecelakaan sebesar Rp. 40.000.000,-"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 3,
                'MANFAAT' => "Rp. 50.000.000,-",
                'DESC' => '["Uang pertanggungan jika meninggal dunia karena kecelakaan sebesar Rp. 50.000.000,-"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 4,
                'MANFAAT' => "Rp. 20.000.000,-",
                'DESC' => '["Santunan Meninggal Dunia karena Kecelakaan sebesar Rp. 20.000.000,-","Santunan Meninggal Dunia karena penyakit sebesar Rp. 2.500.000,-","Santunan Cacat Tetap Total atau Sebagian karena Kecelakaan sebesar Rp. 5.000.000,-","Santunan Tunai Harian Rawat Inap Rumah Sakit karena Kecelakaan (maksimal 120 hari) sebesar Rp. 100.000\/hari","Santunan Penggantian Biaya Operasi\/Pembedahan karena Kecelakaan sebesar Rp. 3.000.000"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 5,
                'MANFAAT' => "Rp. 20.000.000,-",
                'DESC' => '["Santunan Meninggal Dunia karena Kecelakaan sebesar Rp. 20.000.000,-","Santunan Meninggal Dunia karena penyakit sebesar Rp. 2.500.000,-","Santunan Cacat Tetap Total atau Sebagian karena Kecelakaan sebesar Rp. 5.000.000,-","Santunan Tunai Harian Rawat Inap Rumah Sakit karena Kecelakaan (maksimal 120 hari) sebesar Rp. 100.000\/hari","Santunan Penggantian Biaya Operasi\/Pembedahan karena Kecelakaan sebesar Rp. 3.000.000"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 6,
                'MANFAAT' => "Rp. 20.000.000,-",
                'DESC' => '["Santunan Meninggal Dunia karena Kecelakaan sebesar Rp. 20.000.000,-","Santunan Meninggal Dunia karena penyakit sebesar Rp. 2.500.000,-","Santunan Cacat Tetap Total atau Sebagian karena Kecelakaan sebesar Rp. 5.000.000,-","Santunan Tunai Harian Rawat Inap Rumah Sakit karena Kecelakaan (maksimal 120 hari) sebesar Rp. 100.000\/hari","Santunan Penggantian Biaya Operasi\/Pembedahan karena Kecelakaan sebesar Rp. 3.000.000"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 7,
                'MANFAAT' => "Rp. 20.000.000,-",
                'DESC' => '["Santunan Meninggal Dunia karena Kecelakaan sebesar Rp. 20.000.000,-","Santunan Meninggal Dunia karena penyakit sebesar Rp. 2.500.000,-","Santunan Cacat Tetap Total atau Sebagian karena Kecelakaan sebesar Rp. 5.000.000,-","Santunan Tunai Harian Rawat Inap Rumah Sakit karena Kecelakaan (maksimal 120 hari) sebesar Rp. 100.000\/hari","Santunan Penggantian Biaya Operasi\/Pembedahan karena Kecelakaan sebesar Rp. 3.000.000"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 8,
                'MANFAAT' => "Rp. 20.000.000,-",
                'DESC' => '["Santunan Meninggal Dunia karena Kecelakaan sebesar Rp. 20.000.000,-","Santunan Meninggal Dunia karena penyakit sebesar Rp. 2.500.000,-","Santunan Cacat Tetap Total atau Sebagian karena Kecelakaan sebesar Rp. 5.000.000,-","Santunan Tunai Harian Rawat Inap Rumah Sakit karena Kecelakaan (maksimal 120 hari) sebesar Rp. 100.000\/hari","Santunan Penggantian Biaya Operasi\/Pembedahan karena Kecelakaan sebesar Rp. 3.000.000"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 9,
                'MANFAAT' => "Rp. 20.000.000,-",
                'DESC' => '["Santunan Meninggal Dunia karena Kecelakaan sebesar Rp. 20.000.000,-","Santunan Meninggal Dunia karena penyakit sebesar Rp. 2.500.000,-","Santunan Cacat Tetap Total atau Sebagian karena Kecelakaan sebesar Rp. 5.000.000,-","Santunan Tunai Harian Rawat Inap Rumah Sakit karena Kecelakaan (maksimal 120 hari) sebesar Rp. 100.000\/hari","Santunan Penggantian Biaya Operasi\/Pembedahan karena Kecelakaan sebesar Rp. 3.000.000"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PLAN' => 10,
                'MANFAAT' => "Rp. 20.000.000,-",
                'DESC' => '["Santunan Meninggal Dunia karena Kecelakaan sebesar Rp. 20.000.000,-","Santunan Meninggal Dunia karena penyakit sebesar Rp. 2.500.000,-","Santunan Cacat Tetap Total atau Sebagian karena Kecelakaan sebesar Rp. 5.000.000,-","Santunan Tunai Harian Rawat Inap Rumah Sakit karena Kecelakaan (maksimal 120 hari) sebesar Rp. 100.000\/hari","Santunan Penggantian Biaya Operasi\/Pembedahan karena Kecelakaan sebesar Rp. 3.000.000"]',
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
        ));
    }
}
