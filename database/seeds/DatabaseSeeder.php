<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(TipeKartuSeeder::class);
        $this->call(SettingParameterSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(ProductCategories::class);
        $this->call(PlanSeeder::class);
        $this->call(PlanDetailSeeder::class);
        $this->call(ManfaatPlanSeeder::class);
        $this->call(IndoRegionProvinceSeeder::class);
        $this->call(IndoRegionRegencySeeder::class);
        $this->call(AboutUsSeeder::class);
        $this->call(TermsSeeder::class);
        if(env('DB_CONNECTION')=='mysql'){
            $this->call(IndoRegionDistrictSeeder::class);
            $this->call(IndoRegionVillageSeeder::class);
        }
    }
}
