<?php

use Illuminate\Database\Seeder;

class TermsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'id' => 1,
            'detail' => 'contoh terms'
        ];

        \DB::table('terms_and_conditions')->insert($data);
    }
}
