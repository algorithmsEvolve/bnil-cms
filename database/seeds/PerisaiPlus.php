<?php

use Illuminate\Database\Seeder;

class PerisaiPlus extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=0; $i < 1000; $i++) { 
            $act = $faker->dateTime();
            \DB::table('perisai_pluses')->insert(array (
                array (
                    'NAMA_NASABAH' => $faker->name,
                    'NOMOR_HP' => '08123333333',
                    'NOMOR_KARTU' => $faker->creditCardNumber,
                    'STATUS' => rand(0, 1),
                    'ACTIVATION_DATE' => $act,
                    'TIPE_KARTU' => rand(1, 9),
                    'created_at' => $act,
                    'updated_at' => $act,
                ),
            ));
        }
    }
}
