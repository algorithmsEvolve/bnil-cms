<?php

use App\Models\ProductCategorie;
use Illuminate\Database\Seeder;

class ProductCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('product_categories')->insert(array (
            array (
                'NAME' => 'Perjalanan',
                'DESC' => '',
                'BANNER_IMAGE' => 'images/banners/banner-product.svg',
                'PUBLISHED' => 1,
                'created_at' => '2021-06-04 17:19:04',
                'updated_at' => '2021-06-04 17:19:04',
            ),
            array (
                'NAME' => 'Pendidikan',
                'DESC' => '',
                'BANNER_IMAGE' => 'images/banners/banner-product.svg',
                'PUBLISHED' => 1,
                'created_at' => '2021-06-04 17:19:04',
                'updated_at' => '2021-06-04 17:19:04',
            ),
            array (
                'NAME' => 'Kesehatan',
                'DESC' => '',
                'BANNER_IMAGE' => 'images/banners/banner-product.svg',
                'PUBLISHED' => 1,
                'created_at' => '2021-06-04 17:19:04',
                'updated_at' => '2021-06-04 17:19:04',
            ),
            array (
                'NAME' => 'Tabungan',
                'DESC' => '',
                'BANNER_IMAGE' => 'images/banners/banner-product.svg',
                'PUBLISHED' => 1,
                'created_at' => '2021-06-04 17:19:04',
                'updated_at' => '2021-06-04 17:19:04',
            ),
        ));
    }
}
