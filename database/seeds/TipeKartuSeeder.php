<?php

use Illuminate\Database\Seeder;

class TipeKartuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipe_kartu = array(
            "Kartu Kredit BNI Visa Silver",
            "Kartu Kredit BNI Visa Gold",
            "Kartu Kredit BNI Visa Signature",
            "Kartu Kredit BNI Mastercard Silver",
            "Kartu Kredit BNI Mastercard Gold",
            "Kartu Kredit BNI Mastercard World",
            "Kartu Kredit BNI Platinum",
            "Kartu Kredit BNI Infinite",
            "Kartu Kredit BNI Titanium",
            "Kartu Kredit BNI JCB Gold",
            "Kartu Kredit BNI JCB Precious",
            "Kartu Kredit BNI Lottemart Gold",
            "Kartu Kredit BNI Lottemart Platinum",
            "Kartu Kredit BNI Pertamina Gold",
            "Kartu Kredit BNI Pertamina Platinum",
            "Kartu Kredit BNI Garuda Platinum",
            "Kartu Kredit BNI Garuda Signature",
            "Kartu Kredit BNI bjb Gold",
            "Kartu Kredit BNI bjb Platinum",
            "Kartu Kredit BNI Telkomsel Gold",
            "Kartu Kredit BNI Telkomsel Platinum",
            "Kartu Kredit BNI DKI Gold",
            "Kartu Kredit BNI DKI Platinum",
            "Kartu Kredit BNI Ferrari Platinum",
            "Kartu Kredit BNI Chelsea",
            "Kartu Kredit BNI AMEX",
        );
        $data = array();
        $no = 1;
        foreach ($tipe_kartu as $val) {
            $data[] = array(
                'TIPE_NAME' => $val,
                'TIPE_DESC' => '',
                'ORDER' => $no,
                'created_at' => '2021-03-30 17:19:04',
                'updated_at' => '2021-03-30 17:19:04',
            );
            $no++;
        }
        \DB::table('tipe_kartu_kredits')->insert($data);

        // \DB::table('tipe_kartu_kredits')->insert(array (
        //     array (
        //         'TIPE_NAME' => 'Kartu Kredit BNI DKI Platinum',
        //         'TIPE_DESC' => '',
        //         'created_at' => '2021-03-30 17:19:04',
        //         'updated_at' => '2021-03-30 17:19:04',
        //     ),
        //     array (
        //         'TIPE_NAME' => 'Kartu Kredit BNI Platinum',
        //         'TIPE_DESC' => '',
        //         'created_at' => '2021-03-30 17:19:04',
        //         'updated_at' => '2021-03-30 17:19:04',
        //     ),
        //     array (
        //         'TIPE_NAME' => 'Kartu Kredit BNI Infinite',
        //         'TIPE_DESC' => '',
        //         'created_at' => '2021-03-30 17:19:04',
        //         'updated_at' => '2021-03-30 17:19:04',
        //     ),
        //     array (
        //         'TIPE_NAME' => 'Kartu Kredit BNI Garuda Signature',
        //         'TIPE_DESC' => '',
        //         'created_at' => '2021-03-30 17:19:04',
        //         'updated_at' => '2021-03-30 17:19:04',
        //     ),
        //     array (
        //         'TIPE_NAME' => 'Kartu Kredit BNI Garuda Platinum',
        //         'TIPE_DESC' => '',
        //         'created_at' => '2021-03-30 17:19:04',
        //         'updated_at' => '2021-03-30 17:19:04',
        //     ),
        //     array (
        //         'TIPE_NAME' => 'Kartu Kredit BNI Ferrari Platinum',
        //         'TIPE_DESC' => '',
        //         'created_at' => '2021-03-30 17:19:04',
        //         'updated_at' => '2021-03-30 17:19:04',
        //     ),
        //     array (
        //         'TIPE_NAME' => 'Kartu Kredit BNI Visa Signature',
        //         'TIPE_DESC' => '',
        //         'created_at' => '2021-03-30 17:19:04',
        //         'updated_at' => '2021-03-30 17:19:04',
        //     ),
        //     array (
        //         'TIPE_NAME' => 'Kartu Kredit BNI Mastercard World',
        //         'TIPE_DESC' => '',
        //         'created_at' => '2021-03-30 17:19:04',
        //         'updated_at' => '2021-03-30 17:19:04',
        //     ),
        //     array (
        //         'TIPE_NAME' => 'Kartu Kredit BNI Pertamina Gold',
        //         'TIPE_DESC' => '',
        //         'created_at' => '2021-03-30 17:19:04',
        //         'updated_at' => '2021-03-30 17:19:04',
        //     ),
        // ));
    }
}
