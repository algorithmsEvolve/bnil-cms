<?php

use Illuminate\Database\Seeder;

class SettingParameterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('setting_parameters')->insert(array (
            0 => 
            array (
                'SETTING_ID' => 'pdf_perisaiplus',
                'SETTING_VALUE' => 'assets/documents/perisai-plus/RIPLAY Umum.pdf',
                'SETTING_DESC' => 'Document PDF Perisai Plus',
                'SETTING_TYPE' => 'PDF',
                'created_at' => '2021-03-30 17:19:04',
                'updated_at' => '2021-03-30 17:19:04',
            ),
        ));
    }
}
