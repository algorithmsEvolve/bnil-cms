<?php

use Illuminate\Database\Seeder;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('plans')->insert(array (
            array (
                'ID_PRODUCT' => 1,
                'NAME' => 'DIGI_A',
                'MANFAAT' => '',
                'UP' => 30000000,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PRODUCT' => 1,
                'NAME' => 'DIGI_B',
                'MANFAAT' => '',
                'UP' => 40000000,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PRODUCT' => 1,
                'NAME' => 'DIGI_C',
                'MANFAAT' => '',
                'UP' => 50000000,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PRODUCT' => 2,
                'NAME' => 'PANDAI_A',
                'MANFAAT' => '',
                'UP' => 20000000,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PRODUCT' => 3,
                'NAME' => 'Smart_A',
                'MANFAAT' => '',
                'UP' => 50000000,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PRODUCT' => 3,
                'NAME' => 'Smart_B',
                'MANFAAT' => '',
                'UP' => 100000000,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PRODUCT' => 4,
                'NAME' => 'Solusi_A',
                'MANFAAT' => '',
                'UP' => 0,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PRODUCT' => 5,
                'NAME' => 'Maxi_A',
                'MANFAAT' => '',
                'UP' => 50000000,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PRODUCT' => 6,
                'NAME' => 'Definite_A',
                'MANFAAT' => '',
                'UP' => 60000000,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
            array (
                'ID_PRODUCT' => 6,
                'NAME' => 'Definite_B',
                'MANFAAT' => '',
                'UP' => 60000000,
                'PUBLISHED' => 1,
                'created_at' => '2021-04-05 17:19:04',
                'updated_at' => '2021-04-05 17:19:04',
            ),
        ));
    }
}
