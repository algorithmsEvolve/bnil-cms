<?php

use Illuminate\Database\Seeder;

class AboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'id' => 1,
            'TITLE' => 'Contoh About US',
            'QOUTE' => '<p>contoh quote</p>',
            'DESC' => '<p>contoh desc</p>'
        ];
        \DB::table('about_us')->insert($data);
    }
}
