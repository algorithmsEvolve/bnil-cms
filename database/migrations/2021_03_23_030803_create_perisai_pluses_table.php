<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerisaiPlusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perisai_pluses', function (Blueprint $table) {
            $table->id();
            $table->string('NAMA_NASABAH', 100);
            $table->string('NOMOR_HP', 100);
            $table->string('nomor_kartu', 255);
            $table->integer('STATUS');
            $table->tinyInteger('MANUAL_FILLED')->nullable();
            $table->dateTime('ACTIVATION_DATE')->nullable();
            $table->integer('TIPE_KARTU')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perisai_pluses');
    }
}
