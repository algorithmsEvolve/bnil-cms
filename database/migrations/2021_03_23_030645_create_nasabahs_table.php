<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNasabahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nasabahs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('USER_ID')->nullable();
            $table->string('NIK', 20);
            $table->string('NAMA_LENGKAP', 100);
            $table->tinyInteger('JENIS_KELAMIN');
            $table->date('TANGGAL_LAHIR');
            $table->string('EMAIL', 250);
            $table->char('PROVINSI', 2);
            $table->char('KOTA', 4);
            $table->char('KECAMATAN', 7);
            $table->char('KELURAHAN', 10);
            $table->text('ALAMAT');
            $table->string('KODE_POS', 10)->nullable();
            $table->string('PHONE', 20);
            $table->integer('STATUS_PERNIKAHAN')->nullable();
            $table->string('TEMPAT_LAHIR', 100);
            $table->integer('PENGELUARAN')->nullable();
            $table->string('PEKERJAAN', 100)->nullable();
            $table->string('POTO_PROFILE', 250)->nullable();

            $table->integer('TERTANGGUNG_STATUS_TERTANGGUNG')->nullable();
            $table->string('TERTANGGUNG_NIK', 20)->nullable();
            $table->string('TERTANGGUNG_NAMA_LENGKAP', 100)->nullable();
            $table->tinyInteger('TERTANGGUNG_JENIS_KELAMIN')->nullable();
            $table->string('TERTANGGUNG_EMAIL', 250)->nullable();
            $table->string('TERTANGGUNG_NOMOR_HP', 20)->nullable();
            $table->string('TERTANGGUNG_TEMPAT_LAHIR', 100)->nullable();
            $table->date('TERTANGGUNG_TANGGAL_LAHIR')->nullable();
            $table->string('TERTANGGUNG_FOTO_KTP', 250)->nullable();

            $table->string('AW_NIK', 20);
            $table->integer('AW_STATUS_PENERIMA');
            $table->string('AW_NAMA_LENGKAP', 100);
            $table->tinyInteger('AW_JENIS_KELAMIN');
            $table->string('AW_EMAIL', 100)->nullable();
            $table->string('AW_NOMOR_HP', 20)->nullable();
            $table->string('AW_TEMPAT_LAHIR', 100);
            $table->date('AW_TANGGAL_LAHIR');
            $table->string('AW_FOTO_KTP', 250);

            $table->integer('STATUS')->default(0);
            $table->datetime('ACTIVATION_DATE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nasabahs');
    }
}
