<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('password', 255);
            $table->string('email', 100)->unique();
            $table->dateTime('LAST_LOGIN')->nullable();
            $table->integer('USER_STATUS')->default(1); // 1 Active, 0 Non Active
            $table->tinyInteger('role')->default(0); // 0 Member, 1 Admin
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
