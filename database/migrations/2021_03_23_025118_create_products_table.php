<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('PRODUCT_NAMA', 100);
            $table->string('SLUG', 100);
            $table->string('PRODUCT_CODE_API', 100);
            $table->text('PRODUCT_DESC')->nullable();
            $table->integer('CATEGORY_ID');
            $table->text('MANFAAT')->nullable();
            $table->text('KETENTUAN_UMUM')->nullable();
            $table->text('PENGECUALIAN')->nullable();
            $table->text('MEKANISME_KLAIM')->nullable();
            $table->string('FILE_PDF', 200)->nullable();
            $table->integer('PUBLISHED');
            $table->string('PAYMENT_METHOD', 200)->nullable();
            $table->tinyInteger('USIA_TERTANGGUNG')->nullable();
            $table->char('USIA_MIN', 2)->nullable();
            $table->char('USIA_MAX', 2)->nullable();
            $table->string('PRODUCT_IMAGE', 200)->nullable();
            $table->string('PRODUCT_LOGO', 200)->nullable();
            $table->tinyInteger('HIDE_RIWAYAT_KESEHATAN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
