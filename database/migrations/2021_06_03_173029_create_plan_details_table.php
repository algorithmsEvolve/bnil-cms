<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_details', function (Blueprint $table) {
            $table->id();
            $table->integer('ID_PLAN');
            $table->decimal('MONTHLY', 10, 2)->nullable();
            $table->decimal('THREE_MONTH', 10, 2)->nullable();
            $table->decimal('SIX_MONTH', 10, 2)->nullable();
            $table->decimal('ANNUALY', 10, 2)->nullable();
            $table->bigInteger('USIA_MIN')->nullable();
            $table->bigInteger('USIA_MAX')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_details');
    }
}
