<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->string('NO_ORDER', 100);
            $table->string('NIK', 20);
            $table->string('CUST_NAME', 100);
            $table->string('CUST_PHONE', 100);
            $table->string('CUST_EMAIL', 100);
            $table->text('CUST_ADDRESS');
            $table->decimal('TOTAL_AMOUNT', 8, 2);
            $table->string('PREFIX', 20)->nullable();
            $table->string('KDCHANEL', 20)->nullable();
            $table->string('KDPRODUK', 50)->nullable();
            $table->dateTime('MULAS')->nullable();
            $table->dateTime('JTTEMPO')->nullable();
            $table->integer('PAYMENT_STATUS')->default(0);
            $table->string('PAYMENT_PREMIUM', 50)->nullable();
            $table->dateTime('SETTLE_DATE')->nullable();
            $table->integer('PAYMENT_CHANNEL')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
