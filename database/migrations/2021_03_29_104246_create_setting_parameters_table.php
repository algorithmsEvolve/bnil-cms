<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_parameters', function (Blueprint $table) {
            $table->string('SETTING_ID')->primary();
            $table->text('SETTING_VALUE')->nullable();
            $table->string('SETTING_DESC')->nullable();
            $table->string('SETTING_TYPE', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_parameters');
    }
}
