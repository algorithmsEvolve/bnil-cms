<?php

use Illuminate\Support\Facades\Route;
use App\Library\Helpers\BnilifeApi;
use Illuminate\Support\Facades\Cookie;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Frontend'], function () {
  Route::group(['prefix' => 'produk'], function () {
    Route::get('/{slug}', 'ProductController@single_category')->name('produk.single_category');
  });
  Route::group(['prefix' => 'plan'], function () {
    Route::get('/{slug}', 'ProductController@single_product')->name('produk.single_product');
  });
  Route::get('/register', 'RegisterController@index')->name('register-plan');
  Route::get('/perisai-plus', 'PerisaiController@index')->name('front.perisai_plus');
});

Auth::routes(['register' => false]);
Route::get('/404', 'ErrorController@notfound');
Route::get('/500', 'ErrorController@pageerror');
Route::get('/polish', 'PolishController@index');




// hanya untuk test
// Route::post('geturl', 'MidtransController@getUrlPayment');
// Route::get('midtrans', 'MidtransController@index');


Route::get('perisai', 'Api\PerisaiController@index')->name('perisai');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/service', 'HomeController@service')->name('service');
Route::get('/profile', 'HomeController@profile')->name('profile');
Route::get('/register/success', 'HomeController@registerSuccess')->name('register-success');
// Route::get('/product', 'HomeController@product');
// Route::get('/packet', 'HomeController@packet');

Route::get('/contact', 'HomeController@contact')->name('contact');

// Route::get('/test-email', function () {
//   $detail = [
//     'view' => 'mail.registration-perisai-plus',
//     'subject' => 'Hello',
//     'title' => 'Title',
//     'body' => 'Test Body',
//   ];
//   SendEmail::dispatch('kurniarocki30@gmail.com', $detail);
// });
