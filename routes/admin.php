<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin', 'middleware' => 'auth'], function () {
    // You can also use auth middleware to prevent unauthenticated users
    Route::get('/', 'DashboardController@index');

    Route::group((['prefix' => 'perisai-plus']), function () {
        Route::get('/', 'PerisaiPlusController@index')->name('admin.perisai-plus');
        Route::get('/get-perisai-plus', 'PerisaiPlusController@getPerisaiPlus')->name('admin.perisai-plus.datatables');
    });
    Route::group((['prefix' => 'hubungi-kami']), function () {
        Route::get('/', 'HubungiKamiController@index')->name('admin.hubungi-kami');
        Route::get('/get-hubungi-kami', 'HubungiKamiController@getHubungiKami')->name('admin.hubungi-kami.datatables');
    });

    Route::group((['prefix' => 'about']), function () {
        Route::get('/', 'AboutusController@index')->name('admin.about');
        Route::post('/store/{id}', 'AboutusController@store')->name('admin.about.store');
    });

    Route::group((['prefix' => 'terms']), function () {
        Route::get('/', 'TermsController@index')->name('admin.terms');
        Route::post('/store/{id}', 'TermsController@store')->name('admin.terms.store');
    });

    Route::resource('settings', 'SettingController')->names('admin.settings');
});
