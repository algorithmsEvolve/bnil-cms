<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace' => 'Api'], function(){
    //Perisai Plus
    Route::group(['prefix' => 'perisai-plus'], function () {
        Route::POST('confirm-page', 'PerisaiController@confirmPage')->name('api.confirmPage.perisai');
        Route::POST('create-otp', 'PerisaiController@createOtp')->name('api.createOtp.perisai');
        Route::POST('store', 'PerisaiController@store')->name('api.store.perisai');
    });
    //Product
    Route::group(['prefix' => 'product'], function () {
        Route::POST('confirm-page', 'ProductController@confirmPage')->name('api.confirmPage.product');
        Route::POST('store-nasabah', 'ProductController@storeNasabah')->name('api.storeNasabah.product');
        Route::POST('get-plan', 'ProductController@getPlan')->name('api.getPlan.product');
    });
    //Midtrans API
    Route::group(['prefix' => 'midtrans'], function () {
        Route::POST('notif-handler', 'MidtransController@notifHandler');
    });
    //Wilayah API
    Route::group(['prefix' => 'wilayah'], function () {
        Route::POST('get-provinsi', 'WilayahController@getProvinsi');
        Route::POST('get-kabupaten', 'WilayahController@getKabupaten');
        Route::POST('get-kecamatan', 'WilayahController@getKecamatan');
        Route::POST('get-desa', 'WilayahController@getDesa');
    });

    //Hubungi kami
    Route::POST('hubungi-kami', 'HubungiKamiController@store');
});
//untuk coba2
// Route::POST('notify', 'DokuController@getNotify');
